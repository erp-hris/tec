<?php

use Illuminate\Database\Seeder,
    App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
       	User::truncate();

        // And now, let's create a few articles in our database:

        // 0 = admin
        // 1 = regular admin / supervisor
        // 2 = processor
        // 3 = client 
    
        User::create([
            'last_name' => 'Miranda',
            'first_name' => 'Xybriel Dennis',
            'middle_name' => 'Mista',
            'username' => 'XMM0001',
            'email' => 'xybrielmiranda.tieza@gmail.com',
            'password' => bcrypt(123123),
            'level' => '0',
            'status' => '1',
            'created_by' => '1',
        ]);
    }
}
