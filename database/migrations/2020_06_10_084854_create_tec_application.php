<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTecApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tec_application')) {
            Schema::create('tec_application', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->string('passport_no')->nullable();
                $table->dateTime('date_application', 0);
                $table->datetime('date_ticket_issued', 0);
                $table->string('ticket_no')->nullable();
                $table->dateTime('date_flight', 0);
                $table->integer('section_id')->nullable();
                $table->integer('country_id')->nullable();
                $table->integer('status')->nullable();
                $table->integer('denial_id')->nullable();
                $table->text('denial_msg')->nullable();
                $table->integer('assign_processor_id')->nullable();
                $table->string('id_picture_2x2_fn')->nullable();
                $table->string('passport_identification_page_fn')->nullable();
                $table->string('ticket_booking_ref_no_fn')->nullable();
                $table->integer('applicant_type_id')->nullable();
                $table->integer('airlines_id')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tec_application');
    }
}
