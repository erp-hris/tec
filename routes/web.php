<?php

Route::any('/', 'Auth\LoginController@showLoginForm');

Auth::routes();

Route::any('/', 'WebController\HomeController@index');
Route::any('/home', 'WebController\HomeController@index')->name('home');
Route::any('/account', 'WebController\AccountController@index');
Route::any('/account/update', 'WebController\AccountController@update_account');
Route::any('/date_flight/{type}', 'Controller@date_flight');
Route::any('/check_date_flight', 'Controller@check_date_flight');
Route::resource('/create', 'Auth\CreateController');
Route::resource('/fulltax', 'WebController\FullTaxController');
Route::resource('/kiosk', 'WebController\KioskController');
Route::any('/payment_success','WebController\FullTaxController@payment_success')->name('payment_success');
Route::any('/payment_failed','WebController\FullTaxController@payment_failed')->name('payment_failed');
Route::any('/get_arreference','WebController\FullTaxController@getARReference');
Route::any('/get_fulltaxref','WebController\FullTaxController@getFulltaxReference');
Route::any('/resend_ar/{id}','WebController\FullTaxController@resend_ar');
Route::any('/tec_applicant', 'WebController\ApplicantController@tec_index');
Route::any('/rtt_applicant', 'WebController\ApplicantController@rtt_index');
Route::any('/list_online', 'WebController\ApplicantController@list_online_index');
Route::any('/list_online/datatables', 'WebController\ApplicantController@list_online_datatables');


Route::any('/save_onlinetec', 'WebController\ApplicantController@save_onlinetec');
Route::any('/save_onlinertt', 'WebController\ApplicantController@save_onlinertt');

Route::any('/online_section_upload', 'WebController\ApplicantController@online_filter_section_upload_by');
Route::any('/online_section_filter_by', 'WebController\ApplicantController@online_filter_section_by');




Route::get('/view_ar/{fulltax_no}', [ function ( $fulltax_no) {
	$folder_path = storage_path('tieza/fulltax/ar_cert/'.$fulltax_no.'/'.$fulltax_no.'_arcert.png');
	
	if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => 'image/jpeg'));
	
	dd('AR Certificate not found!');
}]);
// Route::any('/single_transaction','WebController\FullTaxController@single_type')->name('single_trans');



 Route::any('/test_email', 'Controller@test_email');
// Route::any('/test_viewmail/{option}', 'Controller@test_viewmail');

//Route::any('/update_tec_generate', 'APIController\ApplicationController@update_tec_generated_id');
//Route::any('/show_current_generate_id', 'APIController\ApplicationController@show_current_generate_id');


//Route::any('/update_name', 'Controller@update_name');

// when authenticated

Route::get('/images/{id}/{file}', [ function ($id, $file) {

    $folder_path = storage_path('tieza/tec/attachment/'.$id.'/'.$file);

    $explode_file = explode('.', $file);

    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
    {
    	$content = 'image/jpeg';
    }
    elseif(in_array($explode_file[1], ['pdf']))
    {
    	$content = 'application/pdf';
    }

    if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
    
    abort(404);
}]);

Route::get('/images_rtt/{id}/{file}', [ function ($id, $file) {

    $folder_path = storage_path('tieza/rtt/attachment/'.$id.'/'.$file);

    $explode_file = explode('.', $file);

    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
    {
    	$content = 'image/jpeg';
    }
    elseif(in_array($explode_file[1], ['pdf']))
    {
    	$content = 'application/pdf';
    }

    if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
    
    abort(404);
}]);

Route::get('/attachment/{option}/{id}/{file}', [ function ($option, $id, $file) {

    $folder_path = storage_path('tieza/'.$option.'/attachment/'.$id.'/'.$file);

    $explode_file = explode('.', $file);

    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
    {
    	$content = 'image/jpeg';
    }
    elseif(in_array($explode_file[1], ['pdf']))
    {
    	$content = 'application/pdf';
    }

    if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
    
    abort(404);
}])->middleware('auth');

Route::get('/qr_code/{file}', [ function ($file) {
	$folder_path = storage_path('tieza/tec/qr-code/'.$file);

	$explode_file = explode('.', $file);

	if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	{
	   	$content = 'image/jpeg';
	}
	elseif(in_array($explode_file[1], ['pdf']))
	{
	   	$content = 'application/pdf';
	}

	if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	abort(404);
}]);
Route::get('/qr_code_ft/{id}/{file}', [ function ($id,$file) {
	$folder_path = storage_path('tieza/fulltax/qr-code/'.$id.'/'.$file);

	$explode_file = explode('.', $file);

	if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	{
	   	$content = 'image/jpeg';
	}
	elseif(in_array($explode_file[1], ['pdf']))
	{
	   	$content = 'application/pdf';
	}

	if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	abort(404);
}]);

Route::get('/qr_code_rtt/{file}', [ function ($file) {
	$folder_path = storage_path('tieza/rtt/qr-code/'.$file);

	$explode_file = explode('.', $file);

	if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	{
	   	$content = 'image/jpeg';
	}
	elseif(in_array($explode_file[1], ['pdf']))
	{
	   	$content = 'application/pdf';
	}

	if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	abort(404);
}]);

Route::middleware(['isTravelTax'])->prefix('travel_tax')->group(function () {
	Route::any('/application/{option}', 'WebController\ApplicationController@application');

	Route::any('/tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');
	Route::any('/tr_application/datatables/attachment', 'APIController\ApplicationController@tr_application_datatables');
	
	Route::any('/filter_tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');

	Route::any('/section/json', 'APIController\ApplicationController@filter_section_by');
	Route::any('/section_upload/json', 'APIController\ApplicationController@filter_section_upload_by');

	Route::get('/tr/certificate/{id}/{file}', [ function ($id, $file) {
	    $folder_path = storage_path('tieza/tr/certificate/'.$id.'/'.$file);
	    if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => 'image/jpeg'));
	    abort(404);
	}]);
});

Route::middleware(['isProcessor'])->prefix('processor')->group(function () {
	// Web
	Route::any('/application/{option}', 'WebController\ApplicationController@application');
	Route::any('/application/{option}/save', 'WebController\ApplicationController@save_application');
	Route::any('/application/{option}/delete', 'WebController\ApplicationController@delete_application');
	Route::any('/application/{option}/print', 'WebController\ApplicationController@print_application');

	// Api
	Route::any('/tec_application/json', 'APIController\ApplicationController@filter_tec_application_by');
	Route::any('/tec_application/datatables/attachment', 'APIController\ApplicationController@tec_application_datatables');

	Route::any('/tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');
	Route::any('/tr_application/datatables/attachment', 'APIController\ApplicationController@tr_application_datatables');

	Route::any('/rtt_application/json', 'APIController\ApplicationController@filter_rtt_application_by');
	Route::any('/rtt_application/datatables/attachment', 'APIController\ApplicationController@rtt_application_datatables');
	Route::any('/rtt_application/datatables', 'APIController\ApplicationController@rtt_application_datatables');

	Route::any('/filter_tec_application/json', 'APIController\ApplicationController@filter_tec_application_by');
	Route::any('/filter_tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');
 
	Route::any('/section/json', 'APIController\ApplicationController@filter_section_by');
	Route::any('/section_upload/json', 'APIController\ApplicationController@filter_section_upload_by');

	Route::get('/esignature','APIController\ApplicationController@get_image_application');

	Route::any('/airlines/json', 'APIController\ApplicationController@filter_airline_by');

	Route::any('/reason_denials/json', 'APIController\ApplicationController@filter_reason_denial_by');

	Route::any('/applicant_fee/json', 'APIController\ApplicationController@filter_applicant_fee_by');
	Route::any('/chk_duplicate/json', 'APIController\ApplicationController@check_duplicate_name');

});

Route::middleware(['isRegularAdmin'])->prefix('supervisor')->group(function () {
	// Web
	Route::any('/application/{option}', 'WebController\ApplicationController@application');
	Route::any('/application/{option}/save', 'WebController\ApplicationController@save_application');
	Route::any('/application/{option}/delete', 'WebController\ApplicationController@delete_application');
	Route::any('/application/{option}/remove', 'WebController\ApplicationController@remove_application_attachment');
	Route::any('/application/{option}/print', 'WebController\ApplicationController@print_application');
	Route::any('/application/{option}/generate', 'WebController\ApplicationController@generate_application');

	// Api
	Route::any('/tec_application/json', 'APIController\ApplicationController@filter_tec_application_by');
	Route::any('/tec_application/datatables/attachment', 'APIController\ApplicationController@tec_application_datatables');

	Route::any('/tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');
	Route::any('/tr_application/datatables/attachment', 'APIController\ApplicationController@tr_application_datatables');

	Route::any('/rtt_application/json', 'APIController\ApplicationController@filter_rtt_application_by');
	Route::any('/rtt_application/datatables/attachment', 'APIController\ApplicationController@rtt_application_datatables');
	Route::any('/rtt_application/datatables', 'APIController\ApplicationController@rtt_application_datatables');

	Route::any('/filter_tec_application/json', 'APIController\ApplicationController@filter_tec_application_by');
	Route::any('/filter_tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');

	Route::any('/section/json', 'APIController\ApplicationController@filter_section_by');
	Route::any('/section_upload/json', 'APIController\ApplicationController@filter_section_upload_by');

	Route::any('/airlines/json', 'APIController\ApplicationController@filter_airline_by');

	Route::any('/reason_denials/json', 'APIController\ApplicationController@filter_reason_denial_by');

	Route::any('/applicant_fee/json', 'APIController\ApplicationController@filter_applicant_fee_by');
	Route::any('/chk_duplicate/json', 'APIController\ApplicationController@check_duplicate_name');

	Route::get('/esignature/{file}', [ function ($file) {
	    $folder_path = storage_path('esig/'.$file);

	    if (file_exists($folder_path)) {
	        return response()->file($folder_path, array('Content-Type' =>'image/jpeg'));
	    }
	    abort(404);
	}]);

	Route::get('/certificate/{id}/{file}', [ function ($id, $file) {

	    $folder_path = storage_path('tieza/tec/certificate/'.$id.'/'.$file);

	    $explode_file = explode('.', $file);

	    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	    {
	    	$content = 'image/jpeg';
	    }
	    elseif(in_array($explode_file[1], ['pdf']))
	    {
	    	$content = 'application/pdf';
	    }

	    if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	    abort(404);
	}]);

	Route::get('tr/certificate/{id}/{file}', [ function ($id, $file) {

	    $folder_path = storage_path('tieza/tr/certificate/'.$id.'/'.$file);

	    $explode_file = explode('.', $file);

	    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	    {
	    	$content = 'image/jpeg';
	    }
	    elseif(in_array($explode_file[1], ['pdf']))
	    {
	    	$content = 'application/pdf';
	    }

	   if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	    abort(404);
	}]);

	Route::get('rtt/certificate/{id}/{file}', [ function ($id, $file) {

	    $folder_path = storage_path('tieza/rtt/certificate/'.$id.'/'.$file);

	    $explode_file = explode('.', $file);

	    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	    {
	    	$content = 'image/jpeg';
	    }
	    elseif(in_array($explode_file[1], ['pdf']))
	    {
	    	$content = 'application/pdf';
	    }

	   if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	    abort(404);
	}]);
});

Route::middleware(['isUser'])->prefix('user')->group(function () {
	// only for user
	

	 Route::any('/application/{option}', 'WebController\ApplicationController@application'); 
     Route::any('/application/{option}/save', 'WebController\ApplicationController@save_application');
	 
	Route::any('/chk_duplicate/json', 'APIController\ApplicationController@check_duplicate_name');
	Route::any('/section/json', 'APIController\ApplicationController@filter_section_by');
	Route::any('/section_upload/json', 'APIController\ApplicationController@filter_section_upload_by');
});

// Route::middleware(['isKiosk'])->prefix('kiosk')->group(function () {
// 	// only for kiosk
	
	
// 	Route::any('/application/{option}', 'WebController\ApplicationController@application'); 
// 	Route::any('/application/{option}/save', 'WebController\ApplicationController@save_application');
// 	Route::get('/kiosk_qr/{id}', 'WebController\KioskController@generate_qrcode')->name('kiosk_qrcode');
	 
// 	Route::any('/chk_duplicate/json', 'APIController\ApplicationController@check_duplicate_name');
// 	Route::any('/section/json', 'APIController\ApplicationController@filter_section_by');
// 	Route::any('/section_upload/json', 'APIController\ApplicationController@filter_section_upload_by');
// });

Route::middleware(['isSuperAdmin'])->group(function () {
	// only for super admin

	// Web
	Route::any('/system_config/{option}', 'WebController\SystemConfigController@system_config');
	Route::any('/system_config/{option}/save', 'WebController\SystemConfigController@save_system_config');
	Route::any('/system_config/{option}/delete', 'WebController\SystemConfigController@delete_system_config');
	
	Route::any('/system_config/{option}', 'WebController\SystemConfigController@system_config');
	Route::any('/system_config/registration_approval/update', 'WebController\SystemConfigController@update_status');
	Route::any('/system_config/signatories/addnew', 'WebController\SystemConfigController@new_signatories');


	Route::any('/application/{option}', 'WebController\ApplicationController@application');
	Route::any('/application/{option}/save', 'WebController\ApplicationController@save_application');
	Route::any('/application/{option}/delete', 'WebController\ApplicationController@delete_application');
	Route::any('/application/{option}/remove', 'WebController\ApplicationController@remove_application_attachment');
	Route::any('/application/{option}/print', 'WebController\ApplicationController@print_application');
	Route::any('/application/{option}/generate', 'WebController\ApplicationController@generate_application');
	Route::any('/application/show_residency/{id}', 'WebController\ApplicationController@show_residency');
	Route::any('/application/show_residency/update_residency', 'WebController\ApplicationController@update_residency');



	
	// Api
	Route::any('/user_account/json', 'APIController\SystemConfigController@filter_user_account_by');
	Route::any('/signatories_list/json', 'APIController\SystemConfigController@signatories_list');
	Route::any('/filter_list/json', 'APIController\SystemConfigController@get_filter_list_by');
	Route::any('/registration_approval/datatables', 'APIController\SystemConfigController@system_config_datatables');
	Route::any('/residency/datatables/{id}', 'APIController\ApplicationController@residency_datatables');
	Route::any('/tec_application/json', 'APIController\ApplicationController@filter_tec_application_by');
	Route::any('/tec_application/datatables/attachment', 'APIController\ApplicationController@tec_application_datatables');
	Route::any('/tec_application/datatables', 'APIController\ApplicationController@tec_application_datatables');
	
	Route::any('/tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');
	Route::any('/tr_application/datatables/attachment', 'APIController\ApplicationController@tr_application_datatables');
	
	Route::any('/filter_tec_application/json', 'APIController\ApplicationController@filter_tec_application_by');
	Route::any('/filter_tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');
	
	Route::any('/rtt_application/json', 'APIController\ApplicationController@filter_rtt_application_by');
	Route::any('/rtt_application/datatables/attachment', 'APIController\ApplicationController@rtt_application_datatables');
	Route::any('/rtt_application/datatables', 'APIController\ApplicationController@rtt_application_datatables');
	
	Route::any('/section/json', 'APIController\ApplicationController@filter_section_by');
	Route::any('/section_upload/json', 'APIController\ApplicationController@filter_section_upload_by');
	
	Route::get('/esignature','APIController\ApplicationController@get_image_application');
	
	Route::any('/airlines/json', 'APIController\ApplicationController@filter_airline_by');
	
	Route::any('/reason_denials/json', 'APIController\ApplicationController@filter_reason_denial_by');
	
	Route::any('/applicant_fee/json', 'APIController\ApplicationController@filter_applicant_fee_by');
	
	Route::any('/chk_duplicate/json', 'APIController\ApplicationController@check_duplicate_name');
	Route::get('/certificate/{id}/{file}', [ function ($id, $file) {
		$folder_path = storage_path('tieza/tec/certificate/'.$id.'/'.$file);
		
	    if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => 'image/jpeg'));
	    
	    abort(404);
	}]);
	
	Route::get('tr/certificate/{id}/{file}', [ function ($id, $file) {
		
		$folder_path = storage_path('tieza/tr/certificate/'.$id.'/'.$file);
		
	    $explode_file = explode('.', $file);
		
	    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	    {
	    	$content = 'image/jpeg';
	    }
	    elseif(in_array($explode_file[1], ['pdf']))
	    {
			$content = 'application/pdf';
	    }
		
		if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	    abort(404);
	}]);
	
	Route::get('rtt/certificate/{id}/{file}', [ function ($id, $file) {
		
		$folder_path = storage_path('tieza/rtt/certificate/'.$id.'/'.$file);
		
	    $explode_file = explode('.', $file);
		
	    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	    {
			$content = 'image/jpeg';
	    }
	    elseif(in_array($explode_file[1], ['pdf']))
	    {
			$content = 'application/pdf';
	    }
		
		if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	    abort(404);
	}]);
	Route::get('/all_transaction/datatables', 'APIController\ApplicationController@all_transaction_datatables');
	Route::get('/home/all_transaction/datatables', 'APIController\ApplicationController@all_transaction_datatables');

});

