<?php

namespace App\Http\Controllers\WebController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\Application;
use DB;
use Illuminate\Http\Response;
use Carbon\Carbon;
use File;
use Mail;
use PDF;
use Spatie\PdfToImage\Pdf as SpatiePdf;
use Redirect;
class FullTaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->module = 'fulltax';
        $data = ['module'=>$this->module,'file'=>'application.fulltax.form','countries' => Application::get_countries_by()];
        return view('application.fulltax.index',$data);
    }
    // public function single_type()
    // {
    //     $this->module = 'fulltax';
    //     $data = ['module'=>$this->module,'file'=>'application.fulltax.form','countries' => Application::get_countries_by()];
    //     return view('application.fulltax.index',$data);
    // }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $count = 0;
        $total_count = count($request->xlast_name) - 1;
        for ($i=0; $i < count($request->xlast_name) ; $i++) { 
            if($request->xlast_name[$i] != null && $request->xamount[$i] != null){
                $count ++;
            }
        }
        if($count != $total_count) return back()->with('error','The system encountered an error, please try again.');
        $fulltax_no = self::getFulltaxReference();
        try {
            for ($i=0; $i < count($request->xlast_name) ; $i++) { 
                if($request->xlast_name[$i] != null && $request->xamount[$i] != null) {
                    $details =[
                        'last_name'         =>      strtoupper($request->xlast_name[$i]),
                        'first_name'        =>      strtoupper($request->xfirst_name[$i]),
                        'middle_name'       =>      strtoupper($request->xmiddle_name[$i]),
                        'ext_name'          =>      strtoupper($request->xext_name[$i]),
                        'class'             =>      $request->xclass_type[$i],
                        'fulltax_amount'    =>      $request->xamount[$i],
                        'passport_no'       =>      strtoupper($request->xpassport_no[$i]),
                        'ticket_no'         =>      strtoupper($request->xticket_no[$i]),
                        'departure_date'    =>      $request->xdeparture[0],
                        'type'              =>      $request->xclass_type[$i],
                        'airlines_id'       =>      $request->xdestination[0] ?? 0,
                        'created_by'        =>      auth()->user()->id,
                        'fulltax_no'        =>      $fulltax_no,
                        'mobile_no'         =>      $request->xmobile_no[0],
                        'email_address'     =>      $request->xemail_address[0],
                        'created_at'        =>      Carbon::now(),
                    ];
                    DB::beginTransaction();
                    DB::table('fulltax_application')
                    ->insert($details);
                    $LastID = DB::getPdo()->lastInsertId();
                    DB::commit();
                }
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        $details = DB::table('fulltax_application')->where('fulltax_no',$fulltax_no)->get();
        // $details->departure_date =  (new Carbon($details->departure_date))->format('M d, Y');
        $passenger_ticket = array();
        $processing_fee = 0;
        foreach ($details as $item) {
            $amount_wo += $item->fulltax_amount ;
            $processing_fee += 50;
            $total_amount += $item->fulltax_amount + 50 ;
            $passenger_ticket[] = array(
                'passenger_name'=>$item->first_name.' '.$item->middle_name.' '.$item->last_name.' '.$item->ext_name??'',
                'ticket_no'=>$item->ticket_no,
            );
        }
       
        $to_json = json_encode($passenger_ticket);
        $no_pax = count($details);
        $datenow = Carbon::now();
        $data = [
            'amount' => $total_amount,
            'currency' => 'PHP',
            'customer_email' => $request->xemail_address[0],
            'customer_name' => strtoupper($request->xfirst_name[0].' '.$request->xmiddle_name[0].' '.$request->xlast_name[0].' '.$request->xext_name[0] ?? ''),
            // 'customer_id' => $request->customer_id,
            'order_no' => $fulltax_no,
            'product_description' => 'Full Travel Tax',
            'remarks' => 'Full Tax Payment ('.$no_pax.' PAX) ',
            // 'service_id' => 'SPH00000113',     //stg 
            'service_id' => 'SPH00000036',      //live
            'transaction_time' => $datenow->toDateTimeString(),
            'ticket_details_remarks' => $to_json,
            // 'allow_payment_method' => 'Bank Transfer',
            ];
          
            //sort the parameter key alphanumeric ascending
            ksort($data);
            //concatenate the data
            $data_string = '';
            foreach ($data as $key => $value) {
                $data_string .= $value . '|';
            }
            $data_string = substr($data_string, 0, -1);
            info($data_string);
            $private_key = openssl_pkey_get_private(File::get(public_path('pgw-key.pem')));
            openssl_sign($data_string, $signature, $private_key, OPENSSL_ALGO_SHA256);
            $signature = strtoupper(bin2hex($signature));
            
            info($signature);
       
        $view = 'application.payment.index';
        $data = ['module'=> 'fulltax', 'file' => 'application.payment.confirmation','details'=> $data,'checksum'=>$signature,'processing_fee'=>$processing_fee,'amount_wo'=>$amount_wo]; 
        return view($view,$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function payment_success(Request $request)
    {
        info($request);   
        $status =  $request->status_code;
        $payment_channel = $request->payment_channel;
        $payment_reference_no = $request->reference_no;

        $fulltax = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)->first();
        $details = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)->get();
        
        

        if($status == "00" &&  $fulltax->payment_status != "00" && $fulltax->ar_no == "") 
        {
            $amount = 0;
            $fee = 0;
            $total = 0;
            foreach ($details as $val) {
                $amount += $val->fulltax_amount;
                $fee += 50;
                $total += $val->fulltax_amount + 50;
            }
            $data = array(
                'fulltax' => $fulltax,
                'details' => $details,
                'payment_channel' => $payment_channel,
                'payment_reference_no' => $payment_reference_no,
                'order_no' => $request->order_no,
                'ar_no' => self::getARReference(),
                'f_amount' => $amount,
                'f_fee' => $fee,
                'f_total' => $total,
            );
             $ft = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)
            ->where('created_at',$fulltax->created_at)
            ->update([
                'ar_no' => $data['ar_no'],
                'payment_channel' => $data['payment_channel'],
                'payment_status' => $request->status_code,
                'payment_transaction_time' => $request->payment_transaction_time,
                'payment_reference_no' => $data['payment_reference_no'],
            ]);
            $to_email =[$fulltax->email_address,'traveltaxonline@tieza.gov.ph'];
            $view = 'mail.ar_report';
            $pdf = PDF::setPaper('a4', 'portrait')->setOptions(['dpi' => 100, 'defaultFont' => 'cambria', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView($view, $data)->setWarnings(false);
            $folder_path = storage_path('tieza/fulltax/ar_cert/'.$fulltax->fulltax_no);
            if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);
            $filename = $fulltax->fulltax_no.'_arcert';
            $pdf->save($folder_path.'/'.$filename.'.pdf');
            $pdf = new SpatiePdf($folder_path.'/'.$filename.'.pdf');
            $pdf->saveImage($folder_path.'/'.$filename.'.png');
            $generate_image = $folder_path.'/'.$filename.'.png';
    
            info($data);
            $subject = 'TIEZA Acknowledgement Receipt #'.$data['ar_no'];
            Mail::send('mail.ar_main', $data, function($message) use ($to_email,$generate_image,$subject){
                $message->from("no-reply.traveltax@tieza.gov.ph",'no-reply.traveltax@tieza.gov.ph'); 
                $message->to($to_email)->subject($subject);
                $message->attach($generate_image, ['as' => 'AR CERTIFICATE']);
            });
        }
        else{
            $ft = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)
            ->update([
                'payment_channel' => $payment_channel,
                'payment_status' => $request->status_code,
                'payment_transaction_time' => $request->payment_transaction_time,
                'payment_reference_no' => $payment_reference_no,
            ]);
        }
        
        
        $view = 'application.payment.index';
        $data = ['file' => 'application.payment.form','response'=>$request]; 
        return view($view,$data);
    }
    public function payment_failed(Request $request)
    {
        $status =  $request->status_code;
        $payment_channel = $request->payment_channel;
        $payment_reference_no = $request->reference_no;

        $fulltax = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)->first();
        $details = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)->get();
      
        if($status == "00" && $fulltax->payment_status != "00"  && $fulltax->ar_no == "")
        {
            $amount = 0;
            $fee = 0;
            $total = 0;
            foreach ($details as $val) {
                $amount += $val->fulltax_amount;
                $fee += 50;
                $total += $val->fulltax_amount + 50;
            }
            $data = array(
                'fulltax' => $fulltax,
                'details' => $details,
                'payment_channel' => $payment_channel,
                'payment_reference_no' => $payment_reference_no,
                'order_no' => $request->order_no,
                 'ar_no' => self::getARReference(),
                 'f_amount' => $amount,
                 'f_fee' => $fee,
                 'f_total' => $total,
            );
             $ft = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)
            ->where('created_at',$fulltax->created_at)
            ->update([
                'ar_no' => $data['ar_no'],
                'payment_channel' => $data['payment_channel'],
                'payment_status' => $request->status_code,
                'payment_transaction_time' => $request->payment_transaction_time,
                'payment_reference_no' => $data['payment_reference_no'],
            ]);
            $to_email =[$fulltax->email_address,'traveltaxonline@tieza.gov.ph'];
            $view = 'mail.ar_report';
            $pdf = PDF::setPaper('a4', 'portrait')->setOptions(['dpi' => 100, 'defaultFont' => 'cambria', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView($view, $data)->setWarnings(false);
            $folder_path = storage_path('tieza/fulltax/ar_cert/'.$fulltax->fulltax_no);
            if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);
            $filename = $fulltax->fulltax_no.'_arcert';
            $pdf->save($folder_path.'/'.$filename.'.pdf');
            $pdf = new SpatiePdf($folder_path.'/'.$filename.'.pdf');
            $pdf->saveImage($folder_path.'/'.$filename.'.png');
            $generate_image = $folder_path.'/'.$filename.'.png';
    
            info($data);
            $subject = 'TIEZA Acknowledgement Receipt #'.$data['ar_no'];
            Mail::send('mail.ar_main', $data, function($message) use ($to_email,$generate_image,$subject){
                $message->from("no-reply.traveltax@tieza.gov.ph",'no-reply.traveltax@tieza.gov.ph'); 
                $message->to($to_email)->subject($subject);
                $message->attach($generate_image, ['as' => 'AR CERTIFICATE']);
            });
        }
        info($data);
        $view = 'application.payment.index';
        $data = ['file' => 'application.payment.status','response'=>$request]; 
        return view($view,$data);

    }
    public function getFulltaxReference(){
        $ft = DB::table('fulltax_application')->orderBy('fulltax_no','desc')->first();
      
        $last_ticket = $ft->fulltax_no ?? 0;
		$tick_date	= substr($last_ticket, 0, 8);
		$tick_date_now = date('Ymd');
		$tick_pre 	= "FT";
		$tick_start = "000";
		$tick_num	= substr($last_ticket, 10, 13);
		
		if ($tick_date == $tick_date_now) { 
			$tick_d1 = $tick_date; 
			$tick_d2 = $tick_pre;
			$tick_dx = $tick_num + 1;
			$tick_d3 = str_pad($tick_dx, 3, '0', STR_PAD_LEFT);
		} else { 
			$tick_d1 = $tick_date_now;
		    $tick_d2 = $tick_pre;
			$tick_d3 = $tick_start;
		}
        $digits = 3;
        
		//  return $tick_dt = $tick_d1.$tick_d2.$tick_d3;
        return $tick_dt  = $tick_d1.$tick_d2.$tick_d3;
    }
    public function getARReference(){
        $ft = DB::table('fulltax_application')
        ->wherenotnull('ar_no')
        ->orderBy('ar_no','desc')->first();
      
        $last_ticket = $ft->ar_no ?? 0;
		$tick_date	= substr($last_ticket, 0, 2);
		$tick_date_now = date('y');
		$tick_start = "00000";
		$tick_num	= substr($last_ticket, 3, 7);
		
		if ($tick_date == $tick_date_now) { 
			$tick_d1 = $tick_date; 
			$tick_dx = $tick_num + 1;
			$tick_d3 = str_pad($tick_dx, 5, '0', STR_PAD_LEFT);
		} else { 
			$tick_d1 = $tick_date_now;
			$tick_d3 = $tick_start;
		}
        return $tick_dt  = $tick_d1.$tick_d3;
    }
    public function resend_ar($id)
    {
        $fulltax = DB::table('fulltax_application')->where('fulltax_no',$id)->first();
        $details = DB::table('fulltax_application')->where('fulltax_no',$id)->get();
        $amount = 0;
        $fee = 0;
        $total = 0;
        foreach ($details as $val) {
            $amount += $val->fulltax_amount;
            $fee += 50;
            $total += $val->fulltax_amount + 50;
        }
        $data = array(
            'fulltax' => $fulltax,
            'details' => $details,
            'f_amount' => $amount,
            'f_fee' => $fee,
            'f_total' => $total,
        );

        $to_email =[$fulltax->email_address,'traveltaxonline@tieza.gov.ph'];
        $view = 'mail.ar_report';
        $pdf = PDF::setPaper('a4', 'portrait')->setOptions(['dpi' => 100, 'defaultFont' => 'cambria', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView($view, $data)->setWarnings(false);
        $folder_path = storage_path('tieza/fulltax/ar_cert/'.$fulltax->fulltax_no);
        if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);
        $filename = $fulltax->fulltax_no.'_arcert';
        $pdf->save($folder_path.'/'.$filename.'.pdf');
        $pdf = new SpatiePdf($folder_path.'/'.$filename.'.pdf');
        $pdf->saveImage($folder_path.'/'.$filename.'.png');
        $generate_image = $folder_path.'/'.$filename.'.png';

        info($data);
        $subject = 'TIEZA Acknowledgement Receipt #'.$fulltax->ar_no;
        Mail::send('mail.ar_main', $data, function($message) use ($to_email,$generate_image,$subject){
            $message->from("no-reply.traveltax@tieza.gov.ph",'no-reply.traveltax@tieza.gov.ph'); 
            $message->to($to_email)->subject($subject);
            $message->attach($generate_image, ['as' => 'AR CERTIFICATE']);
        });

        return Redirect::to("/view_ar/".$id);
    }
}

