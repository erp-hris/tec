<?php

namespace App\Http\Controllers\WebController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\Application;
use DB;
use Illuminate\Http\Response;
use Carbon\Carbon;
use File;
use Mail;
use PDF;
use Spatie\PdfToImage\Pdf as SpatiePdf;
use App\Http\Traits\System_Config;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Yajra\DataTables\Facades\DataTables;


class ApplicantController extends Controller
{
    use Application, System_Config;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tec_index()
    {
        $this->module = 'applicant';
        $data = ['module'=>$this->module,'file'=>'application.applicant.tec.form','countries' => Application::get_countries_by(),'list_sections' => $this->get_section_by(), 'list_reason_denials' => $this->get_reason_denials_by(), 'list_type_applicants' => $this->get_type_applicants_by(), 'list_airlines' => $this->get_airlines_by()];
        return view('application.applicant.tec.index',$data);
    }
    public function rtt_index(){
        $this->module = 'applicant';
        $data = ['module'=>$this->module,'file'=>'application.applicant.rtt.form','list_users' => $this->get_user_account(null, 3, 1), 'application_status' => $this->application_status, 'countries' => $this->get_countries_by(), 'list_processors' => $this->get_user_account(null, 2, 1), 'list_sections' => $this->get_section_by(null , [3]), 'list_reason_denials' => $this->get_reason_denials_by(), 'list_type_applicants' => $this->get_type_applicants_by(null, 3), 'list_airlines' => $this->get_airlines_by(), 'list_supervisor' => $this->get_user_account(null, 1, 1), 'type_applicant_reduced_list' => $this->type_applicant_reduced_list, 'type_accomodation' => $this->type_accomodation];
        return view('application.applicant.rtt.index',$data);
    }

    public function list_online_index(){
        $this->module = 'applicant';
        $data = ['module'=>$this->module,'file'=>'application.applicant.list.form','list_users' => $this->get_user_account(null, 3, 1), 'application_status' => $this->application_status, 'countries' => $this->get_countries_by(), 'list_processors' => $this->get_user_account(null, 2, 1), 'list_sections' => $this->get_section_by(null , [3]), 'list_reason_denials' => $this->get_reason_denials_by(), 'list_type_applicants' => $this->get_type_applicants_by(null, 3), 'list_airlines' => $this->get_airlines_by(), 'list_supervisor' => $this->get_user_account(null, 1, 1), 'type_applicant_reduced_list' => $this->type_applicant_reduced_list, 'type_accomodation' => $this->type_accomodation];
        return view('application.applicant.list.index',$data);
    }
    


    
    public function save_onlinetec(Request $request){

        $last_name = ucwords($request->get('last_name'));
        $first_name = ucwords($request->get('first_name'));
        $middle_name = ucwords($request->get('middle_name'));
        $country_id = $request->get('destination');
        $date_flight = $request->get('departure_date');
        $file_token = Str::random(50);
       
        if($id_picture_2x2 = Input::File('id_picture_2x2'))
        {
            $id_picture_2x2_file_extension = strtolower($id_picture_2x2->getClientOriginalExtension());

            $id_picture_2x2_file_name = 'id_picture_2x2_'.date('Ymd').'_'.$file_token.'.'.$id_picture_2x2_file_extension;

            $data['id_picture_2x2_fn'] = $id_picture_2x2_file_name;
         
        }

        $passport_identification_page_files = array();

        if($passport_identification_page = Input::File('passport_identification_page'))
        {
            $count = 0;
            foreach($passport_identification_page as $key => $val)
            {
                $count ++;                
                $passport_identification_page_file_extension = strtolower($val->getClientOriginalExtension());

                $passport_identification_page_file_name = 'passport_identification_page_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$passport_identification_page_file_extension;

                $passport_identification_page_files[] = ['file' => Input::File('passport_identification_page.'.$key), 'name' => $passport_identification_page_file_name];
              
            }
        }

        $ticket_booking_ref_no_files = array();

        if($ticket_booking_ref_no_ = Input::File('ticket_booking_ref_no_'))
        {
            foreach($ticket_booking_ref_no_ as $key => $val)
            {
                $ticket_booking_ref_no_extension = strtolower($val->getClientOriginalExtension());

                $ticket_booking_ref_no_file_name = 'ticket_booking_ref_no_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$ticket_booking_ref_no_extension;

                $ticket_booking_ref_no_files[] = ['file' => Input::File('ticket_booking_ref_no_.'.$key), 'name' => $ticket_booking_ref_no_file_name];
            }
        }

        $additional_files = array();

        if($additional_file = Input::File('additional_file'))
        {
            foreach($additional_file as $key => $val)
            {
                $additional_file_extension = strtolower($val->getClientOriginalExtension());

                $additional_file_name = 'additional_file_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$additional_file_extension;

                $additional_files[] = ['file' => Input::File('additional_file.'.$key), 'name' => $additional_file_name];
            }
        }

        /*********** Type of Applicant Uploads  **************/

        $files_0 = array();

        if($file_0 = Input::File('file_0'))
        {
            foreach($file_0 as $key => $val)
            {
                $file_0_extension = strtolower($val->getClientOriginalExtension());

                $file_0_name = 'file_0_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_0_extension;

                $files_0[] = ['file' => Input::File('file_0.'.$key), 'name' => $file_0_name];
            }
        }

        $files_1 = array();

        if($file_1 = Input::File('file_1'))
        {
            foreach($file_1 as $key => $val)
            {
                $file_1_extension = strtolower($val->getClientOriginalExtension());

                $file_1_name = 'file_1_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_1_extension;

                $files_1[] = ['file' => Input::File('file_1.'.$key), 'name' => $file_1_name];
            }
        }

        $files_2 = array();

        if($file_2 = Input::File('file_2'))
        {
            foreach($file_2 as $key => $val)
            {
                $file_2_extension = strtolower($val->getClientOriginalExtension());

                $file_2_name = 'file_2_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_2_extension;

                $files_2[] = ['file' => Input::File('file_2.'.$key), 'name' => $file_2_name];
            }
        }

        $files_3 = array();

        if($file_3 = Input::File('file_3'))
        {
            foreach($file_3 as $key => $val)
            {
                $file_3_extension = strtolower($val->getClientOriginalExtension());

                $file_3_name = 'file_3_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_3_extension;

                $files_3[] = ['file' => Input::File('file_3.'.$key), 'name' => $file_3_name];
            }
        }
        
        $LastID = DB::table('tec_application')->insertGetID([
            'last_name' => $last_name,
            'first_name' => $first_name,
            'middle_name' => $middle_name,
            'email' => $request->get('email_address'),
            'mobile_no' => $request->get('mobile_no'),
            'passport_no' => $request->get('passport'),
            'ticket_no' => $request->get('ticket'),
            'date_ticket_issued' => $request->get('departure_date'),
            'country_id' => $country_id,
            'date_validity' => $request->get('tec_validity'),
            'date_flight' => $date_flight,
            'applicant_type_id' => $request->get('type_applicant'),
            'airlines_id' => $request->get('airlines'),
            'date_application' => \Carbon\Carbon::now(),
            'id_picture_2x2_fn' => $id_picture_2x2_file_name ?? "",
            'is_onsite' => "0",

            // 'process_time' => $request->get('process_time') ?? '', 
            // 'cancel_remarks' => $request->get('cancel_remarks') ?? '', 
        ]);
        $folder_path = storage_path('tieza/tec/attachment/'.$LastID);
        if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

        if($id_picture_2x2 = Input::File('id_picture_2x2'))   $request->file('id_picture_2x2')->move($folder_path, $id_picture_2x2_file_name);
           

        if($passport_identification_page = Input::File('passport_identification_page'))
        {
            foreach($passport_identification_page as $key => $val)
            {
                $val->move($folder_path, $passport_identification_page_files[$key]['name']);  

                DB::table('tec_application_upload')
                ->insert([
                'tec_id' => $LastID,
                'section_id' => 0,
                'file_name' => $passport_identification_page_files[$key]['name'],
                // 'upload_by' => Auth::user()->id
                ]);
            }

        }

        if($ticket_booking_ref_no_ = Input::File('ticket_booking_ref_no_'))
        {
            foreach($ticket_booking_ref_no_ as $key => $val)
            {
                $val->move($folder_path, $ticket_booking_ref_no_files[$key]['name']);  

                DB::table('tec_application_upload')
                ->insert([
                'tec_id' => $LastID,
                'section_id' => 0,
                'file_name' => $ticket_booking_ref_no_files[$key]['name'],
                // 'upload_by' => Auth::user()->id
                ]);
            }
        }

        if($additional_file = Input::File('additional_file'))
        {
            foreach($additional_file as $key => $val)
            {
                $val->move($folder_path, $additional_files[$key]['name']);  

                DB::table('tec_application_upload')
                ->insert([
                'tec_id' => $LastID,
                'section_id' => 0,
                'file_name' => $additional_files[$key]['name'],
                // 'upload_by' => Auth::user()->id
                ]);
            }
        }

        if($file_0 = Input::File('file_0'))
        {
            foreach($file_0 as $key => $val)
            {
                $val->move($folder_path, $files_0[$key]['name']);  

                DB::table('tec_application_upload')
                ->insert([
                'tec_id' => $LastID,
                'section_id' => $request->get('type_applicant'),
                'file_name' => $files_0[$key]['name'],
                // 'upload_by' => Auth::user()->id
                ]);
            }
        }

        if($file_1 = Input::File('file_1'))
        {
            foreach($file_1 as $key => $val)
            {
                $val->move($folder_path, $files_1[$key]['name']);  

                DB::table('tec_application_upload')
                ->insert([
                'tec_id' => $LastID,
                'section_id' => $request->get('type_applicant'),
                'file_name' => $files_1[$key]['name'],
                // 'upload_by' => Auth::user()->id
                ]);
            }
        }

        if($file_2 = Input::File('file_2'))
        {
            foreach($file_2 as $key => $val)
            {
                $val->move($folder_path, $files_2[$key]['name']);  

                DB::table('tec_application_upload')
                ->insert([
                'tec_id' => $LastID,
                'section_id' => $request->get('type_applicant'),
                'file_name' => $files_2[$key]['name'],
                // 'upload_by' => Auth::user()->id
                ]);
            }
        }

        if($file_3 = Input::File('file_3'))
        {
            foreach($file_3 as $key => $val)
            {
                $val->move($folder_path, $files_3[$key]['name']);  

                DB::table('tec_application_upload')
                ->insert([
                'tec_id' => $LastID,
                'section_id' => $request->get('type_applicant'),
                'file_name' => $files_3[$key]['name'],
                // 'upload_by' => Auth::user()->id
                ]);
            }
        }   
         return back()->with('save-success-online','Save successfully! Please wait for your approval notification to your email.');


       
    }
    public function save_onlinertt(Request $request){

                /*********** File Upload **********/

                $file_token = Str::random(50);

                if($id_picture_2x2 = Input::File('id_picture_2x2'))
                {
                    $id_picture_2x2_file_extension = strtolower($id_picture_2x2->getClientOriginalExtension());

                    $id_picture_2x2_file_name = 'id_picture_2x2_'.date('Ymd').'_'.$file_token.'.'.$id_picture_2x2_file_extension;

                    $data['id_picture_2x2_fn'] = $id_picture_2x2_file_name;
                }

                $passport_identification_page_files = array();

                if($passport_identification_page = Input::File('passport_identification_page'))
                {
                    foreach($passport_identification_page as $key => $val)
                    {
                        $passport_identification_page_file_extension = strtolower($val->getClientOriginalExtension());

                        $passport_identification_page_file_name = 'passport_identification_page_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$passport_identification_page_file_extension;

                        $passport_identification_page_files[] = ['file' => Input::File('passport_identification_page.'.$key), 'name' => $passport_identification_page_file_name];
                    }
                }

                $oec_files = array();

                if($oec = Input::File('oec'))
                {
                    foreach($oec as $key => $val)
                    {
                        $oec_file_extension = strtolower($val->getClientOriginalExtension());

                        $oec_file_name = 'oec_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$oec_file_extension;

                        $oec_files[] = ['file' => Input::File('oec.'.$key), 'name' => $oec_file_name];
                    }
                }

                $ticket_booking_ref_no_files = array();

                if($ticket_booking_ref_no_ = Input::File('ticket_booking_ref_no_'))
                {
                    foreach($ticket_booking_ref_no_ as $key => $val)
                    {
                        $ticket_booking_ref_no_extension = strtolower($val->getClientOriginalExtension());

                        $ticket_booking_ref_no_file_name = 'ticket_booking_ref_no_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$ticket_booking_ref_no_extension;

                        $ticket_booking_ref_no_files[] = ['file' => Input::File('ticket_booking_ref_no_.'.$key), 'name' => $ticket_booking_ref_no_file_name];
                    }
                }

                $additional_files = array();

                if($additional_file = Input::File('additional_file'))
                {
                    foreach($additional_file as $key => $val)
                    {
                        $additional_file_extension = strtolower($val->getClientOriginalExtension());

                        $additional_file_name = 'additional_file_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$additional_file_extension;

                        $additional_files[] = ['file' => Input::File('additional_file.'.$key), 'name' => $additional_file_name];
                    }
                }

                /*********** Type of Applicant Uploads  **************/

                $files_0 = array();

                if($file_0 = Input::File('file_0'))
                {
                    foreach($file_0 as $key => $val)
                    {
                        $file_0_extension = strtolower($val->getClientOriginalExtension());

                        $file_0_name = 'file_0_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_0_extension;

                        $files_0[] = ['file' => Input::File('file_0.'.$key), 'name' => $file_0_name];
                    }
                }

                $files_1 = array();

                if($file_1 = Input::File('file_1'))
                {
                    foreach($file_1 as $key => $val)
                    {
                        $file_1_extension = strtolower($val->getClientOriginalExtension());

                        $file_1_name = 'file_1_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_1_extension;

                        $files_1[] = ['file' => Input::File('file_1.'.$key), 'name' => $file_1_name];
                    } 
                }

                $files_2 = array(); 

                if($file_2 = Input::File('file_2'))
                {
                    foreach($file_2 as $key => $val)
                    {
                        $file_2_extension = strtolower($val->getClientOriginalExtension());

                        $file_2_name = 'file_2_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_2_extension;

                        $files_2[] = ['file' => Input::File('file_2.'.$key), 'name' => $file_2_name];
                    }
                }

                $files_3 = array();

                if($file_3 = Input::File('file_3'))
                {
                    foreach($file_3 as $key => $val)
                    {
                        $file_3_extension = strtolower($val->getClientOriginalExtension());

                        $file_3_name = 'file_3_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_3_extension;

                        $files_3[] = ['file' => Input::File('file_3.'.$key), 'name' => $file_3_name];
                    }
                }

                /************ Save to Database ************/

                $last_name = ucwords($request->get('last_name'));
                $first_name = ucwords($request->get('first_name'));
                $middle_name = ucwords($request->get('middle_name'));
                $country_id = $request->get('country_designation');
                $date_flight = $request->get('date_flight');
                $list_type_applicant = $request->get('list_type_applicant');
                $type_accomodation = $request->get('type_accomodation'); 
                $rtt_amount =$this->get_applicant_fee('no', $type_accomodation, $list_type_applicant); 
                $LastID = DB::table('rtt_application')->insertGetID([
                //'user_id' => $request->get('applicant_name'),
                'last_name' => $last_name,
                'first_name' => $first_name,
                'middle_name' => $middle_name,
                'email' => $request->get('email_address'),
                'mobile_no' => $request->get('mobile_no'),
                'passport_no' => $request->get('passport'),
                'ticket_no' => $request->get('ticket'),
                'date_ticket_issued' => $request->get('ticket_date'),
                'country_id' => $country_id,
                'date_validity' => $request->get('date_validity'),
                'date_flight' => $date_flight,
                'applicant_type_id' => $list_type_applicant,
                'type_applicant_id' => $request->get('type_applicant'),
                'airlines_id' => $request->get('airlines_name'),
                'type_reduced_id' => $type_accomodation,
                'process_time' => $request->get('process_time') ?? '',
                'is_minor' => $request->get('is_minor'),
                'birthdate' => $request->get('birthdate'),
                'reduced_amount' =>$rtt_amount->amount,
                'id_picture_2x2_fn' => $id_picture_2x2_file_name ?? "",
                'is_onsite' => "0",
                'date_application' => \Carbon\Carbon::now(),
    
                ]);
              
             

                $data['reduced_amount'] = $rtt_amount->amount;

                $folder_path = storage_path('tieza/rtt/attachment/'.$LastID);

                    if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

                    if($id_picture_2x2 = Input::File('id_picture_2x2')) $request->file('id_picture_2x2')->move($folder_path, $id_picture_2x2_file_name);

                    if($passport_identification_page = Input::File('passport_identification_page'))
                    {
                        foreach($passport_identification_page as $key => $val)
                        {
                            $val->move($folder_path, $passport_identification_page_files[$key]['name']);  

                            DB::table('rtt_application_upload')
                            ->insert([
                            'rtt_id' => $LastID,
                            'section_id' => 0,
                            'file_name' => $passport_identification_page_files[$key]['name'],
                            // 'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($oec = Input::File('oec'))
                    {
                        foreach($oec as $key => $val)
                        {
                            $val->move($folder_path, $oec_files[$key]['name']);  

                            DB::table('rtt_application_upload')
                            ->insert([
                            'rtt_id' => $LastID,
                            'section_id' => 0,
                            'file_name' => $oec_files[$key]['name'],
                            // 'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($ticket_booking_ref_no_ = Input::File('ticket_booking_ref_no_'))
                    {
                        foreach($ticket_booking_ref_no_ as $key => $val)
                        {
                            $val->move($folder_path, $ticket_booking_ref_no_files[$key]['name']);  

                            DB::table('rtt_application_upload')
                            ->insert([
                            'rtt_id' => $LastID,
                            'section_id' => 0,
                            'file_name' => $ticket_booking_ref_no_files[$key]['name'],
                            // 'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($additional_file = Input::File('additional_file'))
                    {
                        foreach($additional_file as $key => $val)
                        {
                            $val->move($folder_path, $additional_files[$key]['name']);  

                            DB::table('rtt_application_upload')
                            ->insert([
                            'rtt_id' => $LastID,
                            'section_id' => 0,
                            'file_name' => $additional_files[$key]['name'],
                            // 'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_0 = Input::File('file_0'))
                    {
                        foreach($file_0 as $key => $val)
                        {
                            $val->move($folder_path, $files_0[$key]['name']);  

                            DB::table('rtt_application_upload')
                            ->insert([
                            'rtt_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'file_name' => $files_0[$key]['name'],
                            // 'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_1 = Input::File('file_1'))
                    {
                        foreach($file_1 as $key => $val)
                        {
                            $val->move($folder_path, $files_1[$key]['name']);  

                            DB::table('rtt_application_upload')
                            ->insert([
                            'rtt_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'file_name' => $files_1[$key]['name'],
                            // 'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_2 = Input::File('file_2'))
                    {
                        foreach($file_2 as $key => $val)
                        {
                            $val->move($folder_path, $files_2[$key]['name']);  

                            DB::table('rtt_application_upload')
                            ->insert([
                            'rtt_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'file_name' => $files_2[$key]['name'],
                            // 'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_3 = Input::File('file_3'))
                    {
                        foreach($file_3 as $key => $val)
                        {
                            $val->move($folder_path, $files_3[$key]['name']);  

                            DB::table('rtt_application_upload')
                            ->insert([
                            'rtt_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'file_name' => $files_3[$key]['name'],
                            // 'upload_by' => Auth::user()->id
                            ]);
                        }
                    }   

                    DB::commit();

         return back()->with('save-success-online','Save successfully! Please wait for your approval notification to your email.');

    }
    public function online_filter_section_upload_by(request $request)
    {
        try
        {
            $section_upload_id = $request->get('section_upload_id');
            $section_id = $request->get('section_id');

            $query = DB::table('sections_upload as su');

            if($section_id) $query->where('su.section_id', $section_id);
            
            $response = array();

            if($section_upload_id)
            {
                $response['sections_upload'] = $query->where('su.id', $section_upload_id)->first(); 
            }
            else
            {
                $sections_upload = $query->get();

                $response['sections_uploads'] = $sections_upload;
                $response['count_sections_upload'] = count($sections_upload);
            }
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response($response);
    }
    public function online_filter_section_by(request $request){
        try
        {
            
            $section_id = $request->get('section_id');
            $section_type = $request->get('section_type');

            $query = DB::table('sections');

            if($section_type) $query->where('sections.section_type', $section_type);
            
            $response = array();

            if($section_id)
            {
                $response['section'] = $query->where('sections.id', $section_id)->first(); 
            }
            else
            {
                $sections = $query->get();

                $response['sections'] = $sections;
            }
        }
        catch(Exception $e)
        {
           
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response($response);
    }
    public function list_online_datatables()
    {
        try
        {
            $data = array();
            $data = $this->list_online_data();
        
            $datatables = Datatables::of($data);
            // ->addColumn('approve', function($data){
            //     $button = '<div class="tools">
            //     <button type="button"  class="btn btn-primary " onclick="approve_decline('.$data->id.',1)" ><i class="icon icon-left mdi mdi-check"></i>Approve</button> 
            //     <button type="button"  class="btn btn-danger "  onclick="approve_decline('.$data->id.',2)"><i class="icon icon-left mdi mdi-close-circle"></i>Decline</button>
            //     </div>';
              
            //     return $button;
            // })
            // ->addColumn('decline', function($data){
            //     $button = '<div class="tools">
            //      </div>';
              
            //     return $button;
            // });

            $rawColumns = ['trans_type', 'name','passport_no','departure_date'];

        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 201);
        }
       
        return $datatables->rawColumns($rawColumns)->make(true);
    }
    
}
