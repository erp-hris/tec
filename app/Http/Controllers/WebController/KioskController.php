<?php

namespace App\Http\Controllers\WebController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\Application;
use DB;
use Illuminate\Http\Response;
use Carbon\Carbon;

use SimpleSoftwareIO\QrCode\Facades\QrCode; 
use Illuminate\Support\Facades\Storage;
class KioskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->module = 'kiosk';

        $data = ['module'=>$this->module,'file'=>'application.kiosk_ft.form','countries' => Application::get_countries_by()];

        return view('application.kiosk_ft.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'departure_date' => 'required|date|date_format:Y-m-d',
        ]);

        //code...
        $data =[
            'last_name' =>$request->last_name,
            'first_name' =>$request->first_name,
            'middle_name' =>$request->middle_name,
            'ext_name' =>$request->ext_name,
            'class' =>$request->class_type,
            'fulltax_amount' =>$request->class_amt,
            'passport_no' =>$request->passport,
            'ticket_no' =>$request->ticket,
            'departure_date' =>$request->departure_date,
            'airlines_id' =>$request->ft_destination ?? null,
            'date_application' =>Carbon::now()->toDateTimeString(),
            'created_by' => auth()->user()->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ];

        DB::beginTransaction();
        DB::table('fulltax_application')
        ->insert($data);
        
        $LastID = DB::getPdo()->lastInsertId();
        DB::commit();

         $data = DB::table('fulltax_application as ft')->selectRaw('date_format(date_application,"%Y-%m-%d") as date_app,ft.*')->where('id',$LastID)->first();
         $data->departure_date =  (new Carbon($data->departure_date))->format('M d, Y');

        return response()->json([
            'details' =>$data ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function generate_qrcode($id)
    {
        $f_tax = DB::table('fulltax_application')
        ->find($id);

        $qrcode_msg = 'http://tec-onsite.test/view_application/fulltax/'.$f_tax->id;

         $pngImage = QrCode::format('png')->merge('img/tieza-logo.png', 0.3, true)
         ->size(500)->errorCorrection('H')
         ->generate($qrcode_msg);

         $output_file = '/tieza/fulltax/qr-code/'.$f_tax->id.'/_qrcode.png';
         Storage::disk('storage')->put($output_file, $pngImage);

         return response()->json([
            'details' =>$f_tax ], 201);
    }
}
