<?php

namespace App\Http\Traits;

use DB;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

trait Application
{
	protected $application_option = ['overview', 'tec_application', 'tr_application', 'tec_dashboard', 'tec_report', 'tr_dashboard', 'tr_report', 'reason_denied', 'rtt_dashboard', 'rtt_application', 'rtt_report','residency','add_residency'];

	protected $application_status = array(
	['id' => '1', 'name' => 'awaiting_documents'],
	['id' => '2', 'name' => 'approved'],
	['id' => '3', 'name' => 'denied'],
	['id' => '5', 'name' => 'cancelled'],

	);

	protected $sections = array(	
	['id' => '1', 'name' => 'travel_tax_refund'],
	['id' => '2', 'name' => 'standard_reduced_travel_tax'],
	['id' => '3', 'name' => 'privelege_reduced_travel_tax'],
	);

	protected $travel_tax_refund_form = array(
	['id' => '1', 'name' => 'tec'],
	['id' => '2', 'name' => 'cash_refund'],
	);

	protected $type_applicant_refund_list = array(
	['id' => '2', 'name' => 'Travel Tax Refund'],
	['id' => '3', 'name' => 'Standard Reduced Travel Tax'],
	['id' => '4', 'name' => 'Privileged Reduced Travel Tax']
	);

	protected $type_applicant_reduced_list = array(
	['id' => '3', 'name' => 'Standard Reduced Travel Tax'],
	['id' => '4', 'name' => 'Privileged Reduced Travel Tax']
	);

	protected $type_accomodation = array(
	['id' => '1', 'name' => 'First Class'],
	['id' => '2', 'name' => 'Economy']
	);

	protected $acknowledge_form_checklist = array(
	['id' => 0, 'name' => 'passport'],
	['id' => 1, 'name' => 'airline_ticket_confirmation'],
	['id' => 2, 'name' => 'tieza_official_receipt'],
	['id' => 3, 'name' => 'marriage_contract'],
	['id' => 4, 'name' => 'birth_certificate'],
	['id' => 5, 'name' => 'ofw_employment_certificate'],
	['id' => 6, 'name' => 'proof_permanent_residency'],
	['id' => 7, 'name' => 'travel_authority'],
	['id' => 8, 'name' => 'passport_applicable_arrival'],
	['id' => 9, 'name' => 'letter_request'],
	['id' => 10, 'name' => 'affidavit_name_descrepancy'],
	);

	protected $reason_refund = array(
	['id' => 1, 'name' => 'Exemption'],
	['id' => 2, 'name' => 'Reduced Rate'],
	['id' => 3, 'name' => 'Non Coverage'],
	['id' => 4, 'name' => 'Unused Ticket'],
	['id' => 5, 'name' => 'Double Payment'],
	['id' => 6, 'name' => 'Others']
	);

	protected $tec_reports = ['airlines_passenger', 'summary_section_tec', 'summary_certificate','summary_process_time','process_time_motion'];

	protected $rtt_reports = ['airlines_passenger', 'summary_section_rtt', 'summary_certificate','summary_process_time','process_time_motion'];
	protected $tr_reports =['airlines_passenger', 'summary_section_tr', 'summary_certificate','summary_process_time','process_time_motion'];


	protected function is_application_option_exist($option)
	{
		if(!in_array($option, $this->application_option)) throw new Exception(trans('page.not_found', ['attribute' => strtolower(trans('page.option'))]));

		return true;
	}

	protected function is_application_report_exist($report)
	{
		$list_reports = array_merge($this->tec_reports, $this->rtt_reports,$this->tr_reports);

		if(!in_array($report, $list_reports)) throw new Exception(trans('page.not_found', ['attribute' => strtolower(trans('page.report'))]));

		return true;
	}

	/***************************** TEC Application *********************************/

	protected function count_tec_application_by($id = null, $except = array(), $full_name = null, $destination = null, $date_flight = null, $status = null)
	{
		$query = DB::table('tec_application as ta')
		->where('ta.deleted_at', null);

		if($full_name) $query->where(DB::raw('CONCAT(ta.last_name, "-", ta.first_name, "-", ta.middle_name)'), 'LIKE', "%".$full_name."%");

		if($destination) $query->where('ta.country_id', $destination);

		if($date_flight) $query->where('ta.date_flight', $date_flight);

		if($except) $query->wherenotin('ta.id', $except);

		if($id) $query->where('ta.id', $id);

		if($status) $query->where('ta.status', $status);

		return $query->count();
	}

	protected function get_tec_application_by($id = null, $processor_id = null, $status = null ,$from_date = null, $to_date = null)
	{
		DB::enableQueryLog();
		$query = DB::table('tec_application as ta')
		->where('ta.deleted_at', null)
		->leftjoin('countries', 'countries.id', 'ta.country_id')
		->leftjoin('sections', 'sections.id', 'ta.applicant_type_id')
		->leftjoin('users as supervisor', 'supervisor.id', 'ta.supervisor_id')
		->leftjoin('users as processor', 'processor.id', 'ta.assign_processor_id')
		->leftjoin('reason_denials', 'reason_denials.id', 'ta.denial_id')
		->select('ta.*',DB::raw('if(substring(generate_to_email,1,3) = "TEC",substring(generate_to_email,11,8),"") AS _date'),DB::raw('CONCAT( SUBSTRING((SELECT _date),5,4),"-",
			SUBSTRING((SELECT _date),1,2),"-",
			SUBSTRING((SELECT _date),3,2)) AS _newdate'), DB::raw('CONCAT(ta.last_name,", ",ta.first_name," ", IFNULL(SUBSTRING(ta.middle_name, 1, 1), ""), IF(ISNULL(SUBSTRING(ta.middle_name, 1, 1)) or ta.middle_name != "", ".", "")) AS full_name'), DB::raw('CONCAT("TEC-", LPAD(ta.tec_no, 5, 0)) AS tec_id'), DB::raw('DATE_FORMAT(ta.date_application, "%d %b %Y") as date_application'), DB::raw('CONCAT(ta.first_name," ", IFNULL(SUBSTRING(ta.middle_name, 1, 1), ""), IF(ISNULL(SUBSTRING(ta.middle_name, 1, 1)) or ta.middle_name != "", ".", "")," ",ta.last_name) AS ffull_name'), 'countries.name as country_name', 'sections.code as section_code', 'supervisor.username as supervisor_code', 'processor.username as processor_code', 'reason_denials.name as reason_denial', DB::raw('CONCAT("APP-", LPAD(ta.id, 5, 0)) AS app_id'));

		if($status) $query->where('ta.status', $status);

		if($to_date)
		{
			if($from_date)
			{
				$query->havingRaw("DATE(_newdate) between ? and ? ",[$from_date,$to_date]);
				// $query->whereBetween(DB::raw('DATE(ta.date_application)'), [$from_date, $to_date]);
			}
			else
			{
				$query->havingRaw("DATE(_newdate) between ? and ? ",[date('Y-m-01'),$to_date]);
				// $query->whereBetween(DB::raw('DATE(ta.date_application)'), [date('Y-m-01'), $to_date]);
			}
		
		}
		if($processor_id) $query->where('ta.assign_processor_id', $processor_id);

		if($id)
		{
			$tec_application = $query->where('ta.id', $id)->first();
		
			return $tec_application;
		}
		else
		{
			

			return $query->get();
		}
	}

	protected function check_exist_tec_application($id, $processor_id = null)
	{
		if($this->count_tec_application_by($id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_tec_application_by($id, $processor_id);
	}

	protected function check_exist_applied_tec_application($id = null, $except = null, $full_name = null, $destination = null, $date_flight = null, $status = null)
	{
		if($this->count_tec_application_by($id, $except, $full_name, $destination, $date_flight, $status) > 0) 
		
		{
			if($id)
			{
				throw new Exception('This Application already exist.');
			}
			else
			{
				$query = DB::table('tec_application as ta')
				->where('ta.deleted_at', null)
				->select('ta.id', DB::raw('CONCAT("APP-", LPAD(ta.id, 5, 0)) AS app_id'));

				if($full_name) $query->where(DB::raw('CONCAT(ta.last_name, "-", ta.first_name, "-", ta.middle_name)'), 'LIKE', "%".$full_name."%");

				if($destination) $query->where('ta.country_id', $destination);

				if($date_flight) $query->where('ta.date_flight', $date_flight); 

				if($status) $query->where('ta.status', $status);

				if($full_name && !$destination && !$date_flight) $query->orderby('id', 'DESC'); // to get latest record; filter by name

				$tec_application = $query->first();
 
				throw new Exception("This Application already exist.<br> Please check Application No : <a href='javascript:void(0);' onclick='select_application(".$tec_application->id.")'>". $tec_application->app_id.'</a>');
			}
		}

		return true; 
	}

	protected function get_tec_application_upload($id = null, $tec_id = null, $section_id = null)
	{
		$query = DB::table('tec_application_upload as tau')
		->where('tau.deleted_at', null);

		if($tec_id) $query->where('tau.tec_id', $tec_id);

		if($section_id) $query->where('tau.section_id', $section_id);
       
		if($id)
		{
			return $query->where('tau.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}
	/***************************** RTT Application *********************************/

	protected function count_rtt_application_by($id = null, $except = array(), $full_name = null, $destination = null, $date_flight = null, $status = null)
	{
		$query = DB::table('rtt_application as ta')
		->where('ta.deleted_at', null);

		if($full_name) $query->where(DB::raw('CONCAT(ta.last_name, "-", ta.first_name, "-", ta.middle_name)'), 'LIKE', "%".$full_name."%");

		if($destination) $query->where('ta.country_id', $destination);

		if($date_flight) $query->where('ta.date_flight', $date_flight);

		if($except) $query->wherenotin('ta.id', $except);

		if($id) $query->where('ta.id', $id);

		if($status) $query->where('ta.status', $status);

		return $query->count();
	}

	protected function get_rtt_application_by($id = null, $processor_id = null, $status = null ,$from_date = null, $to_date = null)
	{
		$query = DB::table('rtt_application as ta')
		->where('ta.deleted_at', null)
		->leftjoin('countries', 'countries.id', 'ta.country_id')
		->leftjoin('sections', 'sections.id', 'ta.type_applicant_id')
		->leftjoin('users as supervisor', 'supervisor.id', 'ta.supervisor_id')
		->leftjoin('users as processor', 'processor.id', 'ta.assign_processor_id')
		->leftjoin('reason_denials', 'reason_denials.id', 'ta.denial_id')
		->select('ta.*', DB::raw('CONCAT(ta.last_name,", ",ta.first_name," ", IFNULL(SUBSTRING(ta.middle_name, 1, 1), ""), IF(ISNULL(SUBSTRING(ta.middle_name, 1, 1)) or ta.middle_name != "", ".", "")) AS full_name'), DB::raw('CONCAT("RTT-", LPAD(ta.rtt_no, 5, 0)) AS rtt_id'), DB::raw('DATE_FORMAT(ta.date_application, "%d %b %Y") as date_application'), DB::raw('CONCAT(ta.first_name," ", IFNULL(SUBSTRING(ta.middle_name, 1, 1), ""), IF(ISNULL(SUBSTRING(ta.middle_name, 1, 1)) or ta.middle_name != "", ".", "")," ",ta.last_name) AS ffull_name'), 'countries.name as country_name', 'sections.code as section_code', 'supervisor.username as supervisor_code', 'processor.username as processor_code', 'reason_denials.name as reason_denial', DB::raw('CONCAT("APP-", LPAD(ta.id, 5, 0)) AS app_id'));

		if($status) $query->where('ta.status', $status);

		if($to_date)
		{
			if($from_date)
			{
				$query->whereBetween('ta.date_application', [$from_date, $to_date]);
			}
			else
			{
				$query->whereBetween('ta.date_application', [date('Y-m-01'), $to_date]);
			}
		}

		if($processor_id) $query->where('ta.assign_processor_id', $processor_id);

		if($id)
		{
			$rtt_application = $query->where('ta.id', $id)->first();

			return $rtt_application;
		}
		else
		{
			return $query->get();
		}
	}

	protected function check_exist_rtt_application($id, $processor_id = null)
	{
		if($this->count_rtt_application_by($id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_rtt_application_by($id, $processor_id);
	}

	protected function check_exist_applied_rtt_application($id = null, $except = null, $full_name = null, $destination = null, $date_flight = null, $status = null)
	{
		if($this->count_rtt_application_by($id, $except, $full_name, $destination, $date_flight, $status) > 0) 
		{
			if($id)
			{
				throw new Exception('This Application already exist.');
			}
			else
			{
				$query = DB::table('rtt_application as ta')
				->where('ta.deleted_at', null)
				->select('ta.id', DB::raw('CONCAT("APP-", LPAD(ta.id, 5, 0)) AS app_id'));

				if($full_name) $query->where(DB::raw('CONCAT(ta.last_name, "-", ta.first_name, "-", ta.middle_name)'), 'LIKE', "%".$full_name."%");

				if($destination) $query->where('ta.country_id', $destination);

				if($date_flight) $query->where('ta.date_flight', $date_flight); 

				if($status) $query->where('ta.status', $status);

				if($full_name && !$destination && !$date_flight) $query->orderby('id', 'DESC'); // to get latest record; filter by name

				$rtt_application = $query->first();
 
				throw new Exception("This Application already exist.<br> Please check Application No : <a href='javascript:void(0);' onclick='select_application(".$rtt_application->id.")'>". $rtt_application->app_id.'</a>');
			} 
		}

		return true;
	}

	protected function get_rtt_application_upload($id = null, $rtt_id = null, $section_id = null)
	{
		$query = DB::table('rtt_application_upload as tau')
		->where('tau.deleted_at', null);

		if($rtt_id) $query->where('tau.rtt_id', $rtt_id);

		if($section_id) $query->where('tau.section_id', $section_id);

		if($id)
		{
			return $query->where('tau.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	/***************************** TR Application *********************************/

	protected function count_tr_application_by($id = null, $except = array(), $full_name = null, $destination = null, $date_flight = null)
	{
		$query = DB::table('tr_application as tr')
		->where('tr.deleted_at', null);

		if($full_name) $query->where(DB::raw('CONCAT(tr.last_name, "-", tr.first_name, "-", tr.middle_name)'), 'LIKE', "%".$full_name."%");

		if($destination) $query->where('tr.country_id', $destination);

		if($date_flight) $query->where('tr.date_flight', $date_flight);

		if($except) $query->wherenotin('tr.id', $except);

		if($id) $query->where('tr.id', $id);

		return $query->count();
	}

	protected function check_exist_tr_application($id, $processor_id = null)
	{
		if($this->count_tr_application_by($id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_tr_application_by($id, $processor_id);
	}

	protected function get_tr_application_by($id = null, $processor_id = null, $status = null ,$from_date = null, $to_date = null)
	{
		$query = DB::table('tr_application as tr')
		->where('tr.deleted_at', null)
		->leftjoin('countries', 'countries.id', 'tr.country_id')
		->leftjoin('sections', 'sections.id', 'tr.applicant_type_id')
		->leftjoin('users as supervisor', 'supervisor.id', 'tr.supervisor_id')
		->leftjoin('users as processor', 'processor.id', 'tr.assign_processor_id')
		->leftjoin('reason_denials', 'reason_denials.id', 'tr.denial_id')
		->select('tr.*', DB::raw('CONCAT(tr.last_name,", ",tr.first_name," ", IFNULL(SUBSTRING(tr.middle_name, 1, 1), ""), IF(ISNULL(SUBSTRING(tr.middle_name, 1, 1)) or tr.middle_name != "", ".", "")) AS full_name'), DB::raw('CONCAT("TR-", LPAD(tr.id, 5, 0)) AS tr_id'), DB::raw('DATE_FORMAT(tr.date_application, "%d %b %Y") as date_application'), DB::raw('CONCAT(tr.first_name," ", IFNULL(SUBSTRING(tr.middle_name, 1, 1), ""), IF(ISNULL(SUBSTRING(tr.middle_name, 1, 1)) or tr.middle_name != "", ".", "")," ",tr.last_name) AS ffull_name'), 'countries.name as country_name', 'sections.code as section_code', 'supervisor.username as supervisor_code', 'processor.username as processor_code', 'reason_denials.name as reason_denial', DB::raw('CONCAT("APP-", LPAD(tr.id, 5, 0)) AS app_id'), DB::raw('CONCAT(tr.guardian_last_name,", ",tr.guardian_first_name," ", IFNULL(SUBSTRING(tr.guardian_middle_name, 1, 1), ""), IF(ISNULL(SUBSTRING(tr.guardian_middle_name, 1, 1)) or tr.guardian_middle_name != "", ".", "")) AS guardian_full_name'), DB::raw('CONCAT(tr.guardian_first_name," ", IFNULL(SUBSTRING(tr.guardian_middle_name, 1, 1), ""), IF(ISNULL(SUBSTRING(tr.guardian_middle_name, 1, 1)) or tr.guardian_middle_name != "", ".", "")," ",tr.guardian_last_name) AS guardian_ffull_name'), DB::raw('CONCAT(supervisor.first_name," ", IFNULL(SUBSTRING(supervisor.middle_name, 1, 1), ""), IF(ISNULL(SUBSTRING(supervisor.middle_name, 1, 1)) or supervisor.middle_name != "", ".", "")," ",supervisor.last_name) AS supervisor_ffull_name'), DB::raw('CONCAT("ONREF-", YEAR(tr.date_application), "-", LPAD(tr.id, 5, 0)) AS ar_no'));

		if($status) $query->where('tr.status', $status);

		if($to_date)
		{
			if($from_date)
			{
				$query->whereBetween('tr.date_application', [$from_date, $to_date]);
			}
			else
			{
				$query->whereBetween('tr.date_application', [date('Y-d-00 00:00:00'), $to_date]);
			}
		}

		if($processor_id) $query->where('tr.assign_processor_id', $processor_id);

		if($id)
		{
			$tec_application = $query->where('tr.id', $id)->first();

			$tec_application->file_0 = null;
			$tec_application->file_1 = null;
			$tec_application->file_2 = null;
			$tec_application->file_3 = null;

			foreach($this->get_tec_application_upload(null, $tec_application->id, $tec_application->applicant_type_id) as $key => $val)
            {
                    $file_name = substr($val->file_name, 0, 6); //'file_'.$key;

                    $tec_application->$file_name =  $val->file_name;
            }

			return $tec_application;
		}
		else
		{
			return $query->get();
		}
	}

	protected function check_exist_applied_tr_application($id = null, $except = null, $full_name = null, $destination = null, $date_flight = null)
	{
		if($this->count_tr_application_by($id, $except, $full_name, $destination, $date_flight) > 0) throw new Exception('This Application already exist.');

		return true;
	}

	/***************************** Others *********************************/

	protected function get_section_by($id = null, $section_type = null)
	{
		$query = DB::table('sections');

		if($section_type) $query->wherein('section_type', $section_type);

		if($id)
		{
			return $query->where('sections.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function get_reason_denials_by($id = null)
	{
		$query = DB::table('reason_denials')
		->where('deleted_at', null);

		if($id)
		{
			return $query->where('reason_denials.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function count_reason_denials_by($id = null)
	{
		$query = DB::table('reason_denials')
		->where('deleted_at', null);

		if($id) $query->where('reason_denials.id', $id);
		
		return $query->count();
	}

	protected function check_exist_reason_denial($id)
	{
		if($this->count_reason_denials_by($id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_reason_denials_by($id);
	}	

	protected function get_type_applicants_by($id = null, $group = 1, $except = [])
	{
		$query = DB::table('sections');

		if($group) $query->where('sections.section_type', $group);

		if($except) $query->wherenotin('sections.section_type', $except);

		if($id)
		{
			return $query->where('sections.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	public function get_countries_by($id = null)
	{
		$query = DB::table('countries')
		->where('countries.deleted_at', null);

		if($id)
		{
			return $query->where('countries.id', $id)->first();
		}
		else
		{
			return $query->orderby('countries.name', 'asc')->get();
		}
	}

	protected function get_airlines_by($id = null)
	{
		$query = DB::table('airlines');

		if($id)
		{
			return $query->where('airlines.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function get_tec_attachment_by($id = null, $section_id = null, $tec_id = null, $option = null)
	{
		$query = DB::table('tec_application_upload as tau')
		->join('tec_application as ta', 'ta.id', 'tau.tec_id')
		->where('tau.deleted_at', null)
		->select('tau.*', 'ta.generate_to_email');

		if($option == 'passport') 
		{
			$query->where('tau.file_name', 'like', 'passport_identification_page%');
		}
		elseif($option == 'airline')
		{
			$query->where('tau.file_name', 'like', 'ticket_booking_ref_no%');
		}
		elseif($option == 'travel_tax_receipt')
		{
			$query->where('tau.file_name', 'like', 'travel_tax_receipt%');
		}
		elseif($option == 'additional')
		{
			$query->where('tau.file_name', 'like', 'additional_file%');
		}
		elseif($option == 'file_0')
		{
			$query->where('tau.file_name', 'like', 'file_0%');
		}
		elseif($option == 'file_1')
		{
			$query->where('tau.file_name', 'like', 'file_1%');
		}
		elseif($option == 'file_2')
		{
			$query->where('tau.file_name', 'like', 'file_2%');
		}
		elseif($option == 'file_3')
		{
			$query->where('tau.file_name', 'like', 'file_3%');
		}
			
		 if($section_id) $query->where('tau.section_id', $section_id);
		
		if($tec_id) $query->where('tau.tec_id', $tec_id);

		if($id)
		{
			return $query->where('tau.id', $id)->first();
		}
		else
		{
			
			return $query->get();
		}
	}

	protected function get_tr_attachment_by($id = null, $section_id = null, $tr_id = null, $option = null)
	{
		$query = DB::table('tr_application_upload as tau')
		->join('tr_application as ta', 'ta.id', 'tau.tr_id')
		->where('tau.deleted_at', null)
		->select('tau.*', 'ta.is_complete');

		if($option == 'passport') 
		{
			$query->where('tau.file_name', 'like', 'passport_identification_page%');
		}
		elseif($option == 'airline')
		{
			$query->where('tau.file_name', 'like', 'ticket_booking_ref_no%');
		}
		elseif($option == 'tieza_official_receipt')
		{
			$query->where('tau.file_name', 'like', 'tieza_official_receipt%');
		}
		elseif($option == 'additional')
		{
			$query->where('tau.file_name', 'like', 'additional_file%');
		}
		elseif($option == 'file_0')
		{
			$query->where('tau.file_name', 'like', 'file_0%');
		}
		elseif($option == 'file_1')
		{
			$query->where('tau.file_name', 'like', 'file_1%');
		}
		elseif($option == 'file_2')
		{
			$query->where('tau.file_name', 'like', 'file_2%');
		}
		elseif($option == 'file_3')
		{
			$query->where('tau.file_name', 'like', 'file_3%');
		}
		elseif($option == 'file_4')
		{
			$query->where('tau.file_name', 'like', 'file_4%');
		}
			
		if($section_id) $query->where('tau.section_id', $section_id);
		
		if($tr_id) $query->where('tau.tr_id', $tr_id);

		if($id)
		{
			return $query->where('tau.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function get_rtt_attachment_by($id = null, $section_id = null, $rtt_id = null, $option = null)
	{
		$query = DB::table('rtt_application_upload as rau')
		->join('rtt_application as ra', 'ra.id', 'rau.rtt_id')
		->where('rau.deleted_at', null)
		->select('rau.*', 'ra.generate_to_email');

		if($option == 'passport') 
		{
			$query->where('rau.file_name', 'like', 'passport_identification_page%');
		}
		elseif($option == 'oec') 
		{
			$query->where('rau.file_name', 'like', 'oec%');
		}
		elseif($option == 'airline')
		{
			$query->where('rau.file_name', 'like', 'ticket_booking_ref_no%');
		}
		elseif($option == 'tieza_official_receipt')
		{
			$query->where('rau.file_name', 'like', 'tieza_official_receipt%');
		}
		elseif($option == 'additional')
		{
			$query->where('rau.file_name', 'like', 'additional_file%');
		}
		elseif($option == 'file_0')
		{
			$query->where('rau.file_name', 'like', 'file_0%');
		}
		elseif($option == 'file_1')
		{
			$query->where('rau.file_name', 'like', 'file_1%');
		}
		elseif($option == 'file_2')
		{
			$query->where('rau.file_name', 'like', 'file_2%');
		}
		elseif($option == 'file_3')
		{
			$query->where('rau.file_name', 'like', 'file_3%');
		}
		elseif($option == 'file_4')
		{
			$query->where('rau.file_name', 'like', 'file_4%');
		}
			
		if($section_id) $query->where('rau.section_id', $section_id);
		
		if($rtt_id) $query->where('rau.rtt_id', $rtt_id);

		if($id)
		{
			return $query->where('rau.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function get_refund_amount_by($id = null)
	{
		$query = DB::table('tr_amount')
		->where('deleted_at', null);

		if($id)
		{
			return $query->where('tr_amount.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function get_bank_by($id = null)
	{
		$query = DB::table('banks')
		->where('banks.deleted_at', null);

		if($id)
		{
			return $query->where('banks.id')->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function count_attached_file_by($key_tag = null, $tr_id)
	{
		$query = DB::table('tr_application_upload as ta')
		->where('ta.tr_id', $tr_id)
		->where('ta.deleted_at', null);

		if($key_tag)
		{
			if($key_tag == 'passport')
			{
				$query->where('ta.file_name', 'like', 'passport%');
			}
			elseif($key_tag == 'airline')
			{
				$query->where('ta.file_name', 'like', 'ticket_booking%');
			}
			elseif($key_tag == 'tieza_official')
			{
				$query->where('ta.file_name', 'like', 'tieza_official%');
			}
			else
			{
				$query->join('sections_upload as su', 'su.id', 'ta.section_upload_id')
				->where('su.key_tag', $key_tag);
			}
		}

		return $query->count();
	}

	protected function get_applicant_fee($id = null, $applicant_fee_id = null, $section_id = null)
	{
		$query = DB::table('rtt_amount as ra')
		->where('ra.deleted_at', null);

		if($applicant_fee_id) $query->where('ra.type_applicant_fee_id', $applicant_fee_id);

		if($section_id) $query->where('ra.section_type_id', $section_id);

		if($id)
		{
			if($id == 'no')
			{
				return $query->first();
			}
			else
			{
				return $query->where('ra.id')->first();
			}
		}
		else
		{
			 return $query->get();
		}
	}

	protected function get_last_generate_no_by($option)
	{	
		$field_generate_no = 'app_tbl.'.$option.'_no';
		$field_name = $option.'_no';

		$app_tbl_no = 1;

		if($app_tbl = DB::table($option.'_application as app_tbl')
		->where('app_tbl.deleted_at', null)
		->where('app_tbl.status', 2)
		->wherenotnull('app_tbl.generate_to_email')
		->orderby($field_generate_no, 'DESC')
		->select($field_generate_no)
		->first()) $app_tbl_no = $app_tbl->$field_name + 1; 

		return $app_tbl_no;
	}
	protected function get_signatories()
	{
		return DB::table('signatories')
		->leftJoin('users','users.id','signatories.name')
		->selectRaw('CONCAT(first_name," ",last_name) as fullname,users.id,position')
		->get();
	}
	protected function get_signatories_where($id)
	{
		return DB::table('users')
		->leftJoin('signatories','users.id','signatories.name')->where('users.id',$id)->first();
	}

	protected function get_residency($id ="",$byID = "0")
	{
		$data =  DB::table('residency')
		->leftJoin('countries','countries.id','residency.country_id')
		->selectRaw('residency.id,residency.remarks,residency.file_name,residency.country_id,residency.created_at,countries.name');
		 if($id != "null" && $byID == "0") $data->where('country_id',$id);
		 elseif($id != "null" && $byID == "1") $data->where('residency.id',$id);
	
		
		return $byID == "0" ? $data->get():$data->first();
	}
	protected function get_alltrans()
	{
		$fulltax = DB::table('fulltax_application')
		->selectRaw("'Full Tax' as trans_type, CONCAT(first_name,' ',middle_name,' ',last_name) as name,passport_no,ticket_no,departure_date");
		
		$tec = DB::table('tec_application')
		->selectRaw("'Exemption' as trans_type, CONCAT(first_name,' ',middle_name,' ',last_name) as name,passport_no,ticket_no,date_flight");
		
		$ttr = DB::table('tr_application')
		->selectRaw("'Refund' as trans_type, CONCAT(first_name,' ',middle_name,' ',last_name) as name,passport_no,ticket_no,date_flight");
		$rtt = DB::table('rtt_application')
		->selectRaw("'Reduced' as trans_type, CONCAT(first_name,' ',middle_name,' ',last_name) as name,passport_no,ticket_no,date_flight");
		
		return $fulltax->union($tec)->union($ttr)->union($rtt)->get();
	}
	protected function list_online_data()
	{
		$tec = DB::table('tec_application as a')
		->leftjoin('countries as b','b.id','a.country_id')
		->leftjoin('airlines as c','c.id','a.airlines_id')
		->selectRaw("'Exemption' as trans_type, CONCAT(first_name,' ',middle_name,' ',last_name) as name,passport_no,ticket_no,date_application,date_flight as departure_date,email,mobile_no
		,b.name as country,c.name as airlines")
		->where('is_onsite','0')
		->groupby('a.id');
		$rtt = DB::table('rtt_application as a')
		->leftjoin('countries as b','b.id','a.country_id')
		->leftjoin('airlines as c','c.id','a.airlines_id')
		->selectRaw("'Reduced' as trans_type, CONCAT(first_name,' ',middle_name,' ',last_name) as name,passport_no,ticket_no,date_application,date_flight as departure_date,email,mobile_no
		,b.name as country,c.name as airlines")
		->where('is_onsite','0')
		->groupby('a.id')
		->union($tec);
		
		return $subqry = DB::table(DB::raw("({$rtt->toSql()}) as a"))
		->mergeBindings($rtt)
		->orderby('date_application','desc')->get();
	}
}