<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AllowifUser
{
    public function handle($request, Closure $next)
    {
        if(!Auth::check()) return redirect('/');

	    if(!Auth::user()->isUser()) return redirect('/');
	        
	    return $next($request);    
    }
}