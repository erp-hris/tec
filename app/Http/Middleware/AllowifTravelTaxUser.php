<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AllowifTravelTaxUser
{
    public function handle($request, Closure $next)
    {
        if(!Auth::check()) return redirect('/');

	    if(!Auth::user()->isTravelTaxUser()) return redirect('/');
	        
	    return $next($request);    
    }
}
