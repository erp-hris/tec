<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AllowifKiosk
{
    public function handle($request, Closure $next)
    {
        if(!Auth::check()) return redirect('/');

	    if(!Auth::user()->isKiosk()) return redirect('/');
	        
	    return $next($request);    
    }
}