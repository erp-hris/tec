<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class tec_approved extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('otps.tieza@gmail.com')
        ->view('mail.tec-report', ['image_src' => url('/images/5/id_picture_2x2_20200612_QsWljMBxDQ4ZEokBdvveLmxdLB2X5Cai0qRrPW1fIUbRuwWnsk.png']);
    }
}
