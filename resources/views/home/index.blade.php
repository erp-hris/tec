@extends('layouts.master-auth')
@section('css')
@include('layouts.auth-partials.datatables-css')
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/css/jquery.niftymodals.css') }}"/>
<style>
.card-1
{
    border-radius:50px!important;
    background: #ff6000 !important;
    border:25px solid  rgb(6,10,57) !important;
}
.card-1:hover
{
    border:25px solid rgb(179, 0, 0) !important;
}

.card-2
{
    border-radius:50px!important;
    background: #255E68 !important;
    border:25px solid  rgb(6,10,57) !important;
}
.card-2:hover
{
    border:25px solid #ffe241 !important;
}
.card-3
{
    border-radius:50px!important;
    background: #ffe241 !important;
    border:25px solid  rgb(6,10,57) !important;
}
.card-3:hover
{
    background: #ffe241 !important;
    border:25px solid green !important;
}
.card-4
{
    border-radius:50px!important;
    background: #44a8fd !important;
    border:25px solid  rgb(6,10,57) !important;
}
.card-4:hover
{
    background: #44a8fd !important;
    border:25px solid #ff6000 !important;
}
.a-href {
    color:black;
    background:black;
}
h1{
    font-family:'Arial Black' !important;
    color:rgb(6,10,57) !important;
}

</style>
@endsection

@section('content')
    <div class="page-head">
        <div class="page-head-title "> @if(!(Auth::user()->isUser() || Auth::user()->isKiosk()))Welcome! {{ Auth::user()->getFullNameFML() }}  @endif</div>
        <div class="page-description">
            @if(!(Auth::user()->isUser() || Auth::user()->isKiosk()))
                Online Travel Tax Application
            
            @endif
        </div>
    </div>
    @if(Auth::user()->isUser() || Auth::user()->isKiosk())  
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-0 col-xs-0"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
            <img src="{{ asset('img/tieza-logo.png') }}" alt="logo" class="ml-0 img-fluid " style="height: 15em;">
            <h1>WELCOME TO TRAVEL TAX SERVICES @if(Auth::user()->isKiosk()) (KIOSK) @endif </h1>

        </div>
        
    </div>
    
    <div class="row col-12">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
              <h1 class="text-center"><b>PAYMENT</b></h1> 
              <div class="card  card-1 ">
                  <div class="card-body" >
                    <center><img class="" id="payment"src="{{asset('img/mass-payment.png')}}" style="height:10em;width: 10em;"alt="Placeholder" ></center>
                  </div>
              
             </div>
            
            </div>  
            <!-- <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div> -->
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
              <h1 class="text-center"><b>EXEMPTION</b></h1> 
              <div class="card  card-2 ">
                  <div class="card-body ">
                    <center> 
                        @if(Auth::user()->isKiosk())
                        <a href=" {{ url('kiosk/application/tec_application') }}"><img class="exemption" id="imgexemption" src="{{asset('img/exemption.png')}}" style="height:10em;"alt="Placeholder" ></a>
                        @else
                        <a href=" {{ url('user/application/tec_application') }}"><img class="exemption" id="imgexemption" src="{{asset('img/exemption.png')}}" style="height:10em;"alt="Placeholder" ></a>
                        @endif
                    </center>
                  </div>
              
             </div>
             
            </div>  
            <!-- <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div> -->
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
              <h1 class="text-center"><b>REDUCED</b></h1> 
              <div class="card card-3  ">
                  <div class="card-body ">
                    <center>
                    @if(Auth::user()->isKiosk())
                        <a href=" {{ url('kiosk/application/rtt_application') }}"><img class="" src="{{asset('img/taxreduced.png')}}" style="height:10em;"alt="Placeholder" ></a>
                    @else
                        <a href=" {{ url('user/application/rtt_application') }}"><img class="" src="{{asset('img/taxreduced.png')}}" style="height:10em;"alt="Placeholder" ></a>
                    @endif
                    </center>
                  </div>
              
             </div>

            </div>  
            <!-- <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div> -->
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
              <h1 class="text-center"><b>REFUND</b></h1> 
              <div class="card card-4">
                  <div class="card-body  ">
                    <center>
                    @if(Auth::user()->isKiosk())
                        <a href=" {{ url('kiosk/application/tr_application') }}"><img class="" src="{{asset('img/REFUND2.png')}}" style="height:10em;width:10em"alt="Placeholder" ></a>
                    @else
                        <a href=" {{ url('user/application/tr_application') }}"><img class="" src="{{asset('img/REFUND2.png')}}" style="height:10em;width:10em"alt="Placeholder" ></a>
                    @endif
                    </center>
                  </div>
             </div>
            </div>  
    </div>
    <div class="ml-n8">
        <div class="row">
            <!-- <div class="row col-xl-9 offset-xl-1 col-lg-7 col-md-7 col-sm-12 col-xs-12 mt-8 text-center"  > -->
            <p> For Inquiries or clarifications please contact us.</p>
        </div>
        <div class="row">
        <b> Tieza Central Office</b>
        </div>
        <div class="row">
            (02) 8 249 5987
        </div>
        <div class="row">
            traveltax@tieza.gov.ph
        </div>
        <div class="row">
        Monday to Thursday 8:00 AM to 3:00 PM
        </div>
    </div>
    <div class="md-container md-effect-11" id="modal1" >
            <div class="md-content ">
            <img src="{{ asset('img/Clip051.png') }}" class="ml-0 img-fluid">
              
            </div>
    </div>
    <div class="md-container md-effect-11" id="modal2" >
            <div class="md-content ">
            <img src="{{ asset('img/Clip052.png') }}" class="ml-0 img-fluid">
              
            </div>
    </div>

    @elseif(Auth::user()->isSuperAdmin())
    <div class="card-body">
        <table class="table table-striped bg-white table-hover table-fw-widget" id="table4">
            <thead>
                <tr>
                <td>Transaction Type</td>
                <td>Name</td>
                <td>Passport No.</td>
                <td>Ticket No.</td>
                <td>Departure Date</td>
                </tr>
            </thead>
            <tbody>
            
            </tbody>
        </table>
        
    </div>
    @endif
@endsection


@section('scripts')
@include('layouts.auth-partials.datatables-scripts')
<script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
    @yield('additional-scripts')
   
    <script type="text/javascript">
       $(document).ready(function () {   
        App.init();
        App.dataTables();
        loadDatatable();   

       });
        $('#payment').click(function()
        {
            title = "";
            text = "Complete the account information to proceed payment."
           
            color = "colored-header colored-header-primary";
            button = "btn btn-primary";
            const swal_continue = alert_continue(title, text,button, color);
            swal_continue.then((result) => {
                if(result.value){
                   
                    var isKiosk = '@if(Auth::user()->isKiosk()) yes  @endif';
                    if(isKiosk) location.href = "{{route('kiosk.index')}}";
                    else location.href = "{{route('fulltax.index')}}";
                    // 
                }
            });
        });
        $('#imgexemption').click(function()
        {
            $('#modal2').niftyModal();
        });

    function loadDatatable()
    {

        
        @php
        $this->based_url =Request::url();
        $columns = array(['data' => 'trans_type'],['data' => 'name'], ['data' => 'passport_no'], ['data' => 'ticket_no'], ['data' => 'departure_date']);
        $url = $this->based_url.'/all_transaction/datatables/';
        @endphp

        console.log('{{$url}}');
        load_datables('#table4', "{!! $url !!}" ,{!! json_encode($columns) !!}, null);

    }
    </script>
@endsection