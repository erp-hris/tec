@extends('layouts.app')

@section('content')
<div class="be-wrapper be-login be-signup">
    <div class="be-content">
        <div class="main-content container-fluid">
            <div class="splash-container sign-up">
                <div class="card card-border-color card-border-color-primary">
                    <!-- <img src="{{ URL::asset('beagle-assets/img/logo-xx.png') }}" alt="logo" width="102" height="27" class="logo-img"> -->
                    <div class="card-header"><span class="splash-description">Please enter your user information.</span></div>
                    <div class="card-body">
                        <form action="{{ route('register') }}" method="POST"><span class="splash-title pb-4">Sign Up</span>
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input id="name" type="text" name="name" required="" placeholder="Name" autocomplete="off" class="form-control" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <input type="text" name="username" required="" placeholder="Username" autocomplete="off" class="form-control">
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}  row signup-password">
                                <div class="col-6">
                                    <input id="pass1" name="password" type="password" required="" placeholder="Password" class="form-control">
                                </div>
                                <div class="col-6">
                                    <input required="" name="password_confirmation" type="password" placeholder="Confirm" class="form-control">
                                </div>
                            </div>

                            <div class="form-group pt-2">
                                <button type="submit" class="btn btn-block btn-primary btn-xl">Sign Up</button>
                            </div>

                            <!-- Social Logins
                            <div class="title"><span class="splash-title pb-3">Or</span></div>
                            <div class="form-group row social-signup pt-0">
                                <div class="col-6">
                                    <button type="button" class="btn btn-lg btn-block btn-social btn-facebook btn-color"><i class="mdi mdi-facebook icon icon-left"></i> Facebook</button>
                                </div>
                                <div class="col-6">
                                    <button type="button" class="btn btn-lg btn-block btn-social btn-google-plus btn-color"><i class="mdi mdi-google-plus icon icon-left"></i> Google Plus</button>
                                </div>
                            </div>
                            <div class="form-group pt-3 mb-3">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input"><span class="custom-control-label">By creating an account, you agree the <a href="#">terms and conditions</a>.</span>
                                </label>
                            </div>
                            -->
                            <!-- ./End Social Logins -->
                        </form>
                    </div>
                </div>
                <div class="splash-footer">&copy; 2018 Your Company</div>
            </div>
        </div>
    </div>
</div>
@endsection