                
                <table id="overview_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                    <thead>
                        <tr class="text-center">
                            <th style="width: 10%;">{{ __('page.action') }}</th>
                            <th style="width: 25%;">Application No</th>
                            <th style="width: 25%;">Full Name</th>
                            <th style="width: 20%;">Status</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>