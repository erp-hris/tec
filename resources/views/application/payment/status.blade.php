{{ csrf_field() }}
<style>
.event-background{
    background-image:url("{{ asset('img/bg-withlogo.png') }}");
    /* background-size:90vmax; */
     background-repeat: no-repeat;  
    
}

.card{
    height:80vh;
    background-image:url("{{ asset('img/bg-tieza2.png') }}");
     background-size:90vmax; 
     background-repeat: no-repeat;  
    background-position: center;
    font-family:Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif, Haettenschweiler, 'Arial Narrow Bold', sans-serif, Geneva, Tahoma, sans-serif !important;
    font-weight: bold;
    opacity: .95;
}
.card-footer, .card-divider ,.card-header{
    display:none;
}
.payment{
    font-size:5rem;
}
ul{

    list-style-position: inside;
    font-family:arial;
}
</style>

<br><br><br><br>
<div class="row bdy" style="">
    <div class="col-12 text-center" >
                <a class="payment">
                    @if($response->status_code == "00")
                        Payment Successful
                    @else {{$response->message}}
                    @endif
                </a>
        <div class="row">
            <div class="col-8 offset-2">
                 <h2 class="">Thank you for using our Online Travel Tax Services System.</h2>
            </div>
           
        </div>
        
<br><br>
    <div class="col-10 offset-1 text-left">
        <h3>REMINDERS:</h3>
        <ul style="" class="h4 p-1">
            <li>If you do not receive the TIEZA Acknowledgement Receipt (AR) via email in a few
                minutes, please check your “junk mail” or “spam” folder.</li>
            <li>If you find the AR in you Junk or Spam folder, please add: no-
                reply.traveltax@tieza.gov.ph to your White List or Safe Sender List.</li>
            <li>If you did not receive the TIEZA AR, please email: “traveltax.helpdesk@tieza.gov.ph”</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-3     offset-1 mt-5 h4">
            <a href="{{URL::to('/fulltax')}}" ><u> Add another transaction?</u></a>
        </div>
    </div>
</div>