<h2><i class="icon icon-left mdi mdi-shield-check"></i> Payment Confirmation</h2>
<div class="row">
    <div class="col-12">
        {{-- https://payment.myeg.ph/payment/v1/easy --}}
        {{-- https://ipay.myegdev.com/payment/v1/easy --}}
        <form role="form" action="https://payment.myeg.ph/payment/v1/easy" method="post"  enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" class="form-control form-control-sm" name="customer_email" value="{{$details['customer_email']}}"/>
            <input type="hidden" name="product_description" value="{{$details['product_description']}}"/>
            <input type="hidden" name="service_id" value="{{$details['service_id']}}">
            <input type="hidden" name="checksum" 
            value="{{$checksum}}">
            <input type="hidden" name="remarks" value="{{$details['remarks']}}">
            <input type="hidden" class="form-control form-control-sm" name="customer_name" value="{{$details['customer_name']}}"/>
            <input type="hidden" class="form-control form-control-sm"  name="order_no" value="{{$details['order_no']}}">
            <input type="hidden" class="form-control form-control-sm" name="transaction_time" value="{{$details['transaction_time']}}">
            <input type="hidden" class="form-control form-control-sm" name="amount" value="{{$details['amount']}}"/>
            <input type="hidden" class="form-control form-control-sm" name="currency" value="{{$details['currency']}}"/>
            <input type="hidden" class="form-control form-control-sm" name="ticket_details_remarks" value="{{$details['ticket_details_remarks']}}"/>

            {{-- <input type="hidden" class="form-control form-control-sm" name="allow_payment_method" value="Bank Transfer"/> --}}
           
            <br>
        <table class="table table-bordered table-hover table-fw-widget" id="table4">
            <thead class="">
                <tbody>
                    <tr>
                        <td>Transaction Number</td>
                        <td>{{$details['order_no']}}</td>
                    </tr>
                    <tr>
                        <td>Passenger Name</td>
                        <td>{{strtoupper($details['customer_name'])}}</td>
                    </tr>
                    <tr>
                        <td>Transaction Time</td>
                        <td>{{$details['transaction_time']}}</td>
                    </tr>
                    <tr>
                        <td>Currency</td>
                        <td>{{$details['currency']}}</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td>₱ {{number_format($amount_wo,2)}}</td>
                    </tr>
                    <tr>
                        <td>Processing Fee</td>
                        <td>₱ {{$processing_fee}}</td>
                    </tr>
                    <tr>
                        <td>Total Amount</td>
                        <td><b>  ₱ {{number_format($details['amount'],2)}}</b></td>
                    </tr>
                </tbody>
        </table>
        <div class="col-3 offset-9">
            <span class=" h6 text-info">Note: If you use any Credit Card or Grab Pay, there is another fee about 1.4% of total amount.</span>
            <br><br>
            <button class="button btn btn-primary btn-md w-100" type="submit">Proceed</button>
        </div>
        </form>
    </div>
    
</div>

    
        
        {{--
        <label for="amount">Amount: </label>
        --}}