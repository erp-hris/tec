<style>
.event-background{
    background-image:url("{{ asset('img/bg-withlogo.png') }}");
    /* background-size:90vmax; */
     background-repeat: no-repeat;  
    
}

.card{
    height:80vh;
    background-image:url("{{ asset('img/bg-tieza2.png') }}");
     background-size:100vmax; 
     background-repeat: no-repeat;  
    background-position: center;
    font-family:Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif, Haettenschweiler, 'Arial Narrow Bold', sans-serif, Geneva, Tahoma, sans-serif !important;
    font-weight: bold;
    opacity: .97;
}
.card-footer, .card-divider ,.card-header{
    display:none;
}
.color-navy{
    font-family:'Arial Black' !important;
    color:rgb(13, 25, 148) !important;
    
}
.type_label 
{
    color:rgb(13, 25, 148) !important;
    font-weight: 900;
    text-align: center;
}
.main{

}
</style>
<br><br><br><br><br><br><br><br><br><br>
<div class="_this">
    <div class="row py-4">
        <div class=" col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
            <button type="button" class="btn btn-primary btn-block btn-xl p-5" onclick="window.location= '{{route('single_trans')}}'"><span class="h3"><i class="mdi mdi-account-add"></i> SINGLE</span></button>
        </div>
    </div>
    <div class="row py-2">
        <div class=" col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
            <button type="button" class="btn btn-primary btn-block btn-xl p-5" onclick="window.location= ''"><span class="h3"><i class="mdi mdi-accounts-add"></i> MULTIPLE</span></button>
        </div>
    </div>
    <div class="row py-4 type_label">
        <div class=" col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
            <p class="ml-5">For Full Tax multiple transaction maximum of 5 pax per transaction</p>
        </div>
    </div>
</div>