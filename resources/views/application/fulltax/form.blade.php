<style>
    input{
        text-transform: uppercase !important;
        
    }
    label{
        font-family:Arial, Helvetica, sans-serif ;
        
    }
    @media only screen and (min-width:0px) and (max-width:992px) {
        .img-logo{
            position: absolute;
            margin: auto;
            left: 0;
            right: 0;
            top:30%;
          
            z-index: 0;
            width: 50%;
            opacity: 0.2;

        }
    }
    @media only screen and (min-width:993px) {
        .img-logo{
            position: absolute;
            margin: auto;
            top:20%;
            left: 40%;
            right: 0;
            z-index: 0;
            width: 35vh;
        }
    }
    @media only screen and (min-width:1600px) {
        .img-logo{
            position: absolute;
            margin: auto;
            top:18%;
            left: 50%;
            right: 0;
            z-index: 0;
            width: 32vh;
        }
    }
    @media only screen and (min-width:2560px) {
        .img-logo{
            position: absolute;
            margin: auto;
            top:18%;
            left: 50%;
            right: 0;
            z-index: 0;
            width: 25vh;
        }
    }
    tr,td label{
        cursor: pointer;
    }
</style>
<div class="fulltax p-1">
    <form role="form" id="form_ft" action="{{route('fulltax.store')}}" method="post"  enctype="multipart/form-data">
        {{ csrf_field() }}
        <a class="btn btn-warning" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
          <h4><i class="mdi mdi-info"></i> Notes for Multiple Passengers in one transaction</h4> 
        </a>
        <div class="collapse" id="collapseExample">
            <div class="card card-body bg-warning p-3">
                - A maximum of five (5) passengers can be added in one transaction. <br>
                - All passengers within the same transaction must be on the same flight and will retain the same Mobile Number, E-mail Address, Destination, and Departure Date information of the first added passenger (Primary). <br>
                - Only one (1) Acknowledgement Receipt (AR) will be issued to the Primary passenger for all passengers under the same transaction.
            </div>
        {{-- <img src="{{asset('img/tieza-logo.png')}}" class=" img-logo"alt=""> --}}
        </div>
        <div class="fields">
            <div class="row p-1 ">
                <div class="col-12 col-lg-3 text-left p-1">
                    <label class="control-label" for="last_name">LAST NAME</label>
                    <input type="text" id="last_name" name="last_name"class="cf form-control  form-control-sm @if( $errors->first('last_name')) is-invalid @endif">
                </div>
                <div class="col-12 col-lg-3 text-left p-1">
                    <label class="control-label" for="last_name">FIRST NAME</label>
                    <input type="text" id="first_name" name="first_name"  class="cf form-control form-control-sm @if( $errors->first('first_name')) is-invalid @endif">    </div>
                <div class="col-12 col-lg-1 text-left p-1">
                    <label class="control-label" for="last_name">M.I</label>
                    <input type="text" id="middle_name" name="middle_name" class="cf form-control form-control-sm">
                </div>
                <div class="col-12 col-lg-1 text-left p-1" >
                    <label class="control-label bold" for="last_name">EXT. NAME</label>
                    <input type="text" id="ext_name" name="ext_name" class="cf form-control form-control-sm">
                </div>
            </div>
            <div class="row p-1">
                <div class="col-12 col-lg-3  text-left p-1">
                    <label class="control-label" for="passport">PASSPORT NUMBER</label>
                    <input type="text" id="passport" name="passport" class="cf form-control form-control-sm @if( $errors->first('passport')) is-invalid @endif">
                </div>
                <div class="col-12 col-lg-3  text-left p-1">
                    <label class="control-label" for="ticket">TICKET NUMBER</label>
                    <input type="text" id="ticket" name="ticket" class="cf form-control form-control-sm  @if( $errors->first('ticket')) is-invalid @endif">
                </div>
            </div>
            <div class="row p-1">
                <div class="col-12 col-lg-3 text-left p-1">
                    <label class="control-label" for="class_type">CLASS</label>
                    <div class="form-check mt-1 ">
                        <label class="custom-control custom-radio ">
                          <input class="custom-control-input class_1 c_class" type="radio" value="1" id="class_type" name="class_type" ><span class="custom-control-label">{{ __('page.first_class') }}</span>
                        </label>
                        <label class="custom-control custom-radio">
                          <input class="custom-control-input class_2 c_class" type="radio" value="2" id="class_type" name="class_type"><span class="custom-control-label">{{__('page.business_class') }}</span>
                        </label>
                      </div>
                </div>
                <div class="col-12 col-lg-3  text-left p-1">
                    <label class="control-label" for="class_type">AMOUNT</label>
                    <input type="text" class="cf form-control form-control-sm text-danger  text-bold" id="class_amt" name="class_amt" readonly>
                </div>
            </div>
            <div class="row p-1 div_fix">
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="mobile_no">MOBILE NUMBER</label>
                    <input type="text" id="mobile_no" name="mobile_no" class="form-control form-control-sm @if( $errors->first('mobile_no')) is-invalid @endif">
                </div>
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="email_address">EMAIL ADDRESS</label>
                    <input type="text" id="email_address" name="email_address" style="text-transform:lowercase !important" class="form-control form-control-sm  @if( $errors->first('email_address')) is-invalid @endif">
                </div>
                <div class="col-12 col-lg-6 text-left  p-1">
                </div>
            </div>
            <div class="row div_fix p-1">
                <div class="col-6 col-lg-3 text-left p-1">
                    <label class="control-label" for="ft_destination">DESTINATION</label>
                    <select class="form-control form-control-sm @if( $errors->first('ft_destination')) is-invalid @endif " 
                        name="ft_destination" 
                        id="ft_destination">   
                        <option value="0" disabled selected>{{ __('page.please_select') }}</option> 
                        @if($countries)
                            @foreach($countries as $key => $val)
                            <option value="{{$val->id}}">{{ $val->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="ticket">DEPARTURE DATE</label>
                    <input type="date" id="departure" name="departure" class="form-control form-control-sm @if( $errors->first('departure')) is-invalid @endif"" >
                </div>
                <div class="col-6 col-lg-6 p-1">
                    <div class="div_add">
                        <button type="button" id="add_btn" class="btn btn-primary mt-5 btn-lg"><i class="mdi mdi-plus"></i> &nbsp&nbspADD PASSENGER&nbsp&nbsp </button>
                    </div>
                    <div class="div_edit d-none">
                        <button type="button" id="update_btn" class="btn btn-primary mt-5 btn-lg"><i class="mdi mdi-refresh-alt"></i> &nbsp&nbspUPDATE PASSENGER&nbsp&nbsp </button>
                        <button type="button" id="cancel_btn" class="btn btn-danger mt-5 btn-lg"> &nbsp&nbsp CANCEL &nbsp&nbsp </button>
                    </div>
                </div>
            </div>
            <div class="row">
               
            </div>
        </div>
        <br>
        <div class="p-2">
            <table class="table table-striped table-hover border table-fw-widget text-uppercase" id="tblItems" style="overflow:auto !important" >
                <thead class="bg-primary text-white"> 
                    <tr>
                        <th></th>
                        <th>Lastname</th>
                        <th>Firstname</th> 
                        <th>Middlename</th> 
                        <th>ExtName</th> 
                        {{-- <th>Class</th> 
                        <th>Amount</th> 
                        <th>Departure Date</th> 
                        <th>Mobile</th>
                        <th>Email</th> --}}
                        <th>Destination</th>
                        <th>Passport Number</th>
                        <th>Ticket Number</th>
                    </tr>
                </thead>
                <tbody>
                  <tr class="tr_class">
                    <td><button type="button" class="btn btn-primary btntbl_edit" ></button></td>
                    <td><input type="hidden" name="xlast_name[]" id="xlast_name"><label id="lbl_lastname"></label></td>
                    <td><input type="hidden" name="xfirst_name[]" id="xfirst_name"><label id="lbl_firstname"></label></td>
                    <td><input type="hidden" name="xmiddle_name[]" id="xmiddle_name"><label id="lbl_middlename"></label></td>
                    <td><input type="hidden" name="xext_name[]" id="xext_name"><label id="lbl_extname"></label></td>
                    <td class="d-none"><input type="hidden" name="xclass_type[]" id="xclass_type"><label id="lbl_class"></label></td>
                    <td class="d-none"><input type="hidden" name="xamount[]" id="xamount"><label id="lbl_amount"></label></td>
                    <td class="d-none"><input type="hidden" name="xdeparture[]" id="xdeparture"><label id="lbl_departure"></label></td>
                    <td class="d-none"><input type="hidden" name="xmobile_no[]" id="xmobile_no"><label id="lbl_mobile"></label></td>
                    <td class="d-none"><input type="hidden" name="xemail_address[]" id="xemail_address"><label id="lbl_email"></label></td>
                    <td class="d-none"><input type="hidden" name="xdestination[]" id="xdestination"><label id="lbl_destination"></label></td>
                    <td ><input type="hidden" name="xdestination_text[]" id="xdestination_text"><label id="lbl_destination_text"></label></td>
                    <td ><input type="hidden" name="xpassport_no[]" id="xpassport_no"><label id="lbl_passport"></label></td>
                    <td ><input type="hidden" name="xticket_no[]" id="xticket_no"><label id="lbl_ticket"></label></td>
                </tr>
                </tbody>
            </table>
            <table class="table table-striped table-hover border table-fw-widget text-uppercase d-none" id="tblCopy" >
                <thead class="bg-primary text-white"> 
                    <tr>
                        <th></th>
                        <th>Lastname</th>
                        <th>Firstname</th> 
                        <th>Middlename</th> 
                        <th>ExtName</th> 
                        {{-- <th>Class</th> 
                        <th>Amount</th> 
                        <th>Departure Date</th> 
                        <th>Mobile</th>
                        <th>Email</th> --}}
                        <th>Destination</th>
                        <th>Passport Number</th>
                        <th>Ticket Number</th>
                    </tr>
                </thead>
                <tbody>
                  <tr class="tr_class">
                    <td><button type="button" class="btn btn-primary btntbl_edit"></button></td>
                    <td><input type="hidden" name="xlast_name[]" id="xlast_name"><label id="lbl_lastname"></label></td>
                    <td><input type="hidden" name="xfirst_name[]" id="xfirst_name"><label id="lbl_firstname"></label></td>
                    <td><input type="hidden" name="xmiddle_name[]" id="xmiddle_name"><label id="lbl_middlename"></label></td>
                    <td><input type="hidden" name="xext_name[]" id="xext_name"><label id="lbl_extname"></label></td>
                    <td class="d-none"><input type="hidden" name="xclass_type[]" id="xclass_type"><label id="lbl_class"></label></td>
                    <td class="d-none"><input type="hidden" name="xamount[]" id="xamount"><label id="lbl_amount"></label></td>
                    <td class="d-none"><input type="hidden" name="xdeparture[]" id="xdeparture"><label id="lbl_departure"></label></td>
                    <td class="d-none"><input type="hidden" name="xmobile_no[]" id="xmobile_no"><label id="lbl_mobile"></label></td>
                    <td class="d-none"><input type="hidden" name="xemail_address[]" id="xemail_address"><label id="lbl_email"></label></td>
                    <td class="d-none"><input type="hidden" name="xdestination[]" id="xdestination"><label id="lbl_destination"></label></td>
                    <td ><input type="hidden" name="xdestination_text[]" id="xdestination_text"><label id="lbl_destination_text"></label></td>
                    <td ><input type="hidden" name="xpassport_no[]" id="xpassport_no"><label id="lbl_passport"></label></td>
                    <td ><input type="hidden" name="xticket_no[]" id="xticket_no"><label id="lbl_ticket"></label></td>
                  </tr>
                </tbody>
            </table>
        </div>
        {{-- <div class="row p-1">
            <div class="col-12 col-xl-6 offset-xl-6 col-lg-6 offset-lg-6">
                <label class="custom-control custom-checkbox custom-control-inline">
                    <input class="custom-control-input form-control" id="agreement" type="checkbox" ><span class="custom-control-label custom-control-color">By ticking the checkbox, I hereby give my consent to <b>TIEZA</b> to collect and process all the personal data information requested on this page and succeeding pages.</span>
                </label>
            </div>
        </div> --}}
        <div class="row p-1">
            <div class="col-12 col-xl-6 offset-xl-6 col-lg-6 offset-lg-6 text-right">
                <button type="submit" class="btn btn-primary btn-lg w-50" ><i class="mdi mdi-arrow-right"></i> PROCEED</button>
            </div>
        </div>
        
    </form> 
</div>
<div class="modal-container modal-effect-1" id="nft-default">
    <div class="modal-content">
      <div class="modal-header">
        <h3>TERMS AND CONDITIONS</h3>
        <button class="close modal-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
      </div>
      <div class="modal-body">
        <div class="row ">
            <div class="px-5 ">
                <label style="font-size: 1.25rem !important;">By ticking the checkbox, I hereby give my consent to <b>TIEZA</b> to collect and process all the personal data information requested on this page and succeeding pages.</p>
            </div>
            <div class="px-5">
                <label style="font-size: 1.25rem !important;">I also declare that I shall exercise my rights at any reasonable opportunity in accordance with Republic Act no. 10173 otherwise known as The Data Privacy Act of 2012 it’s rules and other related issuance by the National Privacy Commission.</p>
            </div>
            <div class="px-5">
                <label style="font-size: 1.25rem !important;">For further information, please see the TIEZA Privacy Policy.</p>
            </div>
            <div class="px-5">
                <label style="font-size: 1.25rem !important;">Any information collected to process your travel tax transaction is used solely for purposes of that specific transaction. At TIEZA, we take your privacy seriously. If you have any questions and/or concerns regarding our privacy policy, kindly write to us at traveltax.helpdesk@tieza.gov.ph.</p>
            </div>
        </div>
        <div class="row ">
            <div class="col-9 px-5">
              <label class="custom-control custom-checkbox custom-control-inline">
                  <input class="custom-control-input form-control" id="agreement" type="checkbox" ><span class="custom-control-label custom-control-color">
                     <b> I have read and accept the Terms and Conditions.</b></span>
              </label>
            </div>
            <div class="col-3 px-5">
                <button type="button" class="btn btn-primary btn-block" id="btn_ok"> I AGREE</button>
              </div>
        </div>
      </div>
    </div>
</div>
  <div class="modal-overlay"></div>
@section('scripts')
<script src="{{ asset('beagle-v1.7.1/dist/html/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
<script type="text/javascript"> 

    var f_class = "2700.00";
    var e_class = "1620.00";
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    var count = 0;  
    var rowCount = 0;
    var class_type = "";
    var tr_curID = "";

  $(document).ready(function () {
    $('#main-content').addClass('be-nosidebar-left');
    $('.main').removeClass('col-12');
    $('.main').addClass('col-12 col-xl-10 offset-xl-1');
    $('#trans_date').val(today);
    $('#last_name').focus();
    App.formElements();
    $.fn.niftyModal('setDefaults',{
      	overlaySelector: '.modal-overlay',
      	contentSelector: '.modal-content',
      	closeSelector: '.modal-close',
      	classAddAfterOpen: 'modal-show'
        });

    $('.c_class').change(function()
    {
        if($(this).val() == "1")  $('#class_amt').val(f_class);
        else if($(this).val() == "2")  $('#class_amt').val(e_class);
       
        class_type = $(this).val();
    });

    $('#shw').click(function()
    {
        $('#modal1').niftyModal('show');
       
    });
    $('#pay').click(function()
    {
        location.href = "{{route('payment_success')}}";
    });
    $('#save_btn').click(function()
    {
       
        const formData = new FormData();
        formData.append('last_name',$('#last_name').val());
        formData.append('first_name',$('#first_name').val());
        formData.append('middle_name',$('#middle_name').val());
        formData.append('ext_name',$('#ext_name').val());
        formData.append('class_type',$('#class_type').val());
        formData.append('class_amt',$('#class_amt').val());
        formData.append('fulltax_type',$('#fulltax_type').val());
        formData.append('passport',$('#passport').val());
        formData.append('ticket',$('#ticket').val());
        formData.append('departure',$('#departure').val());
        formData.append('ft_destination',$('#ft_destination').val());

        axios.post('{{route("fulltax.store")}}',formData,{
         headers: {
                    'Content-Type': 'multipart/form-data'
                  }
        })
        .then(response => { 
            success = "Saved Successfully!";
            const swal_success = alert_success(success, 1500);
            swal_success.then((value) => {
                var name =response.data.details.first_name + ' ' + response.data.details.middle_name + ' ' + response.data.details.last_name;
                var amount = parseFloat(response.data.details.fulltax_amount,10);
                var passport = response.data.details.passport_no;
                var ticket = response.data.details.ticket_no;
                var departure = response.data.details.departure_date;

                $('#lbl_name').text(name);
                $('#lbl_amt').text(amount);
                $('#lbl_passport').text(passport);
                $('#lbl_ticket').text(ticket);
                $('#lbl_departure').text(departure);
                $('#lbl_departure').text(departure);
                $('#amt_total').val("P"+amount.toLocaleString());


                $('#modal1').niftyModal('show');
            });
        })
        .catch(error => {
            console.log(error.response)
        });
    });
    $('.tr_class .btntbl_edit').click(function()
    {
        if($(this).text() == "") return;
        var id = $(this).closest('tr').attr('id'); 
        $('#last_name').focus();
        tr_curID = id;
        if(id != ""){
            if(id == "row_id1") {
                $('.div_fix input').prop('readonly',false);
                $('.div_fix #ft_destination').prop('disabled',false);
            }
            else {
                $('.div_fix input').prop('readonly',true);
                $('.div_fix #ft_destination').prop('disabled',true);
            }
            $('#last_name').val($('#'+id+' #xlast_name').val());
            $('#first_name').val($('#'+id+' #xfirst_name').val());
            $('#middle_name').val( $('#'+id+' #xmiddle_name').val());
            $('#ext_name').val($('#'+id+' #xext_name').val());
            $('#passport').val($('#'+id+' #xpassport_no').val());
            $('#ticket').val($('#'+id+' #xticket_no').val());
            //no change 
            $('#mobile_no').val($('#row_id1 #xmobile_no').val());
            $('#email_address').val($('#row_id1 #xemail_address').val());
            $('#ft_destination').val($('#row_id1 #xdestination').val());
            $('#departure').val($('#row_id1 #xdeparture').val());
            class_type  = $('#'+id+' #xclass_type').val();
            if(class_type == "1") { 
                $('.class_1').prop('checked',true);
                $('#class_amt').val(f_class);
            }
            else if(class_type == "2"){
                $('.class_2').prop('checked',true);
                $('#class_amt').val(e_class);
            } 
            $('.div_add').addClass('d-none');   
            $('.div_add').removeClass('d-block');   
            $('.div_edit').removeClass('d-none');
            $('.div_edit').addClass('d-block');
            
        }
    });
    $('#add_btn').click(function()
    {
        Swal.fire({
        title: '', 
        html: '<b>Are you sure you want to save this record?</b> <br> If yes, click the continue button, otherwise, cancel to review your details.',
        type:'question',
            confirmButtonText: 'Continue',
            showCancelButton: true,
            cancelButtonText: 'Cancel'
            }).then((result) => {
            if (result.value) {
                if  ){
                    if(rowCount == 0)  $('table#tblItems tbody>tr').empty();
                    $('.div_fix input').prop('readonly',true);
                    $('.div_fix #ft_destination').prop('disabled',true);
                    rowCount += 1;
                    $("#tblCopy tbody>tr:first").clone(true).insertAfter("#tblItems tbody>tr:last");
                    $("#tblItems tbody>tr:last").attr('id','row_id'+rowCount);
                    $("#tblItems tbody>tr:last .btntbl_edit").text("EDIT");
                    $("#tblItems tbody>tr:last #lbl_lastname").text($('#last_name').val());
                    $("#tblItems tbody>tr:last #xlast_name").val($('#last_name').val());
                    $("#tblItems tbody>tr:last #lbl_firstname").text($('#first_name').val());
                    $("#tblItems tbody>tr:last #xfirst_name").val($('#first_name').val());
                    $("#tblItems tbody>tr:last #lbl_middlename").text($('#middle_name').val());
                    $("#tblItems tbody>tr:last #xmiddle_name").val($('#middle_name').val());
                    $("#tblItems tbody>tr:last #lbl_extname").text($('#ext_name').val());
                    $("#tblItems tbody>tr:last #xext_name").val($('#ext_name').val());
                    $("#tblItems tbody>tr:last #lbl_class").text(class_type);
                    $("#tblItems tbody>tr:last #xclass_type").val(class_type);
                    $("#tblItems tbody>tr:last #lbl_amount").text($('#class_amt').val());
                    $("#tblItems tbody>tr:last #xamount").val($('#class_amt').val());
                    $("#tblItems tbody>tr:last #lbl_departure").text($('#departure').val());
                    $("#tblItems tbody>tr:last #xdeparture").val($('#departure').val());
                    $("#tblItems tbody>tr:last #lbl_mobile").text($('#mobile_no').val());
                    $("#tblItems tbody>tr:last #xmobile_no").val($('#mobile_no').val());
                    $("#tblItems tbody>tr:last #lbl_email").text($('#email_address').val());
                    $("#tblItems tbody>tr:last #xemail_address").val($('#email_address').val());
                    $("#tblItems tbody>tr:last #xdestination_text").val($('#ft_destination :selected').text());
                    $("#tblItems tbody>tr:last #xdestination").val($('#ft_destination').val());
                    $("#tblItems tbody>tr:last #lbl_destination_text").text($('#ft_destination :selected').text());
                    $("#tblItems tbody>tr:last #lbl_ticket").text($('#ticket').val());
                    $("#tblItems tbody>tr:last #xticket_no").val($('#ticket').val());
                    $("#tblItems tbody>tr:last #lbl_passport").text($('#passport').val());
                    $("#tblItems tbody>tr:last #xpassport_no").val($('#passport').val());
                    clearFields();
                    class_type = "";
                    $('.fields input').removeClass('is-invalid');
                    $('.fields #ft_destination').removeClass('is-invalid');
                    setTimeout(function() { $('#last_name').focus() }, 1000);

                    if(rowCount == 5) {
                        $('#add_btn').addClass('d-none');
                    }
                }
                
            }
        });
        
    });
    $("#form_ft").on("submit",function(e) {
        var terms = $('#agreement').is(':checked');
      
       var values = $('#tblItems tbody>tr:last #lbl_lastname').text();
       if(!terms && values != ""){
            $('#nft-default').niftyModal('show');
            return false;
        }
       if(values == "") {

            Swal.fire({
                title: '', 
                text: 'Please add atleast 1 passenger.',
                type:'error',
                 confirmButtonText: 'OK',
                }).then((result) => {
                if (result.value) {
                   
                }
            });
           return false;
       } 

       if(terms) return true;
    });
    $('#cancel_btn').click(function()
    {
        clearFields();
        $('.div_add').addClass('d-block');   
        $('.div_add').removeClass('d-none');
        $('.div_edit').addClass('d-none');
        $('.div_edit').removeClass('d-block');
        $('.div_fix input').prop('readonly',true);
        $('.div_fix #ft_destination').prop('disabled',true);

    });
    $('#update_btn').click(function()
    {
        if(validateFields()){
            $("#"+tr_curID+" #lbl_lastname").text($('#last_name').val());
            $("#"+tr_curID+" #xlast_name").val($('#last_name').val());
            $("#"+tr_curID+" #lbl_firstname").text($('#first_name').val());
            $("#"+tr_curID+" #xfirst_name").val($('#first_name').val());
            $("#"+tr_curID+" #lbl_middlename").text($('#middle_name').val());
            $("#"+tr_curID+" #xmiddle_name").val($('#middle_name').val());
            $("#"+tr_curID+" #lbl_extname").text($('#ext_name').val());
            $("#"+tr_curID+" #xext_name").val($('#ext_name').val());
            $("#"+tr_curID+" #lbl_class").text(class_type);
            $("#"+tr_curID+" #xclass_type").val(class_type);
            $("#"+tr_curID+" #lbl_amount").text($('#class_amt').val());
            $("#"+tr_curID+" #xamount").val($('#class_amt').val());
            $("#"+tr_curID+" #lbl_departure").text($('#departure').val());
            $("#"+tr_curID+" #xdeparture").val($('#departure').val());
            $("#"+tr_curID+" #lbl_mobile").text($('#mobile_no').val());
            $("#"+tr_curID+" #xmobile_no").val($('#mobile_no').val());
            $("#"+tr_curID+" #lbl_email").text($('#email_address').val());
            $("#"+tr_curID+" #xemail_address").val($('#email_address').val());
            $("#"+tr_curID+" #xdestination_text").val($('#ft_destination :selected').text());
            $("#"+tr_curID+" #xdestination").val($('#ft_destination').val());
            $("#"+tr_curID+" #lbl_destination_text").text($('#ft_destination :selected').text());
            $("#"+tr_curID+" #lbl_ticket").text($('#ticket').val());
            $("#"+tr_curID+" #xticket_no").val($('#ticket').val());
            $("#"+tr_curID+" #lbl_passport").text($('#passport').val());
            $("#"+tr_curID+" #xpassport_no").val($('#passport').val());
            $('#cancel_btn').click();
        }
    });
    $('.modal-close').click(function()
    {
    });
    $('#btn_ok').click(function()
    {
        var values = $('#tblItems tbody>tr:last #lbl_lastname').text();
        var terms = $('#agreement').is(':checked');
        if(values != "" && terms){
            $('#form_ft').submit();
        }
        else $('#nft-default').niftyModal('toggle');
        
    });
    function clearFields(){
        
        $('.cf ').val("");
        $('.fields .c_class').prop('checked',false);
       

    }
    function validateFields()
    {
        if(!$('#last_name').val().trim() ) { $('#last_name').addClass('is-invalid');}
        if(!$('#first_name').val().trim() ) { $('#first_name').addClass('is-invalid');}
        if(!class_type) {  $('.c_class').addClass('is-invalid');}
        if(!$('#passport').val().trim() ) { $('#passport').addClass('is-invalid');}
        if(!$('#ticket').val().trim() ) { $('#ticket').addClass('is-invalid');}
        if(!$('#mobile_no').val().trim() ) { $('#mobile_no').addClass('is-invalid');}
        if(!$('#email_address').val().trim() ) { $('#email_address').addClass('is-invalid');}
        if(!$('#departure').val().trim() ) { $('#departure').addClass('is-invalid');}
        if($('#ft_destination').val() == null ) { $('#ft_destination').addClass('is-invalid');}
        
        if($('#last_name').val().trim() && $('#first_name').val().trim() && class_type &&
            $('#passport').val().trim() && $('#ticket').val().trim() && $('#mobile_no').val().trim() &&
            $('#email_address').val().trim() && $('#departure').val().trim() && $('#ft_destination').val() != null ) return true;   
        else {
            Swal.fire({
                title: '', 
                text: 'Please check the required fields.',
                type:'error',
                 confirmButtonText: 'OK',
                }).then((result) => {
                if (result.value) {
                   
                }
            });
            return false ;
        } 
    }
    
  });
</script>
@endsection