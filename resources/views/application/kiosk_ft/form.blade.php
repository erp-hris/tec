<div class="kiosk ">
    <input type="hidden" id="ft_id">
    <div class="row">
        <div class="col-6 col-xs-6 col-sm-6 col-md-9 col-lg-9 col-xl-9 text-right p-1"> 
            <label class="control-label" for="">Transaction No.</label>
        </div>
        <div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
            <input type="text" id="last_name" name="trans_no"class="form-control form-control-xs ">
        </div>
    </div>
    <div class="row">
        <div class="col-6 col-xs-6 col-sm-6 col-md-9 col-lg-9 col-xl-9 text-right p-1"> 
            <label class="control-label" for="">Transaction Date</label>
        </div>
        <div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
            <input type="text" id="trans_date" name="trans_date"class="form-control form-control-xs datetimepicker   " readonly>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-2 col-xl-2 text-right p-1"> 
            <label class="control-label" for="">Travel Tax Rate:</label>
        </div>
        <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3 col-xl-3  form-check form-check-lg ">
            <label class="custom-control custom-radio custom-control-inline">
                <input class="custom-control-input" type="radio" id="first" name="radio-inline" ><span class="custom-control-label">First Class</span>
            </label>
            <label class="custom-control custom-radio custom-control-inline">
                <input class="custom-control-input" type="radio" id="eco" name="radio-inline" ><span class="custom-control-label">Economy/Business Class</span>
            </label>
        </div>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 p-1  "> 
            <input type="text" class="form-control form-control-lg  text-danger " id="amt" for="" readonly>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-2 col-xl-2 text-right p-1"> 
            <label class="control-label" for="">Last Name:</label>
        </div>
        <div class="col-1 col-xs-1 col-sm-1 col-md-1 col-lg-3 col-xl-3"></div>
        <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3 col-xl-3">
            <input type="text" class="form-control form-control-xs" id="xlastname" for="" >
        </div>
    </div>
    <div class="row">
        <div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-2 col-xl-2 text-right p-1"> 
            <label class="control-label" for="">First Name:</label>
        </div>
        <div class="col-1 col-xs-1 col-sm-1 col-md-1 col-lg-3 col-xl-3"></div>
        <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3 col-xl-3">
            <input type="text" class="form-control form-control-xs" for="" id="xfirstname">
        </div>
    </div>
    <div class="row">
        <div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-2 col-xl-2 text-right p-1"> 
            <label class="control-label" for="">Middle Name:</label>
        </div>
        <div class="col-1 col-xs-1 col-sm-1 col-md-1 col-lg-3 col-xl-3"></div>
        <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3 col-xl-3">
            <input type="text" class="form-control form-control-xs" for="" id="xmiddlename">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-2 col-xl-2 text-right p-1"> 
            <label class="control-label" for="">Passport Number:</label>
        </div>
        <div class="col-1 col-xs-1 col-sm-1 col-md-1 col-lg-3 col-xl-3"></div>
        <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3 col-xl-3">
            <input type="text" class="form-control form-control-xs" for="" id="xpassport">
        </div>
    </div>
    <div class="row">
        <div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-2 col-xl-2 text-right p-1"> 
            <label class="control-label" for="">Ticket Number:</label>
        </div>
        <div class="col-1 col-xs-1 col-sm-1 col-md-1 col-lg-3 col-xl-3"></div>
        <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3 col-xl-3">
            <input type="text" class="form-control form-control-xs" for="" id="xticket">
        </div>
    </div>
    <div class="row">
        <div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-2 col-xl-2 text-right p-1"> 
            <label class="control-label" for="">Departure Date:</label>
        </div>
        <div class="col-1 col-xs-1 col-sm-1 col-md-1 col-lg-3 col-xl-3"></div>
        <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3 col-xl-3">
            <input type="date" class="form-control form-control-xs" for="" id="xdepdate">
        </div>
    </div>
</div>


<div class="modal-container  custom-width modal-effect-11" id="modal1" tabindex="-1" role="dialog">
        <div class="modal-content modal-dialog ">
            <div class=" modal-header  modal-header-colored">
            <h4 class="modal-title"> <b>Payment Information</b></h4>
            <!-- <button class="close modal-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button> -->
            </div>
            <div class="modal-body">
            <form method="" action="javascript:submit();">
            {{ csrf_field() }}
                <div class="row">
                    <div class="col-9 text-right">Transaction No.</div>
                    <div class="col-3"></div>
                </div>
                <div class="row">
                    <div class="col-9 text-right">Transaction Date</div>
                    <div class="col-3"><b><label id="transaction_date"></label></b></div>
                </div>    
                <br>
                <div class="row">
                    <div class="col-4 text-right">Passenger Name</div>
                    <div class="col-3"></div>
                    <div class="col-4"><b><label id="name"></label></b></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-4 text-right">Travel Tax Rate</div>
                    <div class="col-3"></div>
                    <div class="col-4"><b><label id="rate"></b></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-4 text-right">Passport Number</div>
                    <div class="col-3"></div>
                    <div class="col-4"><b><label id="pass_lbl"></b></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-4 text-right">Ticket/Confirmation No.</div>
                    <div class="col-3"></div>
                    <div class="col-4"><b><label id="tick_lbl"></b></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-4 text-right">Departure Date</div>
                    <div class="col-3"></div>
                    <div class="col-4"><b><label id="depdate_lbl"></b></div>
                </div>
                <br><br><br><center>
                <button type="button" class="btn btn-success" id="generate_qr">GENERATE QRCODE</button></center>
            </form>

            </div>
        </div>  
    </div>

<div class="modal-container  custom-width modal-effect-11 " id="modal2" tabindex="-1" role="dialog">
        <div class="modal-content modal-dialog ">
            <div class=" modal-header  modal-header-colored">
            <h3 class="modal-title"> <b><label id="modal_label">Your QR Code has been generated.</label></b></h3>
            <!-- <button class="close modal-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button> -->
            </div>
            <div class="modal-body">
            <center>
            <image src="" id="img-qr" width="300px" height="300px" alt="qr_code" />
            </center>
            </div>
        </div>  
    </div>
    <div class="modal-overlay"></div>

@section('scripts')
<script src="{{ asset('beagle-v1.7.1/dist/html/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
<script type="text/javascript">
var f_class = "2700.00";
var e_class = "1620.00";    
var class_type = "";
$(document).ready(function()
{
    App.formElements();
    $.fn.niftyModal('setDefaults',{
      	overlaySelector: '.modal-overlay',
      	contentSelector: '.modal-content',
      	closeSelector: '.modal-close',
      	classAddAfterOpen: 'modal-show'
    });
    $('#trans_date').val(new Date(Date.now()));
    //  var url = "{{ url('/qr_code_ft/103/_qrcode.png') }}";
    // $('#img-qr').attr('src',url);

    // $('#modal2').niftyModal('show');

    $('.custom-control-input').change(function()
    {
        class_type = $(this).attr('id');
        if(class_type == "first") $('#amt').val(f_class);
        else if(class_type == "eco") $('#amt').val(e_class);
        

    });
    $('#save_btn').click(function()
    {
        var lastname = $('#xlastname').val();
        var firstname = $('#xfirstname').val();
        var middlename = $('#xmiddlename').val();
        var taxrate = $('#amt').val();
        var passport = $('#xpassport').val();
        var ticket = $('#xticket').val();
        var departure = $('#xdepdate').val();
        var trans_date = $('#trans_date').val();

        var class_ = "";
        if(class_type == "first") class_ = "1";
        else if (class_type == "eco") class_ = "2";


        const formData = new FormData();
        formData.append('last_name',lastname);
        formData.append('first_name',firstname);
        formData.append('middle_name',middlename);
        formData.append('class_type',class_);
        formData.append('class_amt',taxrate);
        formData.append('passport',passport);
        formData.append('ticket',ticket);
        formData.append('departure_date',departure);
        formData.append('date_application',trans_date);


        axios.post('{{route("kiosk.store")}}',formData,{
         headers: {
                    'Content-Type': 'multipart/form-data'
                  }
        })
        .then(response => { 
            success = "Saved Successfully!";
            const swal_success = alert_success(success, 1500);
            swal_success.then((value) => {
                var name =response.data.details.first_name + ' ' + response.data.details.middle_name + ' ' + response.data.details.last_name;
                var amount = parseFloat(response.data.details.fulltax_amount,10);
                var passport = response.data.details.passport_no;
                var ticket = response.data.details.ticket_no;
                var departure = response.data.details.departure_date;
                var id = response.data.details.id;
                var date = response.data.details.date_app;

                

                $('#name').text(name);
                $('#rate').text(amount);
                $('#pass_lbl').text(passport);
                $('#tick_lbl').text(ticket);
                $('#depdate_lbl').text(departure); 
                $('#ft_id').val(id);                 
                $('#transaction_date').text(date);
                $('#modal1').niftyModal('show');
            });
        })
        .catch(error => {
            console.log(error.response)
        });
    });
    $('#generate_qr').click(function()
    {
       
        Swal.fire({
            title: 'Loading.. Please Wait',
            html:'Generating QR Code...',
            customClass: 'content-actions-center',
            buttonsStyling: true,
            allowOutsideClick: false,
            onOpen: function() {
                swal.showLoading();
                $(window).scrollTop(0);
                setTimeout(function(){ 
                    var url = "{{ route('kiosk_qrcode',['id'=>':id']) }}";
                    url = url.replace(':id',$('#ft_id').val());

                    axios.get(url, {
                       
                    })
                    .then(function(response){
                       
                        $('#modal1').niftyModal('hide');
                        $('#modal2').niftyModal('show');
                        var url = "{{ url('/qr_code_ft/:id/_qrcode.png') }}";
                        url = url.replace(':id',response.data.details.id);
                        // $('#modal_label').val()
                        $('#img-qr').attr('src',url);
                        swal.close();

                    });
                }, 1200); 
            }
        });  

    });

});





</script>
@endsection