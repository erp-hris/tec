@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
    @include('layouts.auth-partials.form-css')

@endsection

@section('content')
  

    @php 
        $data = [
        'option' => $option, 
        'title' => 'kiosk_ft', 
        'has_icon' => $icon, 
        'has_file' => $file,
        'has_footer' => 'yes', 
        'cancel_url' => url('application/kiosk_fulltax')
        ];
        $data['isSave'] = 'yes';
    @endphp

    @include('others.main_content', $data)
    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' => route('fulltax.store'), 'frm_id' => 'save_form'])
    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' =>  $application_url.'/'.$option.'/remove', 'frm_id' => 'remove_form'])
    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' =>  $application_url.'/'.$option.'/update_residency', 'frm_id' => 'update_form'])


@endsection

@section('scripts')


@yield('additional-scripts')
        
    <script type="text/javascript">
        $(document).ready(function(){

        });
    

        
     
    </script>
@endsection