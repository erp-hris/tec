<div class="tec p-1">
    <form role="form" id="form_rtt" action="/save_onlinertt" method="post"  enctype="multipart/form-data">
        {{ csrf_field() }}
        {{-- <a class="btn btn-warning" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
          <h4><i class="mdi mdi-info"></i> Notes for Multiple Passengers in one transaction</h4> 
        </a> --}}
        <div class="fields p-0">
            <div class="row px-2">
                <div class="col-xs-1-12">
                    <h4><b> <i class="mdi mdi-account text-primary "></i> PERSONAL INFORMATION</b></h4>
                </div>
            </div>
            <div class="row p-1 ">
                <div class="col-12 col-lg-3 text-left p-1">
                    <label class="control-label" for="last_name">Last Name<span class="text-red">&nbsp;*</span></label>
                    <input type="text" id="last_name" name="last_name"class="cf form-control  form-control-sm @if( $errors->first('last_name')) is-invalid @endif">
                </div>
                <div class="col-12 col-lg-3 text-left p-1">
                    <label class="control-label" for="last_name">First Name<span class="text-red">&nbsp;*</span></label>
                    <input type="text" id="first_name" name="first_name"  class="cf form-control form-control-sm @if( $errors->first('first_name')) is-invalid @endif">    </div>
                <div class="col-12 col-lg-1 text-left p-1">
                    <label class="control-label" for="last_name">M.I</label>
                    <input type="text" id="middle_name" name="middle_name" class="cf form-control form-control-sm">
                </div>
                <div class="col-12 col-lg-1 text-left p-1" >
                    <label class="control-label bold" for="last_name">Ext.</label>
                    <input type="text" id="ext_name" name="ext_name" class="cf form-control form-control-sm">
                </div>
            </div>
         
            <div class="row p-1 div_fix">
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="mobile_no">Mobile Number<span class="text-red">&nbsp;*</span></label>
                    <input type="text" id="mobile_no" name="mobile_no" class="form-control form-control-sm @if( $errors->first('mobile_no')) is-invalid @endif">
                </div>
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="email_address">Email Address<span class="text-red">&nbsp;*</span></label>
                    <input type="text" id="email_address" name="email_address" style="text-transform:lowercase !important" class="form-control form-control-sm  @if( $errors->first('email_address')) is-invalid @endif">
                </div>
                <div class="col-12 col-lg-3 text-left  p-1">
                    <label class="control-label">{{ __('page.is_minor')}}<span class="text-red">&nbsp;*</span></label>
                    <select class="form-control form-control-sm" 
                            name="is_minor" 
                            id="is_minor">   
                            <option value="" selected disabled>{{ __('page.please_select') }}</option>
                            <option value="1">{{ __('page.yes') }}</option>
                            <option value="0">{{ __('page.no') }}</option>
                    </select>
                    <div class="select_error" id="error-is_minor"></div>
                </div>
                <div class="col-6 col-lg-2 text-left p-1 minor_group">
                    <label class="control-label" for="last_name">Birth Date</label>
                    <input type="date" class="form-control form-control-sm" name="birthdate" id="birthdate">
                </div>
                <div class="col-6 col-lg-1 text-left p-1 minor_group"  >
                    <label class="control-label bold" for="last_name">Age</label>
                    <input type="text" class="form-control form-control-sm" name="age" id="age"readonly>
                </div>
            </div>
            <hr></hr>
            <div class="row px-1 px-2">
                <div class="col-xs-1-12">
                    <h4><b><i class="mdi mdi-flight-takeoff text-primary"></i> FLIGHT INFORMATION</b></h4>
                </div>
            </div>
            <div class="row p-1">
                <div class="col-12 col-lg-3  text-left p-1">
                    <label class="control-label" for="passport">Passport Number<span class="text-red">&nbsp;*</span></label>
                    <input type="text" id="passport" name="passport" class="cf form-control form-control-sm @if( $errors->first('passport')) is-invalid @endif">
                </div>
                <div class="col-12 col-lg-3  text-left p-1">
                    <label class="control-label" for="ticket">Ticket Number<span class="text-red">&nbsp;*</span></label>
                    <input type="text" id="ticket" name="ticket" class="cf form-control form-control-sm  @if( $errors->first('ticket')) is-invalid @endif">
                </div>
                <div class="col-12 col-lg-3 text-left p-1">
                    <label class="control-label" for="destination">Destination<span class="text-red">&nbsp;*</span></label>
                    <select class="form-control form-control-sm @if( $errors->first('destination')) is-invalid @endif " 
                        name="destination" 
                        id="destination">   
                        <option value="0" disabled selected>{{ __('page.please_select') }}</option> 
                        @if($countries)
                            @foreach($countries as $key => $val)
                            <option value="{{$val->id}}">{{ $val->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-12 col-lg-3 text-left p-1">
                    <label class="control-label" for="airlines">Airlines<span class="text-red">&nbsp;*</span></label>
                    <select class="form-control form-control-sm @if( $errors->first('airlines')) is-invalid @endif " 
                        name="airlines" 
                        id="airlines">   
                        <option value="0" disabled selected>{{ __('page.please_select') }}</option> 
                        @foreach($list_airlines as $key => $val)
                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row div_fix p-1">
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="ticket">Date of Flight<span class="text-red">&nbsp;*</span></label>
                    <input type="date" id="date_flight" name="date_flight" class="form-control form-control-sm @if( $errors->first('departure')) is-invalid @endif"" >
                </div>
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="ticket">Date when Ticket was issued<span class="text-red">&nbsp;*</span></label>
                    <input type="date" id="ticket_date" name="ticket_date" class="form-control form-control-sm @if( $errors->first('departure')) is-invalid @endif"" >
                </div>
                {{-- <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="ticket">RTT Validity<span class="text-red">&nbsp;*</span></label>
                    <input type="date" id="rtt_validity" name="rtt_validity" class="form-control form-control-sm @if( $errors->first('departure')) is-invalid @endif"" >
                </div> --}}
            </div>
            <div class="row div_fix p-1">
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label">{{ __('page.list_type_applicant')}}<span class="text-red">&nbsp;*</span></label>
                    <select class="form-control form-control-sm" 
                            name="list_type_applicant" 
                            id="list_type_applicant">   
                            <option value="">{{ __('page.please_select') }}</option>    
                            @foreach($type_applicant_reduced_list as $key => $val)
                            <option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
                            @endforeach    
                    </select>
                    <div class="select_error" id="error-list_type_applicant"></div>
                </div>
                <div class="col-6 col-lg-3  text-left p-1">
                    <div id="show_type_applicant" >
                    </div>
                </div>
                <div class="col-6 col-lg-3  text-left p-1">
                    <div id="show_applicant_to_upload">               
                    </div>
                </div>
            </div>
            <br>
            <hr></hr>
            <div class="row px-1 px-2">
                <div class="col-xs-1-12">
                    <h4><b><i class="mdi mdi-attachment text-primary"></i> ATTACHMENTS</b></h4>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <label class="control-label">Please upload scanned copies of the requirement below:</label>
                </div>
            </div>
            <div class="row div_fix px-1">
                <div class="col-6 col-lg-3 p-1 text-left">
                    <label class="control-label">{{ __('page.id_picture_2x2') }}</label>
                    <div class="pull-right" id="float_id_picture_2x2_link"></div>
                    <div id="link_id_picture_2x2"></div>
                    <input class="form-control form-control-sm" type="file" name="id_picture_2x2" id="id_picture_2x2" multiple="multiple">
                </div>
                <div class="col-6 col-lg-3 p-1 text-left">
                    <label class="control-label">{{ __('page.passport_identification_page') }}
                    </label>
                    <div class="pull-right" id="float_passport_identification_link"></div>
                    <div id="link_passport_identification_page"></div>
                    <input class="form-control form-control-sm" type="file" name="passport_identification_page[]" id="passport_identification_page" multiple="multiple">
                </div>
                <div class="col-6 col-lg-3 p-1 text-left">
                    <label class="control-label">{{ __('page.airline_ticket_issued') }}
                    </label>
                    <div class="pull-right" id="float_airline_ticket_link"></div>
                    <div id="link_ticket_booking_ref_no_"></div>
                    <input class="form-control form-control-sm" type="file" name="ticket_booking_ref_no_[]" id="ticket_booking_ref_no_" multiple="multiple">
                </div>
                <div class="col-6 col-lg-3 p-1 text-left">
                    <label class="control-label">{{ __('page.additional_file') }}
                    </label>
                    <div class="pull-right" id="float_additional_file_link"></div>
                    <div id="link_additional_file"></div>
                    <input class="form-control form-control-sm" type="file" name="additional_file[]" id="additional_file" multiple="multiple">
                </div>
            </div>
            <div class="row text-left p-1 px-2">
                <div class="col-xs-1-12">
                    <button class="btn btn-primary btn-lg px-5 "><i class="mdi mdi-check-circle"></i> SUBMIT</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
  function load_section_upload(type_applicant = null)
    {
        $('#show_applicant_to_upload').empty();

        if(type_applicant)
        {
            $('#type_applicant').val(type_applicant).trigger('change');
        }
        else
        {
            var type_applicant = $('#type_applicant').val();
        }

        if($('#type_applicant').val())
        {
            var ta = $('#type_applicant').val();
            if(ta == '29' || ta == '34')
            {
                var age = parseInt($('#age').val());
                if(age >= "12")
                {
                    if(age == "12")
                    {
                        var dof = new Date($('date_flight').val());
                        var dob = new Date($('birthdate').val());
                        if(dof != dob) restrictMinor();
                    }
                    else  restrictMinor();
                }  
            }
            axios.get("/online_section_upload", {
                params: {
                    section_id : type_applicant
                }
            })
            .then(function(response){
                const sections_uploads = response.data.sections_uploads;
               
                if(sections_uploads)
                {
                    $.each(sections_uploads, function( key, value ) {
                        $("#show_applicant_to_upload").append("<div class='form-group row'><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'><label class='control-label'>"+ value['description'] +"</label><div class='pull-right' id='link_file_" + key + "'><a class='' data-toggle='modal' data-target='#file_modal' id='additional_file_files' onclick=load_file('file_"+ key +"') href='javascript:void(0);'><span class='mdi mdi-hc-2x mdi-more'></span></a></div><input class='form-control form-control-xs' type='file' name='file_"+ key +"[]' id='file_"+ key +"' multiple='multiple' data-id='"+ value['id'] +"'></div><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><div id='file_fn_"+ key +"'></div></div>");
                    });
                }     

                if($('#type_applicant').is(':disabled'))
                {
                    $('#file_0').prop('disabled', true);
                    $('#file_1').prop('disabled', true);
                    $('#file_2').prop('disabled', true);
                    $('#file_3').prop('disabled', true);
                }        
            })
            .catch((error) => {
                alert_warning(error.response.data.errors, 1500);
            });
        }
    }
$(document).ready(function(){
    $('.minor_group').hide();
    $('.card-divider').addClass('d-none');
    $('#type_applicant').change(function(){
        $('#show_applicant_to_upload').empty();
            var type_applicant = $('#type_applicant').val();
            if($(this).val().length)
            {
                axios.get("/online_section_filter_by", {
                    params: {
                        section_id : $(this).val()
                    }
                })
                .then(function(response){
                    const sections_uploads = response.data.sections_uploads;
                    
                    if(sections_uploads)
                    {
                        $.each(sections_uploads, function( key, value ) {
                            $("#show_applicant_to_upload").append("<div class='form-group row'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><label class='control-label'>"+ value['description'] +"</label><div class='pull-right' id='link_file_" + key + "'><a class='' data-toggle='modal' data-target='#file_modal' id='additional_file_files' onclick=load_file('file_"+ key +"') href='javascript:void(0);'><span class='mdi mdi-hc-2x mdi-more'></span></a></div><input class='form-control form-control-xs' type='file' name='file_"+ key +"[]' id='file_"+ key +"' multiple='multiple'></div><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><div id='file_fn_"+ key +"'></div></div>");
                        });
                    }                        
                })
                .catch((error) => {
                    console.log(error.response.data.errors);
                    alert_warning(error.response.data.errors, 1500);
                });
            }
    });
    $('#is_minor').on('change',function()
    {
        var isminor = $(this).val();
        if(isminor == "1")  $('.minor_group').show();
        else $('.minor_group').hide();

    });
    $('#birthdate').on('change',function()
    {
        var dob = new Date($(this).val());
        var today = new Date();
        var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
        $('#age').val(age);

    });
    $('#list_type_applicant').on('change', function() {
        $('#show_type_applicant').empty();
        $('#type_accomodation').val('2').trigger('change');
        $('#amount').val('0.00');
        $('#form_group_oec').hide();

        if($(this).val() == '4') $('#form_group_oec').show();

        if($(this).val().length)
        {
            axios.get("/online_section_filter_by", {
                params: {
                    section_type : $(this).val()
                }
            })
            .then(function(response){
                const sections = response.data.sections;

                if(sections)
                {
                    var section_select_start = "<label class='control-label'>{{ __('page.type_applicant') }}<span class='text-red'>&nbsp;*</span></label><select class='form-control form-control-sm' name='type_applicant' id='type_applicant' onchange='load_section_upload()'><option value=''>{{ __('page.please_select') }}</option>";

                    var section_select_end = "</select><div class='select_error' id='error-type_applicant'></div>";

                    $.each(sections, function( key, value ) {
                        section_select_start = section_select_start + "<option value='"+ value['id'] +"'>"+value['code']+" - "+ value['name'] +"</option>";
                    });

                    $("#show_type_applicant").append(section_select_start + section_select_end);

                    $(".select2").select2({
                        width: '100%'
                    });
                }                        
            })
            .catch((error) => {
                alert_warning(error.response.data.errors, 1500);
            }).finally(function(){
                axios.get("{{ $based_url.'/rtt_application/json' }}", {
                    params: {
                        rtt_id : $('#rtt_id').val(),
                    }
                }).then(function(response){
                    const rtt_application = response.data.rtt_application;

                    $("#type_applicant").val('').trigger('change');

                    if(rtt_application['applicant_type_id'] == $('#list_type_applicant').val())
                    {
                        $("#type_applicant").val(rtt_application['type_applicant_id']).trigger('change'); 
                    }

                });
            });
        }   
    });

    $('#type_accomodation').on('change', function() {
        if($(this).val().length)
        {
            axios.get("{{ $based_url.'/applicant_fee/json' }}", {
                params: {
                    section_type : $('#list_type_applicant').val(),
                    type_applicant_fee : $(this).val(),
                    applicant_fee : 'no'
                }
            })
            .then(function(response){
                const rtt_amount = response.data.rtt_amount; 

                $('#amount').val('0.00');

                if(rtt_amount)
                {
                    $('#amount').val(numberWithCommas(rtt_amount['amount']));
                }              
            })
            .catch((error) => {
                alert_warning(error.response.data.errors, 1500);
            });
        }   
    });
  
});  

</script>