<div class="tec p-1">
    <form role="form" id="form_tec" action="/save_onlinetec" method="post"  enctype="multipart/form-data">
        {{ csrf_field() }}
        {{-- <a class="btn btn-warning" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
          <h4><i class="mdi mdi-info"></i> Notes for Multiple Passengers in one transaction</h4> 
        </a> --}}
        <div class="fields p-0">
            <div class="row px-2">
                <div class="col-xs-1-12">
                    <h4><b> <i class="mdi mdi-account text-primary "></i> PERSONAL INFORMATION</b></h4>
                </div>
            </div>
            <div class="row p-1 ">
                <div class="col-12 col-lg-3 text-left p-1">
                    <label class="control-label" for="last_name">Last Name<span class="text-red">&nbsp;*</span></label>
                    <input type="text" id="last_name" name="last_name"class="cf form-control  form-control-sm @if( $errors->first('last_name')) is-invalid @endif">
                </div>
                <div class="col-12 col-lg-3 text-left p-1">
                    <label class="control-label" for="last_name">First Name<span class="text-red">&nbsp;*</span></label>
                    <input type="text" id="first_name" name="first_name"  class="cf form-control form-control-sm @if( $errors->first('first_name')) is-invalid @endif">    </div>
                <div class="col-12 col-lg-1 text-left p-1">
                    <label class="control-label" for="last_name">M.I</label>
                    <input type="text" id="middle_name" name="middle_name" class="cf form-control form-control-sm">
                </div>
                <div class="col-12 col-lg-1 text-left p-1" >
                    <label class="control-label bold" for="last_name">Ext.</label>
                    <input type="text" id="ext_name" name="ext_name" class="cf form-control form-control-sm">
                </div>
            </div>
         
            <div class="row p-1 div_fix">
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="mobile_no">Mobile Number<span class="text-red">&nbsp;*</span></label>
                    <input type="text" id="mobile_no" name="mobile_no" class="form-control form-control-sm @if( $errors->first('mobile_no')) is-invalid @endif">
                </div>
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="email_address">Email Address<span class="text-red">&nbsp;*</span></label>
                    <input type="text" id="email_address" name="email_address" style="text-transform:lowercase !important" class="form-control form-control-sm  @if( $errors->first('email_address')) is-invalid @endif">
                </div>
                <div class="col-12 col-lg-6 text-left  p-1">
                </div>
            </div>
            <hr></hr>
            <div class="row px-1 px-2">
                <div class="col-xs-1-12">
                    <h4><b><i class="mdi mdi-flight-takeoff text-primary"></i> FLIGHT INFORMATION</b></h4>
                </div>
            </div>
            <div class="row p-1">
                <div class="col-12 col-lg-3  text-left p-1">
                    <label class="control-label" for="passport">Passport Number<span class="text-red">&nbsp;*</span></label>
                    <input type="text" id="passport" name="passport" class="cf form-control form-control-sm @if( $errors->first('passport')) is-invalid @endif">
                </div>
                <div class="col-12 col-lg-3  text-left p-1">
                    <label class="control-label" for="ticket">Ticket Number<span class="text-red">&nbsp;*</span></label>
                    <input type="text" id="ticket" name="ticket" class="cf form-control form-control-sm  @if( $errors->first('ticket')) is-invalid @endif">
                </div>
                <div class="col-12 col-lg-3 text-left p-1">
                    <label class="control-label" for="destination">Destination<span class="text-red">&nbsp;*</span></label>
                    <select class="form-control form-control-sm @if( $errors->first('destination')) is-invalid @endif " 
                        name="destination" 
                        id="destination">   
                        <option value="0" disabled selected>{{ __('page.please_select') }}</option> 
                        @if($countries)
                            @foreach($countries as $key => $val)
                            <option value="{{$val->id}}">{{ $val->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-12 col-lg-3 text-left p-1">
                    <label class="control-label" for="airlines">Airlines<span class="text-red">&nbsp;*</span></label>
                    <select class="form-control form-control-sm @if( $errors->first('airlines')) is-invalid @endif " 
                        name="airlines" 
                        id="airlines">   
                        <option value="0" disabled selected>{{ __('page.please_select') }}</option> 
                        @foreach($list_airlines as $key => $val)
                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row div_fix p-1">
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="ticket">Date of Flight<span class="text-red">&nbsp;*</span></label>
                    <input type="date" id="departure_date" name="departure_date" class="form-control form-control-sm @if( $errors->first('departure')) is-invalid @endif"" >
                </div>
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="ticket">Date when Ticket was issued<span class="text-red">&nbsp;*</span></label>
                    <input type="date" id="ticket_date" name="ticket_date" class="form-control form-control-sm @if( $errors->first('departure')) is-invalid @endif"" >
                </div>
                {{-- <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label" for="ticket">TEC Validity<span class="text-red">&nbsp;*</span></label>
                    <input type="date" id="tec_validity" name="tec_validity" class="form-control form-control-sm @if( $errors->first('departure')) is-invalid @endif"" >
                </div> --}}
            </div>
            <br>
            <hr></hr>
            <div class="row px-1 px-2">
                <div class="col-xs-1-12">
                    <h4><b><i class="mdi mdi-attachment text-primary"></i> ATTACHMENTS</b></h4>
                </div>
            </div>
            <div class="row div_fix p-1">
                <div class="col-6 col-lg-3  text-left p-1">
                    <label class="control-label">Type of Applicant<span class="text-red">&nbsp;*</span></label>
                    <select class="form-control form-control-sm @if( $errors->first('ft_destination')) is-invalid @endif " 
                        name="type_applicant" 
                        id="type_applicant">   
                            <option value="">{{ __('page.please_select') }}</option>
                            @foreach($list_type_applicants as $key => $val)
                                <option value="{{ $val->id }}">{{ $val->code.' - '.$val->name }}</option>
                            @endforeach
                    </select>
                </div>
                <div class="col-6 col-lg-6  text-left p-1">
                    <span id="show_applicant_to_upload">               
                    </span>
                    
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <label class="control-label">Please upload scanned copies of the requirement below:</label>
                </div>
            </div>
            <div class="row div_fix px-1">
                <div class="col-6 col-lg-3 p-1 text-left">
                    <label class="control-label">{{ __('page.id_picture_2x2') }}</label>
                    <div class="pull-right" id="float_id_picture_2x2_link"></div>
                    <div id="link_id_picture_2x2"></div>
                    <input class="form-control form-control-sm" type="file" name="id_picture_2x2" id="id_picture_2x2" multiple="multiple">
                </div>
                <div class="col-6 col-lg-3 p-1 text-left">
                    <label class="control-label">{{ __('page.passport_identification_page') }}
                    </label>
                    <div class="pull-right" id="float_passport_identification_link"></div>
                    <div id="link_passport_identification_page"></div>
                    <input class="form-control form-control-sm" type="file" name="passport_identification_page[]" id="passport_identification_page" multiple="multiple">
                </div>
                <div class="col-6 col-lg-3 p-1 text-left">
                    <label class="control-label">{{ __('page.airline_ticket_issued') }}
                    </label>
                    <div class="pull-right" id="float_airline_ticket_link"></div>
                    <div id="link_ticket_booking_ref_no_"></div>
                    <input class="form-control form-control-sm" type="file" name="ticket_booking_ref_no_[]" id="ticket_booking_ref_no_" multiple="multiple">
                </div>
                <div class="col-6 col-lg-3 p-1 text-left">
                    <label class="control-label">{{ __('page.additional_file') }}
                    </label>
                    <div class="pull-right" id="float_additional_file_link"></div>
                    <div id="link_additional_file"></div>
                    <input class="form-control form-control-sm" type="file" name="additional_file[]" id="additional_file" multiple="multiple">
                </div>
            </div>
            <div class="row text-left p-1 px-2">
                <div class="col-xs-1-12">
                    <button class="btn btn-primary btn-lg px-5 " id="btn_submit"><i class="mdi mdi-check-circle"></i> SUBMIT</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.card-divider').addClass('d-none');
    $('#type_applicant').change(function(){
        $('#show_applicant_to_upload').empty();
            var type_applicant = $('#type_applicant').val();
            if($(this).val().length)
            {
                axios.get("/online_section_upload", {
                    params: {
                        section_id : $(this).val()
                    }
                })
                .then(function(response){
                    const sections_uploads = response.data.sections_uploads;
                     
                    console.log(sections_uploads);
                    if(sections_uploads)
                    {
                        $.each(sections_uploads, function( key, value ) {
                            $("#show_applicant_to_upload").append("<div class='form-group row'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><label class='control-label'>"+ value['description'] +"</label><div class='pull-right' id='link_file_" + key + "'><a class='' data-toggle='modal' data-target='#file_modal' id='additional_file_files' onclick=load_file('file_"+ key +"') href='javascript:void(0);'><span class='mdi mdi-hc-2x mdi-more'></span></a></div><input class='form-control form-control-xs' type='file' name='file_"+ key +"[]' id='file_"+ key +"' multiple='multiple'></div><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><div id='file_fn_"+ key +"'></div></div>");
                        });
                    }                        
                })
                .catch((error) => {
                    console.log(error.response.data.errors);
                    alert_warning(error.response.data.errors, 1500);
                });
            }
    });
    $('#btn_submit').click(function(e){
        e.preventDefault();
        Swal.fire({
        title: '', 
        html: '<b>Are you sure you want to save this record?</b> <br> If yes, click the continue button, otherwise, cancel to review your details.',
        type:'question',
        confirmButtonText: 'Continue',
        showCancelButton: true,
        cancelButtonText: 'Cancel'
        }).then((result) => {
        if (result.value) {
            $('#form_tec').submit();
        }
        });
    });
});  
</script>