<style>
    #table4 tbody td.details-control {
        background-image: url('{{ asset('img/plus.png') }}');
        cursor: pointer;
        background-repeat:  no-repeat;
        background-position: center;
        background-origin: content-box;
        background-size: cover;
        padding: 18px;
    }
    #table4 tbody tr.shown td.details-control {
        background-image: url('{{ asset('img/minus.png') }}');
        cursor: pointer;
        background-repeat:  no-repeat;
        background-position: center;
        background-origin: content-box;
        background-size: cover;
        padding: 18px;
    }

    #table4 tbody tr .rowDetails p {
        font-size: 14px;
        font-weight: 800;
        float: left;
        margin-right: 10px;
        padding: 1px;
        margin-bottom: 0;
    }
    #table4 tbody tr .rowDetails a{

    }
    #table4 tbody tr .rowDetails td{
        padding: 5px;
    }
    </style>

<table class="table table-striped bg-white table-hover table-fw-widget" id="table4">
    <thead>
        <tr>
        <th width="5%"></th>
        <th width="10%">Transaction Type</th>
        <th width="25%">Name</th>
        <th width="15%">Passport No.</th>
        <th width="15%">Ticket No.</th>
        <th width="15%">Application Date</th>
        <th width="20%">Departure Date</th>
        </tr>
    </thead>
    <tbody>
    
    </tbody>
</table>
<script type="text/javascript">
$(document).ready(function () {   
    App.init();
    App.dataTables();
    loadDatatable();   

    function loadDatatable()
    {

        
        @php
        $columns = array(['className' => 'details-control','ordering'=>'false','data'=>'null','defaultContent'=>'']
        ,['data' => 'trans_type'],['data' => 'name'], ['data' => 'passport_no'], ['data' => 'ticket_no'], ['data' => 'date_application'],['data' => 'departure_date']
        
        
        );
        $url = '/list_online_datatable';
        @endphp

        load_datables('#table4', "/list_online/datatables" ,{!! json_encode($columns) !!}, 5,"desc");

    }
    let table = $("#table4").DataTable();
    $('#table4 tbody').on('click', 'td', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
       
        if ( row.child.isShown() ) {
          
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
           
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

    function format ( data ) {
     
        var row_start = '<div class="row">';
        var info1 = '<div class="col-4">'+
           '  <table class="table table-bordered" >'+
            '<tr>'+
                '<td width="25%">Email Address</td>'+
                '<td width="25%">'+data.email+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td width="25%">Mobile Number</td>'+
                '<td width="25%">'+data.mobile_no+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td width="25%">Country</td>'+
                '<td width="25%">'+data.country+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td width="25%">Airlines</td>'+
                '<td width="25%">'+data.airlines+'</td>'+
            '</tr>'+
            '</table></div>';
        var info2 ='<div class="col-4">'+
           '  <table class="table table-bordered" >'+
            '<tr>'+
                '<td width="25%">Email Address</td>'+
                '<td width="25%">'+data.email+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td width="25%">Mobile Number</td>'+
                '<td width="25%">'+data.mobile_no+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td width="25%">Country</td>'+
                '<td width="25%">'+data.country+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td width="25%">Airlines</td>'+
                '<td width="25%">'+data.airlines+'</td>'+
            '</tr>'+
            '</table></div>';

        var row_end ='</div>';
        return  row_start+info1+info2+row_end;
    }
});
</script>