@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
    @include('layouts.auth-partials.form-css')
<style>
body{
    font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif !important;
   
}
.card{
    background-color: white !important;
}
</style>
@endsection

@section('content')
  

    @php 
        $data = [
        'option' => $option, 
        'title' => 'online_list', 
        'has_icon' => $icon, 
        'has_file' => $file,
        'has_footer' => 'yes', 
        'cancel_url' => url('application/fulltax')
        ];
        // $data['isSave'] = 'yes';
    @endphp

    @include('others.main_content', $data)
    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' => route('fulltax.store'), 'frm_id' => 'save_form'])
    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' =>  $application_url.'/'.$option.'/remove', 'frm_id' => 'remove_form'])
    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' =>  $application_url.'/'.$option.'/update_residency', 'frm_id' => 'update_form'])


@endsection

@section('scripts')


@yield('additional-scripts')
@include('layouts.auth-partials.datatables-scripts')
        
    <script type="text/javascript">
        $(document).ready(function(){
             $('#main-content').addClass('be-nosidebar-left');
            $('.main').removeClass('col-10');
            $('.main').addClass('col-10 col-xl-10 offset-1 offset-xl-1');

        });
    

        
     
    </script>
@endsection