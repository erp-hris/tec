
@if ($message = Session::get('success'))
<div class="row">
    <div class="col-12">
        <div role="alert" class="alert alert-contrast alert-success alert-dismissible">
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close">
                    <span aria-hidden="true" class="mdi mdi-close"></span>
                </button>
                <strong>Alert!</strong> {{ $message }}
            </div>
        </div>
    </div>
</div>
@endif


@if ($message = Session::get('error'))
<div class="row">
    <div class="col-12">
        <div role="alert" class="alert alert-contrast alert-danger alert-dismissible">
            <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
            <div class="message">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close">
                    <span aria-hidden="true" class="mdi mdi-close"></span>
                </button>
                <strong>Alert!</strong> {{ $message }}
            </div>
        </div>
    </div>
</div>
@endif


@if ($message = Session::get('warning'))
<div class="row">
    <div class="col-12">
        <div role="alert" class="alert alert-contrast alert-warning alert-dismissible">
            <div class="icon"><span class="mdi mdi-alert-triangle"></span></div>
            <div class="message">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close">
                    <span aria-hidden="true" class="mdi mdi-close"></span>
                </button>
                <strong>Alert!</strong> {{ $message }}
            </div>
        </div>
    </div>
</div>
@endif


@if ($message = Session::get('info'))
<div class="row">
    <div class="col-12">
        <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
            <div class="icon"><span class="mdi mdi-info-outline"></span></div>
            <div class="message">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close">
                    <span aria-hidden="true" class="mdi mdi-close"></span>
                </button>
                <strong>Alert!</strong> {{ $message }}
            </div>
        </div>
    </div>
</div>
@endif

@if ($message = Session::get('save-success-online'))
<div class="row">
    <div class="col-10 offset-1">
        <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
            <div class="icon"><span class="mdi mdi-info-outline"></span></div>
            <div class="message">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close">
                    <span aria-hidden="true" class="mdi mdi-close"></span>
                </button>
                <strong>{{$type}}</strong> {{ $message }}
            </div>
        </div>
    </div>
</div>
@endif