            function isEmpty(property) 
            {
                return (property === null || property === "" || typeof property === "undefined");
            }

            function check_response_is_null(data)
            {
                if(data !== 'undefined' && data !== '' && data !== null){
                    return data;
                }

                return false;
            }

            function load_profile_picture(id, file, target)
            {
                $(target).empty();

                var data = check_response_is_null(file);

                var append_class = 'rounded mr-2 mb-2';

                if(target == '.user-display-avatar')
                {
                    append_class = '';
                }

                if(data == '')
                {
                    $(target).append('<img class="'+ append_class +'" width="100px" height="100px" src="{{ asset('beagle-assets/img/avatar-150.png')}}" alt="Avatar">');
                }
                else
                {
                    axios.get("{{ url('/check_file_exist') }}", {
                        params: {
                            emp_id : id,
                            image_path : file,
                        }
                    }).then(function (response) {
                        const image = response.data.file;

                        if(image)
                        {
                            $(target).append('<img class="'+ append_class +'" width="100px" height="100px" src="{{ asset('/tieza/attachment') }}/' + id + '/' + file +'" alt="Avatar">');
                        }
                        else
                        {
                            $(target).append('<img class="'+ append_class +'" width="100px" height="100px" src="{{ asset('beagle-assets/img/avatar-150.png')}}" alt="Avatar">');
                        }
                    })
                }
            }