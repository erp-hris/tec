	@section('employees_css')
        @include('layouts.auth-partials.form-css')
    @endsection

        @php
            $months = array(
                ['id' => "01", 'month' => 'January'],
                ['id' => "02", 'month' => 'February'],
                ['id' => "03", 'month' => 'March'],
                ['id' => "04", 'month' => 'April'],
                ['id' => "05", 'month' => 'May'],
                ['id' => "06", 'month' => 'June'],
                ['id' => "07", 'month' => 'July'],
                ['id' => "08", 'month' => 'August'],
                ['id' => "09", 'month' => 'September'],
                ['id' => "10", 'month' => 'October'],
                ['id' => "11", 'month' => 'November'],
                ['id' => "12", 'month' => 'Decemeber']
            );

            $year = date('Y');
        @endphp

    <div class="card">
        <div class="card-header">
            <div class="row margin-top">
                <label class="col-form-label text-sm-right">{{ __('page.filter_by') }}</label>
            </div>

            <div class="row margin-top">
                <label class="control-label">Month</label>
                <select class="select2 select2-xs" id="filter_month" name="filter_month">
                    <option value="all">{{ __('page.please_select') }}</option>
                    @foreach($months as $key => $val)
                        <option value="{{ $val['id'] }}">{{ $val['month'] }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row margin-top">
                <label class="control-label">Year</label>
                <select class="select2 select2-xs" id="filter_year" name="filter_year">
                    <option value="all">{{ __('page.please_select') }}</option>
                    @for($i = 2020; $i <= $year; $i++)
                        <option value="{{ $year }}">{{ $year }}</option>
                    @endfor
                </select>
            </div>

            <div class="row margin-top">
                <label class="control-label">Type of Refund</label>
                <select class="select2 select2-xs" id="filter_form" name="filter_form">
                    <option value="all">{{ __('page.please_select') }}</option>
                    @foreach($travel_tax_refund_form as $key => $val)
                        <option value="{{ $val['id'] }}">{{ __('page.'.$val['name']) }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row margin-top">
                <label class="control-label">Sort by</label>
                <select class="select2 select2-xs" id="filter_order" name="filter_order">
                    <option value="1">{{ __('page.ascending') }}</option>
                    <option value="0">{{ __('page.descending') }}</option>
                </select>
            </div>

            <div class="row margin-top">
                <input type="text" class="form-control form-control-xs" name="filter_value" id="filter_value" placeholder="{{ __('page.search_name') }}">
            </div>

            <div class="row margin-top">
                <a href="javascript:void(0);" class="btn btn-space btn-primary hover" id="filter_btn"><span class="mdi mdi-search"></span>&nbsp;{{ __('page.filter') }}</a>
                <a href="javascript:void(0);" class="btn btn-space btn-secondary hover" id="cancel_btn"><span class="mdi mdi-close"></span>&nbsp;{{ __('page.cancel') }}</a>
            </div>
        </div>  
        <div class="card-body">
            <div class="row" style="max-height: 200px; overflow: auto;">
                <div class="col-md-12">
                    <div id="list_tr_application"></div>
                </div>     
            </div>
        </div>
        <div class="card-divider"></div>
        <div class="card-footer">
            <div id="total_tr_application"></div>
        </div>        
    </div>

    @section('employees_scripts')
    	@include('layouts.auth-partials.form-scripts')

    	<script type="text/javascript">
            $(document).change(function(){
                $('input[type="checkbox"]').on('change', function() {
                    $(this).prop('checked', true);
                    $('input[type="checkbox"]').not(this).prop('checked', false);
                });
            });

            $(document).ready(function () {
                App.formElements();

                var filter_month;
                var filter_year;
                var filter_form;
                var filter_order;
                var filter_value;
                
                filter_month = localStorage.getItem("filter_month");
                filter_year = localStorage.getItem("filter_year");
                filter_form = localStorage.getItem("filter_form");
                filter_order = localStorage.getItem("filter_order");
                filter_value = localStorage.getItem("filter_value");
                
                localStorage.clear();

                localStorage.setItem('filter_month', filter_month);
                localStorage.setItem('filter_year', filter_year);
                localStorage.setItem('filter_form', filter_form);
                localStorage.setItem('filter_order', filter_order);
                localStorage.setItem('filter_value', filter_value);

                if(filter_order)
                {  
                    if(filter_form || filter_month || filter_year)
                    {
                        $('#filter_month').val(filter_month).trigger('change');
                        $('#filter_year').val(filter_year).trigger('change');
                        $('#filter_form').val(filter_form).trigger('change');
                        $('#filter_order').val(filter_order).trigger('change');
                        $('#filter_value').val(filter_value);

                        filter_tr_application(filter_month, filter_year, filter_form, filter_order, filter_value);
                    }
                    else
                    {
                        $('#filter_order').val(filter_order).trigger('change');
                        $('#filter_value').val(filter_value);

                        filter_tr_application(null, null, null, filter_order, filter_value);
                    }
                }
                else
                {
                    load_list_tr_application();
                }
            });

            $('#filter_value').keypress(function(e){
                if(e.which == 13){
                    $('#filter_btn').click();
                }
            });

            $('#filter_btn').click(function(){
                var filter_month = $('#filter_month').val();
                var filter_year = $('#filter_year').val();
                var filter_form = $('#filter_form').val();
                var filter_order = $('#filter_order').val();
                var filter_value = $('#filter_value').val();

                localStorage.setItem("filter_month", filter_month);
                localStorage.setItem("filter_year", filter_year);
                localStorage.setItem("filter_form", filter_form);
                localStorage.setItem("filter_order", filter_order);
                localStorage.setItem("filter_value", filter_value);

                filter_tr_application(filter_month, filter_year, filter_form, filter_order, filter_value);
            });

            $('#cancel_btn').click(function(){
                load_list_tr_application();
            });

            function load_list_tr_application()
            {
                $("#list_tr_application").empty();
                $("#total_tr_application").empty();
                
                $('#filter_month').val('all').trigger('change.select2');
                $('#filter_year').val('all').trigger('change.select2');
                $('#filter_form').val('all').trigger('change.select2');
                $('#filter_order').val('1').trigger('change.select2');
                $('#filter_value').val('');
                
                localStorage.removeItem("filter_month");
                localStorage.removeItem("filter_year");
                localStorage.removeItem("filter_form");
                localStorage.removeItem("filter_order");
                localStorage.removeItem("filter_value");

                axios.get("{{ $based_url.'/tr_application/json' }}", {
                    params: {
                        filter_month : 'all',
                        filter_year : 'all',
                        filter_form : 'all',
                        filter_order : 1,
                    }
                })
                .then(function(response){
                    const tr_applications = response.data.tr_applications;
                    const total_record = response.data.count_tr_application;

                    $.each(tr_applications, function( key, value ) {
                        $("#list_tr_application").append("<div class='custom-control custom-checkbox'><input class='custom-control-input' type='checkbox' id='"+ value['id'] +"' data-id='"+ value['id'] +"' onclick=select_application('"+ value['id'] +"') name='employee'><label class='custom-control-label' for='"+ value['id'] +"'>"+ value['full_name'] +"</label></div>")
                    });

                    $("#total_tr_application").append('<label class="control-label">Total Record: '+total_record+'</label>');
                })
                .catch((error) => {
                    alert_warning(error.response.data.errors, 1500);
                })
            }

            function filter_tr_application(filter_month, filter_year, filter_form, filter_order, filter_value)
            {
                axios.get("{{ $based_url.'/tr_application/json' }}", {
                    params: {
                        filter_month : filter_month,
                        filter_year : filter_year,
                        filter_form : filter_form,
                        filter_order : filter_order,
                        filter_value : filter_value,
                    }
                })
                .then(function(response) {
                    $("#list_tr_application").empty();
                    $("#total_tr_application").empty();

                    const tr_applications = response.data.tr_applications;
                    const total_record = response.data.count_tr_application;

                    if($.trim(tr_applications))
                    {   
                        $.each(tr_applications, function( key, value ) {
                            $("#list_tr_application").append("<div class='custom-control custom-checkbox'><input class='custom-control-input' type='checkbox' id='"+ value['id'] +"' data-id='"+ value['id'] +"' onclick=select_application('"+ value['id'] +"') name='employee'><label class='custom-control-label' for='"+ value['id'] +"'>"+ value['full_name'] +"</label></div>")
                        });

                        $("#total_tr_application").append('<label class="control-label">Total Record: '+total_record+'</label>');
                    }
                    else
                    {
                        load_list_tr_application();
                        alert_warning("{{ __('page.no_record_found') }}", 1500);
                    }
                })
            }

            @include('others.page_script')
        </script>
   	@endsection