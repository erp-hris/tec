<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style type="text/css">
        .event-background{
            background: url("<?php echo $message->embed(public_path('img/watermark_bg_0.png')); ?>");
            background-position: center;
            background-size: cover;
            background-color: #eeeeee;
            height: auto;
            width: 100%;
        }
    </style>

    
</head>
<body class="">
    <style>
        @media only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>


    <div class="container">
        
    <table class="wrapper event-background" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <table class="content" width="100%" cellpadding="0" cellspacing="0">
    
                    <!-- Email Body -->
                    <tr>
                        <td class="body" width="100%" cellpadding="0" cellspacing="0">
                            <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0">

                                <tr>
                                    <td class="header">
                                        <h1>Greetings from the TIEZA!</h1>
                                    </td>
                                </tr>
                            	<tr>
                                    <p>
        								We have completed the evaluation of your Reduced Travel Tax Certificate (RTT) Application, which was filed online through <a href="#">traveltaxservices@tieza.gov.ph.</a>
                                    </p>
                                    <p>
                                        Please find attached e-mail for the result of your evaluation.
                                    </p>
                                    <p>
                                        For questions or clarifications regarding the result of your online RTT Application please email  <a href="#">traveltax.helpdesk@tieza.gov.ph.</a>
                                    </p>
                                    <p>
                                        Thank you for availing of our travel tax services online. We are glad to have served you.
                                    </p>
								</tr>
								<tr>
								
								</tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
