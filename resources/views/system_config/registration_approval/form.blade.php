<div class="row">
          <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label text-red">{{ __('page.required_fields') }}</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
              <div class="card card-table">
                <div class="card-header">Applicant Registration List
                  <div class="tools dropdown"><span class="icon mdi mdi-download"></span><a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"><span class="icon mdi mdi-more-vert"></span></a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a>
                      <div class="dropdown-divider"></div><a class="dropdown-item" href="#">Separated link</a>
                      </div>
                  </div>
                </div>
                <div class="card-body">
                  <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                     <tr>
                        <td>Email Address</td>
                        <td>Date Registered</td>
                        <td> </td>

                     </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                  </table>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
     
      
    });

    function approve_decline(id,type)
    {
      var text,success,color,button,title;
      if(type == "1") {
        title = "Approve";
        text = "Are you sure you want to approve this request?";
        success = "Approved Successfully!";
        color = "colored-header colored-header-primary"
        button = "btn btn-primary";
      }
      else if(type == "2"){
        title = "Decline";
        text = "Are you sure you want to decline this request?"
        success = "Declined Successfully!";
        color = "colored-header colored-header-danger";
        button = "btn btn-danger";
      } 

      const swal_continue = alert_continue(title, text,button, color);
          swal_continue.then((result) => {
            
              if(result.value){

                axios({
                    method: 'post',
                    url: "registration_approval/update",
                    data: {
                      id: id,
                      type: type
                    }
                  })
                  .then(function (response) {
                    const swal_success = alert_success(success, 1500);
                                swal_success.then((response) => {

                                  loadDatatable();

                                });
                    
                  })  
                  .catch((error) => {
                      const errors = error.response.data.errors;

                      if(typeof(errors) == 'string')
                      {
                          alert_warning(errors);
                      }
                      else
                      {
                          const firstItem = Object.keys(errors)[0];
                          const firstItemDOM = document.getElementById(firstItem);
                          const firstErrorMessage = errors[firstItem][0];

                          firstItemDOM.scrollIntoView();

                          alert_warning("{{ __('page.check_inputs') }}", 1500);

                          showErrors(firstItem, firstErrorMessage, firstItemDOM, ['user_level', 'user_status']);
                      }
                    });  
              }
            });
    }
   
  </script>
@endsection