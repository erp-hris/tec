@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
@endsection

@section('content')
  

    @php 
        $data = [
        'option' => $option, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $file,
        'has_footer' => 'yes', 
        'cancel_url' => url('system_config/'.$option)
        ];

        
    @endphp

    @include('others.main_content', $data)
@endsection

@section('scripts')

@include('layouts.auth-partials.datatables-scripts')
@yield('additional-scripts')
        
    <script type="text/javascript">
        $(document).ready(function(){
            // $('#main-content').addClass('be-aside');
            App.init();
        	App.dataTables();
            loadDatatable();        
        });
    

        function loadDatatable()
        {
           
        
            @php
                   
            $columns = array(['data' => 'email', 'sortable' => false], ['data' => 'created_at'],['data' => 'approve']);
            @endphp
            load_datables('#table4', "{!! $based_url.'/registration_approval/datatables' !!}" ,{!! json_encode($columns) !!}, null);

        }
     
    </script>
@endsection