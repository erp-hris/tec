		
		<div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.full_name') }}</label>
                        <input type="text" class="form-control form-control-xs" value="{{ Auth::user()->getFullname() }}" readonly>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.email_address') }}</label>
                        <input type="text" class="form-control form-control-xs" value="{{ Auth::user()->email }}" readonly>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row" id="frm_input_user_level">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label class="control-label">{{ __('page.user_level')}}</label>
                    <select class="select2 select2-xs" 
                            name="user_level" 
                            id="user_level"
                            disabled>   
                            <option value="">{{ __('page.please_select') }}</option>
                            @foreach($user_level as $key => $val)
                                <option value="{{ $val['level'] }}" {{ Auth::user()->level == $val['level'] ? 'selected' : '' }}>{{ __('page.'.$val['name']) }}</option>
                            @endforeach
                    </select>
                <div class="select_error" id="error-user_level"></div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.old_password') }}</label>
                        <input type="password" class="form-control form-control-xs" name="old_password" id="old_password" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

		<div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.new_password') }}</label>
                        <input type="password" class="form-control form-control-xs" name="new_password" id="new_password" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.confirm_password') }}</label>
                        <input type="password" class="form-control form-control-xs" name="confirm_password" id="confirm_password" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        @section('additional-scripts')
            <script type="text/javascript">
                $("#save_btn").click(function() {
                    var frm, text, title, success;

                    frm = document.querySelector('#edit_form');
                    title = "{{ __('page.edit_'.$option) }}";
                    text = "{{ __('page.update_this') }}";
                    success = "{{ __('page.updated_successfully', ['attribute' => trans('page.'.$option)]) }}";

                    const swal_continue = alert_continue(title, text);
                    swal_continue.then((result) => {
                        clearErrors();
                        if(result.value){
                            var formData = new FormData();

                            @foreach($default_inputs as $key => $val)
                                formData.append("{{ $val['input_name'] }}", $("#{{ $val['input_name'] }}").val());
                            @endforeach

                            axios.post(frm.action, formData)
                            .then((response) => {
                                const swal_success = alert_success(success, 1500);
                                swal_success.then((value) => {
                                    window.location.href="{{ $cancel_url }}";
                                });
                            })
                            .catch((error) => {

                                const errors = error.response.data.errors;

                                if(typeof(errors) == 'string')
                                {
                                    alert_warning(errors);
                                }
                                else
                                {
                                    alert_warning("{{ __('page.check_inputs') }}");

                                    const firstItem = Object.keys(errors)[0];
                                    const firstItemDOM = document.getElementById(firstItem);
                                    const firstErrorMessage = errors[firstItem][0];

                                    firstItemDOM.scrollIntoView();
                                    showErrors(firstItem, firstErrorMessage, firstItemDOM);
                                }
                            });
                           
                            
                        }
                    })
                });

                function select_account(id)
                {
                    console.log(id);
                }
            </script>
        @endsection