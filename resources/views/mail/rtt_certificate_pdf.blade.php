<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

    <style type="text/css">
        body {
            background: white;
            font-size: 10pt;
            font-family: Cambria /**Courier New**/;
            color: black;
        }
        input {
            border: 1px solid black;
        }
        .canvas {
             background-image: url('{{ asset('img/watermark_bg_0.png') }}');
             background-position: center;
             background-repeat: no-repeat;
        }

        img {
            display:block;
            image-orientation: none
        }

        .center {
          display: block;
          margin-left: auto;
          margin-right: auto;
          width: 50%;
        }

        .image-container {
            vertical-align: bottom;
        }

        .image-esig {
            vertical-align: bottom;
            margin-top: 50px;
            margin-bottom: 0px;
            bottom: 0px;

            /** 
                Optional: provide a max width to the image
                in case it has a higher resolution
            **/
            max-width: 300px;
        }

    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="col-12" style="border: 1px solid black; padding: 20px;">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8 canvas">
                    <div class="row">
                        <div class="col-xs-2"><b><u>AIRLINE COPY</u><br>
                            Form 355 </b><br>(Rev 2010)</div>
                        <div class="col-xs-6 text-center" style="font-size: 12pt;position: relative;position: relative;top: 15px;"><b>REDUCED<br>TRAVEL TAX CERTIFICATE</b>
                        </div>
                        <div class="col-xs-2">
                            <div style="height: 100px; width: 100px; border: 1px solid black;">
                                <image src="{{ url('/images_rtt/'.$ta->id.'/'.$ta->id_picture_2x2_fn) }}" width="100px" height="100px" alt="id_picture_2x2_fn" />
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row margin-top" style="text-transform: justify;">
                        <div class="col-xs-12">
                            This is to certify that <b>Mr. / Ms.</b>&nbsp;<span>&nbsp;<b><u>{{ strtoupper($ta->ffull_name) }}</u></b>&nbsp;</span> with passport number <span>&nbsp;<b><u>{{ $ta->passport_no }}</u></b>&nbsp;</span> is entitled to reduced travel tax of <b><u>P {{ $ta->reduced_amount }}</u></b> persuant to <b>Section </b>&nbsp;<span>&nbsp;<b><u>{{ $ta->section_code }}</u></b>&nbsp;</span> of Presidential Decree (P.D.) 1183, as amended.                        
                        </div>
                    </div>
                    <br> 
                    <div class="row margin-top" style="text-align: justify;">
                        <div class="col-xs-9">
                            <br>
                            <br>
                            <div class="row margin-top">
                                <div class="col-xs-12">
                                    Reservation Code/No. <span>&nbsp;<b><u>{{ $ta->ticket_no }}</u></b>&nbsp;</span>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    Date Issued: <span>&nbsp;<b><u>{{ date("m/d/Y", strtotime($ta->date_application)) }}</u></b>&nbsp;</span>
                                </div>
                            </div>
                            <br >
                            <div class="row">
                                <div class="col-xs-12">
                                    RTT No. <span>&nbsp;<b><u>{{ $ta->rtt_id }}</u></b>&nbsp;</span>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br> 
                            <div class="row">
                                <div class="col-xs-12">
                                    <B>IMPORTANT REMINDERS:</B>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <b>1. Valid only for issuance of ticket.</b>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <b>2. Valid only for travel to:&nbsp;&nbsp;&nbsp;<u>{{ strtoupper($ta->country_name) }}</u>&nbsp;</b>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <b>3. Not valid if departing after:&nbsp;&nbsp;&nbsp;<b><u>{{ date("m/d/Y", strtotime($ta->date_validity)) }}</u></b>&nbsp;</b>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <b>4. Not valid with erasures/alterations.</b>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <b>5. Valid only for one departure.</b>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <b>6. Please print this document for submission to Airline.</b>
                                </div>
                            </div>
                            <br>
                        </div>
                        <div class="col-xs-3">
                            <div class="row margin-top">
                                <div class="col-xs-12">
                                    <div style="position: relative;left: -65px;">
                                        <div id="qrcode">
                                            <image src="{{ url('/qr_code_rtt/'.$ta->rtt_id.'_qrcode.png') }}" width="180px" height="180px" alt="qr_code" />
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div style="position: relative;left: -65px;">
                                    <div class="col-xs-12 text-center">
                                        <div class="image-container">
                                            <image class="image-esig"  src="{{ asset('esig/cristeto_ocampo_.png') }}" width="140px" height="140px"/>
                                        </div>
                                        <span style="font-weight: 800;">
                                            {{ __('page.oic_name') }}
                                        </span>
                                        <br>
                                        <i>{{ __('page.oic_position') }}</i>
                                        <br>
                                        <i>{{ __('page.oic_department') }}</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div> 
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="row margin-top">
                        <div class="col-xs-6">{{ date('m/d/Y H:i:s') }}
                        </div>
                        <div class="col-xs-5 text-right">{{ $ta->supervisor_code.'-'.$ta->processor_code }}
                        </div>
                    </div>
                </div>
                <div class="col-2"></div>
            </div>
        </div> 
    </div>


    
    @include('layouts.auth-partials.scripts')
    @include('layouts.auth-partials.form-scripts')
    <script src="{{ asset('js/custom.js')}}"></script>
</body>

</html>