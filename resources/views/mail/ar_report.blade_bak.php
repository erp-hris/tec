<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')

    @include('layouts.auth-partials.css')
    @yield('css')
    <link rel="stylesheet" href="{{ asset('beagle-v1.7.1/src/assets/css/app.css') }}" type="text/css"/>
    <style type="text/css">
        body {
            background: white;
            font-size: 14pt;
            font-family: Cambria /**Courier New**/;
            color: black;
        }
        input {
            border: 1px solid black;
        }
        .canvas {
             /* background-image: url('{{ asset('img/watermark_0.png') }}');
             background-position: center;
             background-repeat: no-repeat; */

        }

        .bg-triangle {
             background-image: url('{{ asset('img/bg-tieza2.png') }}');
             -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;
        }

        img {
            display:block;
        }

        .center-image {
            display:block;
            margin-left: auto;
            margin-right: auto;
            width:8%;
        }
        .centers{
            text-align: center !important;
        }
    </style>
</head>
<body>
    <div class="container-fluid bg-triangle" style=" padding: 20px;">
        <br><br>
        <div class="col-md-12">
            <div class="row" >
                <div class="col-md-1 col-sm-1"></div>
                <div class="col-md-10 col-sm-10 ">
                    <div class="row center-image">
                            <img src="{{ asset('img/tieza-logo.png')}}" alt="logo" srcset="" class=" " style="height:100px;">
                    </div>
                    <br>
                    <div class="centers">
                        <h3 style="font-weight:bold;">Successful Payment Transaction </h3>
                    </div>
                    <br>
                    <div class="">
                        <h4 style="font-weight:bold">AR NUMBER: {{$ar_no}}  </h4>
                    </div>
                    <br>
                    <div class="">
                       Hello <b>{{$fulltax->first_name.' '.$fulltax->last_name}}</b>
                    </div> <br>
                    <div class="">
                        <p>Here is your complete payment details for <span style="font-weight:bold" class="text-primary">TIEZA Online Payment</span></p>
                    </div>
                    <br>
                    <div class="">
                        <table class="" style="width:100%">
                            <thead>
                                <tr>
                                    <td>PASSENGER NAME</td>
                                    <td>TICKET NUMBER</td>
                                </tr>
                            </thead>
                            <tbody>
                             
                                @foreach ($details as $item)
                                    <tr>
                                        <td >{{$item->first_name.' '.$item->last_name}}</td>
                                        <td>{{$item->ticket_no}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="py-1">
                        EMAIL ADDRESS: <b>{{$fulltax->email_address}}</b>
                    </div>
                    <div class="py-1">
                        PAYMENT CHANNEL: {{$payment_channel}}
                    </div>
                    <div class="py-1 row ">
                        <div class="col-8">
                            TRANSACTION REFERENCE NUMBER: <b>{{$fulltax->fulltax_no}}</b>
                        </div>
                        <div class="col-4">
                            DATE AND TIME: <b>{{$fulltax->created_at}}</b>
                        </div>
                    </div>
                    <div class="py-1">
                        PAYMENT REFERENCE NUMBER: {{$payment_reference_no}}
                    </div>
                    <div class="py-1 row ">
                        <div class="col-8">
                            PAYMENT FOR: <b>FULL TRAVEL TAX</b>
                        </div>
                        <div class="col-4">
                            AMOUNT: <b> ₱{{$fulltax->fulltax_amount}}</b>
                        </div>
                    </div>
                    <br>
                    <div class="centers text-primary">
                        PAYMENT SUMMARY
                    </div>
                    <br>
                    <div class="py-1">
                        Amount Due: <b> {{$fulltax->fulltax_amount}}</b>
                    </div>
                    <div class="py-1">
                        Convenience / Service Fee: <b> 50.00</b>
                    </div>
                    <div class="py-2">
                       TOTAL: <b>{{number_format($fulltax->fulltax_amount + 50,2)}}</b>
                    </div>
                    <br>
                    <div class="py-1">
                       <p>If you need to contact us, you can do it through this email address <a href="#">traveltax.helpdesk@tieza.gov.ph</a></p>
                     </div>
                     <div class="py-1">
                       Sincerely,
                    </div>
                    <div class="py-1">
                        TIEZA
                     </div>
                </div>
                <div class="col-md-1 col-md-1 col-sm-1"></div>
               
            </div>
        </div>
    </div>
    @include('layouts.auth-partials.scripts')
    @include('layouts.auth-partials.form-scripts')

    <script type="text/javascript" src="{{ asset('qrcodejs/qrcode.js')}}"></script>
    <script type="text/javascript" src="{{ asset('easyqrcodejs/src/easy.qrcode.js')}}"></script>


    <script type="text/javascript">
       
        var qrcode = new QRCode(document.getElementById("qrcode"), {
            text: 'Name of Applicant: ' + "{{ $ta->ffull_name }}" + '\n' + 'Passport No.: ' + "{{ $ta->passport_no }}" + '\n' + 'Ticket No.: ' + "{{ $ta->ticket_no }}" + '\n' + 'TEC No.: ' + "{{ $ta->tec_id }}" + '\n' + 'TEC Date Issued: ' + "{{ date("m/d/Y", strtotime($ta->date_application)) }}" + '\n' + 'TEC Validity: ' + "{{ date("m/d/Y", strtotime($ta->date_validity)) }}",
            logo: "{{ asset('img/tieza-logo.png') }}",
            logoWidth: 80,
            logoHeight: 80,
            width : 190,
            height : 190,
            logoBackgroundColor: '#ffffff',
            logoBackgroundTransparent: true
        });

        {{--
        var qrcode = new QRCode(document.getElementById("qrcode"), {
            width : 120,
            height : 120
        });

        function makeCode () {      
            qrcode.makeCode('Name of Applicant: ' + "{{ $ta->ffull_name }}" + '\n' + 'Passport No.: ' + "{{ $ta->passport_no }}" + '\n' + 'Ticket No.: ' + "{{ $ta->ticket_no }}" + '\n' + 'TEC No.: ' + "{{ $ta->tec_id }}" + '\n' + 'TEC Date Issued: ' + "{{ date("m/d/Y", strtotime($ta->date_application)) }}" + '\n' + 'TEC Validity: ' + "{{ date("m/d/Y", strtotime($ta->date_validity)) }}");
        }

        $(qrcode._el).find('img').on('load', function() {
            var srcimg = this.src;
            console.log(srcimg);
            $("#downloadqr").attr('href', srcimg);
            $("#downloadqr").attr('download', "QR.png");
        });

        makeCode();
        --}}
    </script>

    <script src="{{ asset('js/custom.js')}}"></script>
</body>

</html>