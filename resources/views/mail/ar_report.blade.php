<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    
    <style type="text/css">
        body {
            background: white;
            font-size: 10pt;
            font-family: arial /**Courier New**/;
            color: black;
        }
        input {
            border: 1px solid black;
        }
        .canvas {
        }

        .bg-triangle {
             background-image: url('{{ asset('img/bg-tieza2.png') }}');
            background-size:auto;
            height:100%;
            width:100%;

        }
        th, td {
        padding: 5px;
        }
        img {
            display:block;
        }

        .center-image {
            display:block;
            margin-left: auto;  
            margin-right: auto;
            width:8%;
        }
        .centers{
            text-align: center !important;
        }
    </style>
</head>
<body>
    <div class="container-fluid bg-triangle" style="padding:5px" >
        <br>
        <div class="col-md-12">
            <div class="row" >
                <div class="col-md-1 col-sm-1"></div>
                <div class="col-md-10 col-sm-10 ">
                    <div class="row center-image">
                            <img src="{{ asset('img/tieza-logo.png')}}" alt="logo" srcset="" class=" " style="height:100px;">
                    </div>
                    <br>
                    <div class="centers">
                        <h3 style="font-weight:bold;"><u>ACKNOWLEDGEMENT RECEIPT</u></h3>
                    </div>
                    <div class="">
                        <h4 style="font-weight:bold">AR NUMBER: {{$ar_no ?? $fulltax->ar_no}}  </h4>
                    </div>
                    <div class="">
                       Hello <b>{{$fulltax->first_name.' '.$fulltax->middle_name.' '.$fulltax->last_name .' '.$fulltax->ext_name ?? ''}}</b>
                    </div> 
                    <div class="">
                        <p>Here is your complete payment details for <span style="font-weight:bold" class="text-primary">TIEZA Online Payment</span></p>
                    </div>
                    <div class="">
                        <table class="" style="width:100%">
                            <thead>
                                <tr>
                                    <td><u> PASSENGER NAME</u></td>
                                    <td><u>TICKET NUMBER</u></td>
                                </tr>
                            </thead>
                            <tbody>
                             
                                @foreach ($details as $item)
                                    <tr>
                                        <td ><b>{{$item->first_name.' '.$item->middle_name.' '.$item->last_name .' '.$item->ext_name ?? ''}}</b></td>
                                        <td><b>{{$item->ticket_no}}</b></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="py-1">
                        EMAIL ADDRESS: <b>{{$fulltax->email_address}}</b>
                    </div>
                    <div class="py-1">
                        PAYMENT CHANNEL: {{$payment_channel ?? $fulltax->payment_channel}}
                    </div>
                    <div class="py-1">
                        TRANSACTION REFERENCE NUMBER: <b>{{$fulltax->fulltax_no}}</b>
                    </div>
                    <div class="py-1">
                        DATE AND TIME: <b>{{$fulltax->created_at}}</b>
                    </div>
                    <div class="py-1">
                        PAYMENT REFERENCE NUMBER: {{$payment_reference_no ?? $fulltax->payment_reference_no}}
                    </div>
                    <div class="py-1">
                       <b><u> PAYMENT SUMMARY</u></b>
                    </div>
                    <div class="py-1">
                        
                     
                        <table >
                            <tr>
                                <td>Full Travel Tax Fee:</td>
                                <td style="text-align: right"><b> {{number_format($f_amount,2)}}</b></td>
                            </tr>
                            <tr>
                                <td>Processing/Convenience Fee:</td>
                                <td style="text-align: right"><b> {{number_format($f_fee,2)}}</b></td>
                            </tr>
                            <tr>
                                <td>TOTAL AMOUNT DUE:</td>
                                <td style="text-align: right"><b><u> {{number_format($f_total,2)}}</u></b></td>
                            </tr>
                        </table>
                    </div>
                    {{-- <div class="py-1">
                        Convenience / Service Fee: <b> 50.00</b>
                    </div>
                    <div class="py-2">
                       TOTAL: <b>{{number_format($fulltax->fulltax_amount + 50,2)}}</b>
                    </div> --}}
                    <br>
                    <div class="py-1">
                       <p>For questions or concerns, you may email us at <a href="#">traveltax.helpdesk@tieza.gov.ph</a></p>
                     </div>
                     <div class="py-1">
                        <p>Please print <b>two (2) copies</b> of this AR and present them at the airline check-in counter before boarding.</p>
                      </div>
                     <div class="py-1">
                       Sincerely,
                    </div>
                    <div class="py-1">
                        TIEZA
                     </div>
                </div>
                <div class="col-md-1 col-md-1 col-sm-1"></div>
               
            </div>
        </div>
    </div>
    @include('layouts.auth-partials.scripts')
    @include('layouts.auth-partials.form-scripts')

    <script type="text/javascript" src="{{ asset('qrcodejs/qrcode.js')}}"></script>
    <script type="text/javascript" src="{{ asset('easyqrcodejs/src/easy.qrcode.js')}}"></script>


    <script type="text/javascript">
       
      
    </script>

    <script src="{{ asset('js/custom.js')}}"></script>
</body>

</html>