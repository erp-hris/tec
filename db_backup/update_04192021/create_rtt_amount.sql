-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 20, 2021 at 03:37 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `otps`
--

-- --------------------------------------------------------

--
-- Table structure for table `rtt_amount`
--

CREATE TABLE `rtt_amount` (
  `id` int(11) NOT NULL,
  `type_applicant_fee_id` int(11) DEFAULT NULL,
  `section_type_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rtt_amount`
--

INSERT INTO `rtt_amount` (`id`, `type_applicant_fee_id`, `section_type_id`, `amount`, `description`, `created_at`, `deleted_at`) VALUES
(1, 1, 3, 1350, 'First Class - Standard Reduced Rate', '2021-04-20 01:36:09', NULL),
(2, 2, 3, 810, 'Economy - Standard Reduced Rate', '2021-04-20 01:36:09', NULL),
(3, 1, 4, 400, 'First Class - Priviledge Reduced Rate', '2021-04-20 01:36:42', NULL),
(4, 2, 4, 300, 'Economy - Priviledge Reduced Rate', '2021-04-20 01:36:42', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rtt_amount`
--
ALTER TABLE `rtt_amount`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rtt_amount`
--
ALTER TABLE `rtt_amount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
