ALTER TABLE `rtt_application` ADD `type_applicant_id` INT NULL AFTER `supervisor_id`, ADD `type_reduced_id` INT NULL AFTER `type_applicant_id`, ADD `reduced_amount` DECIMAL(15,2) NULL AFTER `type_reduced_id`;
ALTER TABLE `rtt_application` CHANGE `reduced_amount` `reduced_amount` DECIMAL(15,2) NULL DEFAULT '0';
ALTER TABLE `rtt_application` ADD `rtt_no` VARCHAR(50) NULL AFTER `id`;