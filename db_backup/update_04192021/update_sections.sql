UPDATE `sections` SET `code` = '2Aa' WHERE `sections`.`id` = 29;
UPDATE `sections` SET `code` = '2Ab' WHERE `sections`.`id` = 30;
UPDATE `sections` SET `code` = '2Ac' WHERE `sections`.`id` = 31;
UPDATE `sections` SET `name` = 'Children of OFWs with disabilities even above 21 years of age' WHERE `sections`.`id` = 32;
UPDATE `sections` SET `code` = '2B3' WHERE `sections`.`id` = 32;
INSERT INTO `sections` (`id`, `code`, `name`, `section_type`) VALUES (NULL, '2B1', 'Legitimate spouse of an Overseas Filipino Worker (OFW)', '4'), (NULL, '2B2', 'Unmarried children of an OFW whether legitimate or illegitimate who are below 21 years of age', '4');
INSERT INTO `sections` (`id`, `code`, `name`, `section_type`) VALUES (NULL, NULL, "Seaman's spouse joining vessel", '4'), (NULL, NULL, "Seaman's children joining vessel", '4'), (NULL, NULL, "Seaman's children with disabilities even above 21 years of age joining vessel", '4'); 