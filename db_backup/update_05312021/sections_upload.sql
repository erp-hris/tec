-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 31, 2021 at 10:48 PM
-- Server version: 5.7.34
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ttaxback_otps_v2_prod`
--

-- --------------------------------------------------------

--
-- Table structure for table `sections_upload`
--

CREATE TABLE `sections_upload` (
  `id` int(11) NOT NULL,
  `section_id` int(11) DEFAULT NULL,
  `description` text,
  `name` varchar(255) DEFAULT NULL,
  `key_tag` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections_upload`
--

INSERT INTO `sections_upload` (`id`, `section_id`, `description`, `name`, `key_tag`) VALUES
(1, 7, 'NOTE: IF HIRED THROUGH POEA, the Overseas Employment Certificate (OEC) serves as the<br> Travel Tax Exemption Certificate (TEC) and THERE IS NO NEED TO APPLY FOR A TEC. <br> If directly hired abroad, please upload your Certificate of Employment issued by<br> the Philippine Embassy / Consulate in the place of work or an Employment Contract<br> authenticated by the Philippine Embassy / Consulate.', NULL, NULL),
(2, 1, 'Certification from the Office of Protocol, Department of Foreign Affairs or their respective<br> Embassy / Consulate.', NULL, NULL),
(3, 2, 'Certification from the United Nations (UN) Organization or its agencies (WHO, World <br>Bank, FAO, ILO, UNICEF, UNIFA, etc.)<br>', NULL, NULL),
(4, 6, 'Government Transport Request (GTR) for a plane ticket or certification from the<br> US Embassy that the fare is paid from the US Government funds<br>', NULL, NULL),
(5, 8, 'Certification from the Civil Aeronautics Board that the crewmember is joining his aircraft,<br> indicating the crewmember’s name, position, and location of aircraft<br>', NULL, NULL),
(6, NULL, 'Copies of the identification pages of passport and stamp of last arrival<br> in the Philippines.<br>', NULL, NULL),
(7, 9, 'a. Proof of permanent residence in foreign country (e.g. US Green card, Canadian<br> Form 1000, etc.) FRONT and BACK or b.  Valid Certification from the Philippine<br> consulate / embassy indicating that the Filipino has resided uninterruptedly for a period<br> of five (5) years in the foreign country without having been absent <br>therefrom for more than six (6) months in any one year or appropriate<br> stamps in the passport to prove such length of stay.', NULL, 6),
(8, NULL, 'Proof of permanent residence in foreign country (e.g. US Green card, Canadian<br> Form 1000, etc)or Certification of Residence issued by the Philippine Embassy<br> / Consulate in the country which does not grant permanent resident status or<br> appropriate entries in the Passport. BACK PAGE<br>', NULL, NULL),
(9, 10, 'Certification from the Department of Foreign Affairs.', NULL, NULL),
(10, 11, 'Clear copy of Travel Authority or Travel Order from the Department Secretary<br> concerned to the effect that such officials / employees are traveling on<br> official business.', NULL, 7),
(11, 12, 'Proof that travel is funded or provided by a foreign government<br>', NULL, NULL),
(12, 13, 'Certification to this effect from concerned Philippine government agency. NOTE: “Student” is defined<br> as a person attending formal classes in a foreign educational institution for the purpose <br>of taking up a course leading to a diploma, the duration of which is <br>not less than one (1) year. For UNDP Fellow: Certification from UNDP specifying<br> that the Fellow is exempted from Travel Tax.<br>', NULL, NULL),
(13, 14, 'Birth certificate of the infant<br>', NULL, 4),
(14, 15, 'Certification to this effect from the Board of Investments.<br>', NULL, NULL),
(15, 16, 'Written authorization from the Office of the President expressly entitling the passenger<br> to travel tax exemption<br>', NULL, NULL),
(16, 20, 'All pages of your current Passport. Please combined all scanned pages of the<br> passport in one PDF file.<br>', NULL, NULL),
(17, 21, 'Copy of Foreign passport of former Filipino or other evidence of<br> former Philippine Citizenship.<br>', NULL, NULL),
(18, 21, 'Latest arrival stamp in the Philippines.', NULL, NULL),
(19, 20, 'VISA or Residence Card issued by the country of residence.<br><br>\r\nNote: In case your last arrival in the Philippines is stamped in an old passport,<br> you will need to scan this page from your old passport and include it on the PDF file.', NULL, NULL),
(20, 21, 'Marriage contract of accompanying spouse or Birth certificate or <br>adoption papers of the child.', NULL, 3),
(21, NULL, 'All pages of your current passport, Please combine all scanned pages of the passport in one PDF file.<br><br>Note: In case your last arrival in the Philippines is stamped in an old passport, <br>you will need to scan this page from your old passport and include this in the PDF file.', NULL, NULL),
(22, 3, 'Certification from the agency or organization concerned that the passenger belongs to<br> one of the categories, and that their travel is funded by the <br>agency or organization concerned<br>', NULL, NULL),
(23, 4, 'Endorsement from the Philippine Sports Commission.', NULL, NULL),
(24, 4, 'Travel Authority issued by the Philippine Sports Commission.', NULL, NULL),
(25, 4, ' Invitation from the host country.<', NULL, NULL),
(26, 5, 'Endorsement from DOST specifying that the Balik Scientist Awardee is exempted from<br>Travel Tax.', NULL, NULL),
(27, 5, 'For the Spouse or Dependent of a Balik Scientist under Long-Term engagement:<br> Proof of Relationship to the Balik Scientist. (Marriage Certificate, Birth Certificate)', NULL, NULL),
(28, NULL, 'Government Transport Request (GTR) for a plane ticket or certification from the<br> US Embassy that the fare is paid from US Government funds.', NULL, NULL),
(29, NULL, 'NOTE: IF HIRED THROUGH POEA, the Overseas Employment Certificate (OEC) serves as the<br> Travel Tax Exemption Certificate (TEC) and THERE IS NO NEED TO APPLY FOR A TEC. <br> If directly hired abroad, please upload your Certificate of Employment issued by<br> the Philippine Embassy / Consulate in the place of work or an Employment Contract<br> authenticated by the Philippine Embassy / Consulate.', NULL, NULL),
(30, 20, 'Airline ticket used in traveling to the Philippines. <br>                                    NOTE: In case your last arrival in the Philippines is stamped in an old <br>passport, you will need to scan this page from your old passport and<br> include it in the PDF file.', NULL, NULL),
(31, 18, 'Endorsement from the Export Development Council (EDC).', NULL, NULL),
(32, 22, 'Copy of fare refund voucher or certification from the airline authorized signatory<br> that the ticket is unused, non-rebookable, and has no fare refund value.<br>', NULL, NULL),
(33, 22, 'Original TIEZA Official Receipt (passenger and airline copies), if travel tax was<br> paid directly to TIEZA.<br>', NULL, NULL),
(34, 23, 'In case the original passport cannot be presented, a certification from the<br> Bureau of Immigration indicating the passenger’s identity, the immigration status, and the<br> applicable date of arrival shall be submitted.<br>', NULL, 8),
(35, 23, 'Original TIEZA Official Receipt (passenger copy), if travel tax was paid directly to TIEZA<br>', NULL, NULL),
(36, 24, 'Supporting documents for travel tax exemption (kindly refer to the table on<br> TRAVEL TAX EXEMPTION)<br>', NULL, NULL),
(37, 24, 'Original TIEZA Official Receipt (passenger copy), if travel tax was paid directly to TIEZA<br>', NULL, NULL),
(38, 25, 'Supporting documents for reduced travel tax (kindly refer to the table on <br>REDUCED TRAVEL TAX)', NULL, NULL),
(39, 25, 'Original TIEZA Official Receipt (passenger copy), if travel tax was paid directly to TIEZA<br>', NULL, NULL),
(40, 26, 'Certification from the airline that the ticket was downgraded or a certified copy<br> of the airline flight manifest.<br>', NULL, NULL),
(41, 26, 'Original TIEZA Official Receipt (passenger copy), if travel tax was paid directly to TIEZA.<br>', NULL, NULL),
(42, 27, 'Original TIEZA Official Receipt (passenger and airline copies), if travel tax was paid directly to TIEZA.<br>', NULL, NULL),
(43, 28, 'Original TIEZA Official Receipt (passenger and airline copies), if travel tax was paid directly to TIEZA.<br>', NULL, NULL),
(44, 29, 'In case the original passport cannot be presented, the certified true copy<br> / authenticated copy of the birth certificate and photocopy of identification page of passport<br>', NULL, 4),
(45, NULL, 'Airline ticket, if already issued<br>', NULL, NULL),
(46, 30, 'Certification from the passenger’s editor or station manager that he/she is<br> an accredited journalist<br>', NULL, NULL),
(47, 30, 'Certification from the Office of the Press Secretary that the travel is<br> in pursuit of journalistic assignment<br>', NULL, NULL),
(48, 31, 'Written authorization from the Office of the President expressly entitling the passenger to<br> the reduced travel tax<br>', NULL, NULL),
(49, NULL, 'Airline ticket, if already issued<br>', NULL, NULL),
(50, 32, 'Original Person With Disability (PWD) ID card issued by offices established by<br> the National Council on Disability Affairs (NCDA)<br>', NULL, NULL),
(51, 32, 'Original Overseas Employment Certificate (OEC) / Certified True Copy of Balik-Manggagawa Form / Certified<br> True Copy of the OFW’s Travel Exit Permit / Certification of Accreditation or Registration / OFW’s valid work visa / Work Permit / OFW’s<br> Valid Employment Contract / OFW’s valid company ID<br> / OFW’s recent pay slip<br>', NULL, 5),
(52, 32, 'Certified true copy / authenticated copy of the birth certificate<br>', NULL, 4),
(53, NULL, 'Airline ticket, if already issued<br>', NULL, NULL),
(54, 32, 'Certification from the manning agency that the seaman’s dependent is joining the<br> seaman’s vessel<br>', NULL, NULL),
(55, NULL, 'Original Passport', NULL, NULL),
(56, NULL, 'Original Passport', NULL, NULL),
(57, NULL, 'Airline ticket, if already issued', NULL, NULL),
(58, NULL, 'Original Passport', NULL, NULL),
(59, 33, 'Original Overseas Employment Certificate (OEC) / Certified True Copy of Balik-Manggagawa Form / Certified<br> True Copy of the OFW’s Travel Exit Permit / Certification of Accreditation or Registration / OFW’s valid work visa / Work Permit / OFW’s<br> Valid Employment Contract / OFW’s valid company ID<br> / OFW’s recent pay slip<br>', NULL, NULL),
(60, 34, 'Original Overseas Employment Certificate (OEC) / Certified True Copy of Balik-Manggagawa Form / Certified<br> True Copy of the OFW’s Travel Exit Permit / Certification of Accreditation or Registration / OFW’s valid work visa / Work Permit / OFW’s<br> Valid Employment Contract / OFW’s valid company ID<br> / OFW’s recent pay slip<br>', NULL, NULL),
(61, 37, 'Original Overseas Employment Certificate (OEC) / Certified True Copy of Balik-Manggagawa Form / Certified<br> True Copy of the OFW’s Travel Exit Permit / Certification of Accreditation or Registration / OFW’s valid work visa / Work Permit / OFW’s<br> Valid Employment Contract / OFW’s valid company ID<br> / OFW’s recent pay slip<br>', NULL, NULL),
(62, 33, 'Original or authenticated marriage contract', NULL, NULL),
(63, 33, 'Certification from the manning agency that the seaman’s dependent is <br>joining the seaman’s vessel', NULL, NULL),
(64, 34, 'Original PWD ID card issued by offices established by the National <br>Council of Disability Affairs (NCDA)', NULL, NULL),
(65, 34, 'Original or authenticated birth certificate', NULL, NULL),
(66, 34, 'Certification from the manning agency that the seaman’s dependent is joining<br> the seaman’s vessel', NULL, NULL),
(67, 32, 'Original PWD ID card issued by offices established by the <br>National Council of Disability Affairs (NCDA)', NULL, NULL),
(68, 32, 'Original or authenticated birth certificate', NULL, NULL),
(69, 32, 'Certification from the manning agency that the seaman’s dependent is <br>joining the seaman’s vessel', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sections_upload`
--
ALTER TABLE `sections_upload`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sections_upload`
--
ALTER TABLE `sections_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
